#pragma once
#include <fmt/format.h>
#include <string>
#include <string_view>

namespace watchdog
{
   struct build_info
   {
      constexpr static std::string_view date = __DATE__;
      constexpr static std::string_view time = __TIME__;
      constexpr static std::string_view number = "//number//";
      constexpr static std::string_view git_commit_hash = "//commit//";
      constexpr static std::string_view git_branch = "//branch//";

      inline static std::string to_string()
      {
         return fmt::format("{} {} - build number {} git: {}@{}", build_info::date, build_info::time,
                            build_info::number, build_info::git_commit_hash, build_info::git_branch);
      }
   };

} // namespace watchdog