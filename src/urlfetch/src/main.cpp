#include <commons/url_fetch.h>
#include <iostream>
#include <string>
#include <string_view>
#include <thread>

void print_usage()
{
   std::cerr << "incorrect number of arguments\n";
   std::cerr << "usage: urlfetch [mode] [url] <output file> <headers ...>\n";
   std::cerr << "\tmode -s print to output\n";
   std::cerr << "\tmode -f saves it to a file\n";
}

commons::url::header_container parse_headers(int beg_idx, int len, const char *headers[])
{
   commons::url::header_container result;
   std::string key, value;

   for (int i = beg_idx; i < len; i += 2)
   {
      key = headers[i];

      if (i + 1 < len)
      {
         value = headers[i + 1];
      }
      else
      {
         value.clear();
      }

      result.emplace_back(key, value);
   }

   return result;
}

#ifdef WIN32
#include "win/getppid.h"
std::atomic<bool> g_exited = false;
void on_exit()
{
   g_exited = true;
}
void check_parent_process()
{
   using namespace std::chrono_literals;
   std::atexit(on_exit);
   std::at_quick_exit(on_exit);

   auto ppid = getppid();
   std::thread([=]() {
      std::int64_t ticks = 1;
      HANDLE hProcess = OpenProcess(SYNCHRONIZE, FALSE, ppid);

      if (hProcess)
      {
         while (!g_exited)
         {
            auto result = WaitForSingleObject(hProcess, 5);
            if (result == WAIT_OBJECT_0)
            {
               CloseHandle(hProcess);
               g_exited = true;
               std::exit(4);
            }
         }
      }
   }).detach();
}

#else
void check_parent_process() {}
#endif

int main(int argc, const char *argv[])
{

   check_parent_process();
   try
   {
      if (argc < 3)
      {
         print_usage();
         return 1;
      }

      auto mode = std::string_view{argv[1]};
      auto url = std::string_view{argv[2]};
      if (mode == "-s")
      {
         std::string result;
         auto headers = parse_headers(3, argc, argv);
         const auto server_status = commons::url::fetch_to_string(result, static_cast<std::string>(url), headers);
         std::cout << result;
         std::cerr << "status:" << server_status << "\n";

         return server_status;
      }
      else if (mode == "-f")
      {
         if (argc < 4)
         {
            print_usage();
            return 1;
         }

         auto headers = parse_headers(4, argc, argv);
         const auto server_status = commons::url::fetch_to_file(argv[3], static_cast<std::string>(url), headers);
         std::cerr << "server status:" << server_status << "\n";

         return server_status;
      }
      else
      {
         print_usage();
         return 1;
      }
   }
   catch (const std::exception &ex)
   {
      std::cerr << "error: " << ex.what() << "\n";
      return 2;
   }
   catch (...)
   {
      std::cerr << "error: unknown"
                << "\n";
      return 3;
   }

   return 0;
}
