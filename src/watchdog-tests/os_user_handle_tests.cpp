#include "watchdog/io_process.h"

#include <catch.hpp>
#include <iostream>
#include <watchdog/os_user_handle.h>

using namespace watchdog;

TEST_CASE("parse os_user struct", "[os_user]")
{
   SECTION("simple cases")
   {
      auto user = os_user::parse("ADDOMAIN\\user@domain.com?interactive:password");
      REQUIRE(user.has_value() == true);
      REQUIRE(user->name == "user@domain.com");
      REQUIRE(user->domain == "ADDOMAIN");
      REQUIRE(user->interactive == true);
      REQUIRE(user->password == "password");
   }

   SECTION("no domain")
   {
      auto user = os_user::parse("user?interactive:password");
      REQUIRE(user.has_value() == true);
      REQUIRE(user->name == "user");
      REQUIRE(user->domain == "");
      REQUIRE(user->interactive == true);
      REQUIRE(user->password == "password");
   }

   SECTION("no password")
   {
      auto user = os_user::parse("user?interactive");
      REQUIRE(user.has_value() == true);
      REQUIRE(user->name == "user");
      REQUIRE(user->domain == "");
      REQUIRE(user->interactive == true);
      REQUIRE(user->password == "");
   }

   SECTION("only username")
   {
      auto user = os_user::parse("user");
      REQUIRE(user.has_value() == true);
      REQUIRE(user->name == "user");
      REQUIRE(user->domain == "");
      REQUIRE(user->interactive == false);
      REQUIRE(user->password == "");
   }

   SECTION("empty string")
   {
      auto user = os_user::parse("");
      REQUIRE(user.has_value() == false);
   }

   SECTION("user name with spaces")
   {
      auto user = os_user::parse("user name");
      REQUIRE(user.has_value() == true);
      REQUIRE(user->name == "user name");
      REQUIRE(user->domain == "");
      REQUIRE(user->interactive == false);
      REQUIRE(user->password == "");
   }

   SECTION("user name as email")
   {
      auto user = os_user::parse("jhruby.web@gmail.com");
      REQUIRE(user.has_value() == true);
      REQUIRE(user->name == "jhruby.web@gmail.com");
      REQUIRE(user->domain == "");
      REQUIRE(user->interactive == false);
      REQUIRE(user->password == "");
   }

   SECTION("user name with spaces and password")
   {
      auto user = os_user::parse("user name:pwd");
      REQUIRE(user.has_value() == true);
      REQUIRE(user->name == "user name");
      REQUIRE(user->password == "pwd");
      REQUIRE(user->domain == "");
      REQUIRE(user->interactive == false);
   }
}

TEST_CASE("os_user_handle", "[os_user_handle]")
{
   auto *value = getenv("WATCHDOG_TEST_LOGON");
   if (value == nullptr || strlen(value) == 0)
   {
      std::cerr << "Please provide WATCHDOG_TEST_LOGON environment variable to test out login code: \nexample "
                   "WATCHDOG_TEST_LOGON=testuser?interactive:testuser"
                << std::endl;
      return;
   }

   auto user = os_user::parse({value});
   REQUIRE(user.has_value() == true);
   auto handle = os_user_handle{user.value()};
   REQUIRE(handle.get_name() == user->name);
}

struct script_output_impl : public null_script_output
{
   std::string buffer_;
   inline void on_std_out(std::string_view buffer) override
   {
      buffer_ += buffer;
   }
};

TEST_CASE("os_user_handle to launch processes", "[os_user_handle]")
{
   auto *value = getenv("WATCHDOG_TEST_LOGON");
   if (value == nullptr || strlen(value) == 0)
   {
      std::cerr << "Please provide WATCHDOG_TEST_LOGON environment variable to test out login code :\nexample "
                   "WATCHDOG_TEST_LOGON=testuser?interactive:testuser"
                << std::endl;
      return;
   }

   auto io_context = std::make_shared<boost::asio::io_context>();
   auto group = boost::process::group{};
   auto output = script_output_impl{};
   auto process = io_process::start_io_process(io_context, group, command{"test_user.exe"}, false,
                                               io_process_capture_settings{std_handle_output{.output = &output}},
                                               os_user::parse(value));
   process->start_read();
   auto jthread = std::jthread([&io_context]() { io_context->run(); });

   process->get().wait();

   io_context->stop();
   REQUIRE(boost::trim_copy(output.buffer_) == "testuser");
}