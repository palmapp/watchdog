#include <iostream>
#include <windows.h>
int main()
{
   WCHAR username[1024];
   DWORD username_len = 1024;
   GetUserNameW(username, &username_len);

   std::wcout << username << std::endl;
   return 0;
}