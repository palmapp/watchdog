#include <ranges>
#include <watchdog/advanced_script_executor.h>

#define CATCH_CONFIG_MAIN

#include <catch.hpp>
#include <thread>

#include <easylogging++.h>
#include <watchdog/command_parser.h>
#include <watchdog/script_engine_resolver.h>

INITIALIZE_EASYLOGGINGPP

using namespace watchdog;
using namespace std::string_literals;

TEST_CASE("command_parser", "[command parser]")
{
   auto simple = command::parse("cmd");
   REQUIRE(simple.has_value() == true);
   REQUIRE(simple->command_string == "cmd");
   REQUIRE(simple->arguments.empty() == true);

   const auto assert_cmd = [](std::optional<command> const &cmd)
   {
      REQUIRE(cmd.has_value() == true);
      REQUIRE(cmd->command_string == "cmd");
      REQUIRE(cmd->arguments.size() == 3);
      REQUIRE(cmd->arguments[0] == "1");
      REQUIRE(cmd->arguments[1] == "2");
      REQUIRE(cmd->arguments[2] == "3");
   };

   assert_cmd(command::parse("cmd 1 2 3"));
   assert_cmd(command::parse("\"cmd\" \"1\" \"2\" \"3\""));
   assert_cmd(command::parse("\'cmd\' \'1\' \'2\' \'3\'"));
   assert_cmd(command::parse("cmd \"1\" \'2\' 3"));

   REQUIRE(command::parse("").has_value() == false);
}

TEST_CASE("expand var", "[command parser]")
{
   auto props = string_map{{"a"s, "a-value"s}, {"b"s, "b-value"s}, {"c"s, "c-value"s}};

   REQUIRE(command::expand_tokens("$a/{{{b}}}/$c/$d/test", &props) == "a-value/b-value/c-value//test");
   REQUIRE(command::expand_tokens("$a{{{b}}}$c$d/test", &props) == "a-valueb-valuec-value/test");

   auto expanded_path = command::expand_tokens("$PATH", &props);
   REQUIRE(expanded_path.size() > 0);

   REQUIRE(command::expand_tokens("$a/{{{b}}}/$c/$d/test", &props, true, false) == "$a/b-value/$c/$d/test");
}

TEST_CASE("test parse channel", "[parse channel]")
{
   REQUIRE(app_group_config::parse_channel(
               "https://termix.vitrium.com/channel/deloitte-staging/?configuration=single-node") == "deloitte-staging");
   REQUIRE(app_group_config::parse_channel("https://termix.vitrium.com/channel/deloitte-staging/") ==
           "deloitte-staging");
}

#ifndef NDEBUG
TEST_CASE("test script engine resolver", "[script engine resolver]")
{
   auto cmd = to_script_engine({"test.py", {"arg0", "arg1"}});
   REQUIRE(cmd.command_string.find("python") != std::string::npos);
   REQUIRE(cmd.arguments.size() == 3);

   auto bash = to_script_engine({"test.sh", {"arg0", "arg1"}});
   REQUIRE(bash.command_string.find("bash") != std::string::npos);
   REQUIRE(bash.arguments.size() == 3);

#ifdef WIN32
   auto power_shell = to_script_engine({"test.ps1", {"arg0", "arg1"}});
   REQUIRE(power_shell.command_string.find("powershell") != std::string::npos);
   REQUIRE(power_shell.arguments.size() == 4);

   auto batch = to_script_engine({"test.bat", {"arg0", "arg1"}});
   REQUIRE(batch.command_string.find("cmd") != std::string::npos);
   REQUIRE(batch.arguments.size() == 4);
#endif
}
#endif

TEST_CASE("command::expand_tokens with aws secret manager", "[command parser]")
{
   auto props = string_map{{"a"s, "1"s}, {"b"s, "2"s}, {"c"s, "3"s}};
   auto str = command::expand_tokens("$aws:ERA/GLOBAL/TOOLS/VITRIUM/DB/CONNECT_INFO%username$", &props, true, false);

   // no change
   REQUIRE(str == "$aws:ERA/GLOBAL/TOOLS/VITRIUM/DB/CONNECT_INFO%username$");
}