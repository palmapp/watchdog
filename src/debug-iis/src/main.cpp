
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include <commons/file_system.h>
#include <commons/properties_parser.h>

#include <boost/algorithm/algorithm.hpp>
#include <boost/process.hpp>
#include <commons/make_string.h>
#include <cstdlib>
#include <easylogging++.h>
#include <iostream>
#include <mstch/mstch.hpp>
#include <watchdog/app_group_config.h>
#include <watchdog/system_updater.h>
INITIALIZE_EASYLOGGINGPP

namespace bp = boost::process;
namespace fs = boost::filesystem;
using namespace std;
using namespace watchdog;

int main(int argc, const char *argv[])
{
   cout << fs::current_path().string() << "\n";

   if (argc < 4)
   {
      cerr << "invalid number of arguments : <appName> <appPath> <tempFolderForAppHostConfig>\n";
      return 1;
   }
   fs::path program_files{getenv("ProgramFiles")};
   auto iis_exe = program_files / "IIS Express" / "iisexpress.exe";

   fs::path user_profile{getenv("USERPROFILE")};
   auto map = commons::parse_config_file(user_profile / ".bad.properties");
   auto channel = map["channel"];
   if (channel.empty())
   {
      cerr << "channel is empty : " << channel;
      return 1;
   }

   auto base_config = parse(fs::path{channel} / "channel.properties");
   auto config_provider = app_group_config{base_config};
   auto config = config_provider.get_config(argv[1], true);

   mstch::map root;
   root["name"] = std::string{argv[1]};
   root["path"] = fs::canonical(argv[2]).make_preferred().string();
   root["port"] = config.app_mode.substr(config.app_mode.find(':') + 1);

   const auto iis_conf_path = fs::canonical(fs::path{argv[3]} / "applicationhost.config").make_preferred();

   auto template_file_path = config.utils_path / "Templates" / "iis_application_host_config.tmpl";
   auto iis_application_host_config = commons::read_file(template_file_path);
   boost::replace_all(iis_application_host_config, "\r\n", "\n");

   if (iis_application_host_config.empty())
   {
      cerr << "unable to open template path " << template_file_path.string();
      return 2;
   }
   else
   {
      boost::filesystem::ofstream out{iis_conf_path};
      out << mstch::render(iis_application_host_config, root, {});
   }

   auto iis_conf_path_string = iis_conf_path.string();
   // boost::replace_all(iis_conf_path_string, "\\", "\\\\");
   // boost::replace_all(iis_conf_path_string, ":", "::");

   std::vector<std::string> args;

   args.push_back(commons::make_string("/site:", argv[1]));
   args.push_back(commons::make_string("/config:", iis_conf_path_string));
   args.emplace_back("/apppool:Clr4IntegratedAppPool");

   // this function will replace the exe image in memory, if succeded the function won't return

   cout.flush();
   cerr.flush();

   bp::child proc{iis_exe, bp::args(args)};
   cout << argv[1] << " is running at http://localhost:" << config.app_mode.substr(config.app_mode.find(':') + 1)
        << "\n";
   cout << "To debug .NET app attach to " << iis_exe.filename().string() << " PID = " << proc.id() << endl;
   cout.flush();

   proc.wait();

   return proc.exit_code();
}