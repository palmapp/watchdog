import express from "express"

const response =
    {
        "update_url": "https://termix.vitrium.com:443/package/1.0.0/vbus/2",
        "hash": "0afcc22092bbf9ec2e25ab42598d6a46",
        "size": 872470,
        "update_script": null,
        "before_update_script": null,
        "after_update_script": null,
        "metadata": [{"key": "current_build_number", "value": "2"}, {
            "key": "current_version",
            "value": "1.0.0"
        }, {"key": "current_hash", "value": "0afcc22092bbf9ec2e25ab42598d6a46"}]
    };

let timeToSendResponse = parseInt(process.argv[2]);
if (Number.isNaN(timeToSendResponse))
{
    timeToSendResponse = 0;
}
console.log("time to send response: ", timeToSendResponse);

const app = express();

app.get('/', function (req, res) {
    console.log("request received");

    res.status(200);
    res.set("Content-Type", "application/json");
    setTimeout(function (){
        res.send(JSON.stringify(response));
        console.log("response sent");
    }, timeToSendResponse);
});

app.listen(3000)