//
// Created by jhrub on 09.03.2023.
//

#pragma once
#include "indicators.hpp"
#include <filesystem>
#include <fmt/format.h>
#include <memory>
#include <string>
#include <thread>
#include <vector>

namespace watchdog
{
   class console_status
   {
    public:
      struct update_job_status : public script_output,
                                 public step_manager,
                                 public std::enable_shared_from_this<update_job_status>
      {
       public:
         update_job_status(std::string name, indicators::DynamicProgress<indicators::ProgressBar> &progress,
                           std::size_t progress_index)
             : name_(name), progress_manager_(progress), progress_index(progress_index),
               bar_(indicators::option::BarWidth{30}, indicators::option::ForegroundColor{indicators::Color::white},
                    indicators::option::ShowElapsedTime{false}, indicators::option::ShowRemainingTime{false},
                    indicators::option::ShowPercentage{true},
                    indicators::option::PrefixText{fmt::format("{:25}", name)},
                    indicators::option::PostfixText{"initializing"})
         {
            progress_manager_.push_back(bar_);
         }

         auto &bar()
         {
            progress_manager_.print_progress();
            return bar_;
         }

         void on_script_begin(std::string_view script_name, std::string_view command) override
         {
            bar().set_option(indicators::option::PostfixText(script_name));
            fmt::format_to(std::back_inserter(std_err_buffer_), "{}[{}]: command {}\n", name_, script_name, command);
         }
         void on_process_started(int pid) override {}
         void on_std_out(std::string_view buffer) override
         {
            std_out_buffer_.append(buffer);
         }
         void on_std_error(std::string_view buffer) override
         {
            std_err_buffer_.append(buffer);
         }
         void on_execution_error(std::string_view what) override
         {
            fmt::format_to(std::back_inserter(std_err_buffer_), "Execution error: {}\n", what);
            has_errors = true;
         }
         void on_script_finished(int status) override
         {
            fmt::format_to(std::back_inserter(std_out_buffer_), "Exited: {}\n\n", status);
            has_errors |= status != 0;

            buffer_.append(std_err_buffer_);
            buffer_.append(std_out_buffer_);

            std_err_buffer_.clear();
            std_out_buffer_.clear();
         }

         void on_update_step(std::string text) override
         {
            bar().set_option(indicators::option::PostfixText(text));
            step_ = std::move(text);
         }

         void on_step_done() override
         {
            if (has_errors)
            {
               bar().set_option(indicators::option::PostfixText(fmt::format("{}: error", step_)));
               bar().set_option(indicators::option::ForegroundColor(indicators::Color::red));
            }
            else
            {
               bar().set_option(indicators::option::PostfixText(fmt::format("{}: done", step_)));
               bar().mark_as_completed();
            }
         }

         void print_buffer()
         {
            std::cout << termcolor::reset << fmt::to_string(buffer_) << std::endl;
         }

         std::shared_ptr<script_output> begin(std::string_view output_name) override
         {
            return this->shared_from_this();
         }

         virtual void begin_download(boost::filesystem::path file, std::size_t expected_size) override
         {
            std::lock_guard lk{mutex_};
            check_path_ = file;
            expected_size_ = expected_size;
         }

         void update_download_progress()
         {
            auto progress = std::size_t{0};

            auto path = [&]
            {
               std::lock_guard lk{mutex_};
               return check_path_;
            }();

            if (path.has_value())
            {
               boost::system::error_code ec;
               auto size = boost::filesystem::file_size(path.value(), ec);
               if (!ec)
               {
                  progress =
                      static_cast<std::size_t>(static_cast<double>(size) / static_cast<double>(expected_size_) * 100.0);
               }
            }

            if (bar().is_completed())
            {
               bar().set_progress(100);
            }
            else
            {
               bar().set_progress(progress);
            }
         }

         virtual void finish_download() override
         {
            bar().set_progress(100);
            bar().mark_as_completed();
            std::lock_guard lk{mutex_};
            if (check_path_.has_value()) check_path_->clear();
         }

         std::string name_;
         indicators::DynamicProgress<indicators::ProgressBar> &progress_manager_;
         std::size_t progress_index;

         std::string step_;
         indicators::ProgressBar bar_;
         fmt::memory_buffer std_out_buffer_;
         fmt::memory_buffer std_err_buffer_;

         fmt::memory_buffer buffer_;

         bool has_errors = false;

         std::mutex mutex_;
         std::optional<boost::filesystem::path> check_path_;
         std::size_t expected_size_ = 0;
      };

      console_status()
          : progress_updater(
                [&](std::stop_token token)
                {
                   while (!token.stop_requested())
                   {
                      {
                         std::lock_guard lk{mutex_};

                         for (auto &job : jobs)
                         {
                            job->update_download_progress();
                         }
                      }
                      std::this_thread::sleep_for(std::chrono::milliseconds{250});
                   }
                })
      {
         progress.set_option(indicators::option::HideBarWhenComplete{false});

#if defined(BOOST_WINDOWS)
         // SetConsoleOutputCP(CP_UTF8);
         HANDLE hStdOut;

         hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);

         if (!GetConsoleMode(hStdOut, &old_mode))
         {
            return;
         }

         // Try to set the mode.
         if (!SetConsoleMode(hStdOut, ENABLE_ECHO_INPUT | ENABLE_PROCESSED_INPUT | ENABLE_VIRTUAL_TERMINAL_PROCESSING |
                                          DISABLE_NEWLINE_AUTO_RETURN))
         {
            return;
         }

#endif
         set_cursor_visible(false);
      }

      void set_cursor_visible(bool visible)
      {
#if defined(BOOST_WINDOWS)

         CONSOLE_CURSOR_INFO cursorInfo;

         GetConsoleCursorInfo(GetStdHandle(STD_OUTPUT_HANDLE), &cursorInfo);
         cursorInfo.bVisible = visible; // set the cursor visibility
         SetConsoleCursorInfo(GetStdHandle(STD_OUTPUT_HANDLE), &cursorInfo);
#else
         indicators::show_console_cursor(visible);
#endif
      }

      ~console_status()
      {
#if defined(BOOST_WINDOWS)
         SetConsoleMode(GetStdHandle(STD_OUTPUT_HANDLE), old_mode);
#endif
         set_cursor_visible(true);
      }

      std::shared_ptr<update_job_status> create_job(std::string job)
      {
         std::lock_guard lk{mutex_};

         auto result = std::make_shared<update_job_status>(job, progress, jobs.size());
         jobs.push_back(result);
         return result;
      }

      void join_step()
      {
         std::lock_guard lk{mutex_};
         if (!jobs.empty())
         {
            for (auto job_s : jobs)
            {
               if (job_s->has_errors)
               {
                  std::cout << termcolor::bold << termcolor::red << job_s->name_ << termcolor::reset << std::endl;
                  job_s->print_buffer();
               }
            }
            completed.insert(completed.end(), jobs.begin(), jobs.end());
            jobs.clear();
         }
      }

      void step_begin(std::string_view text)
      {
         std::cout << termcolor::reset << text << std::endl;
      }

    private:
      std::vector<std::shared_ptr<update_job_status>> jobs;
      std::vector<std::shared_ptr<update_job_status>> completed;
      indicators::DynamicProgress<indicators::ProgressBar> progress;
      std::mutex mutex_;
      std::jthread progress_updater;
#if defined(BOOST_WINDOWS)
      DWORD old_mode;
#endif
   };
} // namespace watchdog