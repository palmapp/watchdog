#include <boost/program_options.hpp>
#include <iostream>
#include <string>
#include <vector>

#include <boost/dll/runtime_symbol_info.hpp>
#include <commons/logging.h>
#include <commons/properties_parser.h>
#include <easylogging++.h>
#include <watchdog/installer.h>

#include "console_status.h"
#include <boost/asio.hpp>
#include <boost/asio/packaged_task.hpp>
#include <boost/filesystem/fstream.hpp>
#include <commons/uri_builder.h>
#include <execution>
#include <watchdog/build.h>
INITIALIZE_EASYLOGGINGPP

using namespace std;
using namespace watchdog;
namespace po = boost::program_options;
namespace fs = boost::filesystem;

void print_info(fs::path cfg_file);

app_base_config get_defaults(fs::path const &base, string const &channel)
{
   app_base_config result;
   result.app_repo_path = base / "bin";
   result.config_repo_path = base / "config";
   result.app_logging_path = base / "logs";
   result.app_data_path = base / "data";
   result.channel_path = base;

   return result;
}

watchdog::filesystem_path get_channel_path(po::variables_map const &vm)
{
   if (vm.count("channel"))
   {
      auto channel = vm.at("channel").as<std::string>();
      auto result = fs::weakly_canonical(channel);
      if (!result.is_absolute())
      {
         return fs::current_path() / result;
      }
      else
      {
         return result;
      }
   }
   else
   {
      return fs::current_path();
   }
}

string get_channel_name(po::variables_map const &vm)
{
   if (vm.count("channel"))
   {
      auto channel = vm.at("channel").as<std::string>();
      auto path = fs::weakly_canonical(channel);
      return path.filename().string();
   }
   else
   {
      return fs::current_path().filename().string();
   }
}

void disable_std_out_logging()
{
   el::Configurations conf;
   conf.setToDefault();
   conf.parseFromText(R"abc(
*GLOBAL:
  FORMAT = %datetime [%levshort] [%fbase:%line] %msg
  To_File = true
  To_Standard_Output = false
  Filename = install.log
  Max_Log_File_Size = 2097152 ## 2MB - Comment starts with two hashes (##)
*DEBUG:
  Enabled = false
*TRACE:
  Enabled = false
)abc");

   el::Loggers::setDefaultConfigurations(conf, true);

   if (conf.hasConfiguration(el::ConfigurationType::MaxLogFileSize))
   {
      commons::init_easylogging_rolling();
   }

   std::cerr << "The debug output is written to install.log file." << std::endl;
}

void enable_std_out_logging()
{
   el::Configurations conf;
   conf.setToDefault();
   conf.parseFromText(R"abc(*GLOBAL:
  FORMAT = %datetime [%levshort] [%fbase:%line] %msg
  To_File = true
  To_Standard_Output = true
  Filename = install.log
  Max_Log_File_Size = 2097152 ## 2MB - Comment starts with two hashes (##)
*DEBUG:
  Enabled = false
*TRACE:
  Enabled = false
)abc");

   el::Loggers::setDefaultConfigurations(conf, true);

   if (conf.hasConfiguration(el::ConfigurationType::MaxLogFileSize))
   {
      commons::init_easylogging_rolling();
   }
}

int main(int argc, const char *argv[])
{
   START_EASYLOGGINGPP(argc, argv);

   auto status_manager = console_status{};

   try
   {
      po::options_description desc("Allowed options");
      desc.add_options()("help", "produce help message")(
          "init", "it creates / replaces an initial configuration of the channel <current dir>/<arg>")(
          "install,I", "it configures and downloads the channel - this will never overwrite an existing configuration")(
          "configure,C",
          "it configures/reconfigures apps from the channel - this will overwrite an existing configuration")(
          "activate,A", "it sets the global value that the channel is used for development")(
          "info", "it prints install info for particular channel")(
          "scripts", "it runs all install scripts as app would be just installed")(
          "override-templates",
          "it checks out default configuration templates  into <arg>/config-templates, those are going to be used for "
          "the "
          "project configuration / reconfiguration. This call will preserve existing templates untouched.")(
          "channel-file", po::value<std::string>(),
          "it uses channel file to init channel instead of global channel.properties from watchdog installation "
          "directory")("preserve-channel-file",
                       "it does not regenerate channel.properties after initialization and it won't append <channel> "
                       "to the update.url.")(
          "channel", po::value<std::string>(),
          "it sets the channel directory. It can be specified as a last argument without --channel identifier.")(
          "query-tokens", po::value<std::string>(),
          "it loads config from specified channel url and prints out tokens that are required to have present");

      po::positional_options_description p;
      p.add("channel", -1);

      po::variables_map vm;
      po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);

      po::notify(vm);

      auto channel_path = get_channel_path(vm);
      auto channel_name = get_channel_name(vm);
      auto cfg_file = channel_path / "channel.properties";

      if (vm.count("help"))
      {
         cout << "Watchdog Version: " << build_info::to_string() << "\n";
         cout << desc << "\n";

         return 0;
      }

      if (vm.count("init"))
      {
         enable_std_out_logging();

         status_manager.step_begin("initializing channel dir");
         auto config_template = channel_path / "channel.properties";

         if (!fs::exists(config_template))
         {
            fs::create_directories(channel_path);
            if (vm.count("channel-file"))
            {
               fs::copy_file(vm.at("channel-file").as<std::string>(), config_template);
            }
            else
            {
               fs::copy_file(boost::dll::program_location().parent_path() / "channel.properties", config_template);
            }
         }

         LOG(INFO) << "initializing channel " << channel_path.string() << " from " << config_template.string();

         auto base_config = parse(config_template, get_defaults(channel_path, channel_name));
         if (vm.count("preserve-channel-file") == 0)
         {
            base_config.channel_base_url =
                commons::url::uri_builder{base_config.channel_base_url}.make_relative(channel_name + "/").to_string();

            if (!base_config.status_base_url.empty())
            {
               base_config.status_base_url =
                   commons::url::uri_builder{base_config.status_base_url}.make_relative(channel_name + "/").to_string();
            }
         }

         fs::create_directories(channel_path);
         fs::create_directories(base_config.config_repo_path);
         fs::create_directories(base_config.app_repo_path);
         fs::create_directories(base_config.app_logging_path);
         fs::create_directories(base_config.app_data_path);

         if (vm.count("preserve-channel-file") == 0)
         {
            status_manager.step_begin("creating channel.properties config");
            write_config(base_config, channel_path / "channel.properties");
         }

         status_manager.step_begin("done");
      }
      else if (vm.count("configure"))
      {
         enable_std_out_logging();

         if (!fs::exists(cfg_file))
         {
            LOG(ERROR) << cfg_file.string() << " does not exits: use option --init <channel> first";
            return 1;
         }
         else
         {
            status_manager.step_begin("configuring...");

            installer::run_app_configuration(channel_path, cfg_file);
            print_info(cfg_file);
         }
      }
      else if (vm.count("install"))
      {
         if (!fs::exists(cfg_file))
         {
            enable_std_out_logging();
            LOG(ERROR) << cfg_file.string() << " does not exits: use option --init <channel> first";
            return 1;
         }
         else
         {
            disable_std_out_logging();

            status_manager.step_begin("configuring...");

            auto cfg = installer::run_app_configuration(channel_path, cfg_file);

            auto pool = boost::asio::thread_pool{6};

            status_manager.step_begin("downloading...");

            auto progresses = std::vector<std::shared_ptr<console_status::update_job_status>>{};
            for (auto &app : cfg.apps)
            {
               progresses.push_back(status_manager.create_job(app));
            }

            auto tasks = std::vector<std::packaged_task<int()>>{};
            int i = 0;
            for (auto &app : cfg.apps)
            {

               tasks.push_back(std::packaged_task<int()>{[&, i = i++]
                                                         {
                                                            auto mng = progresses.at(i);
                                                            try
                                                            {
                                                               installer::install_app(cfg.config, app, *mng);

                                                               mng->on_step_done();

                                                               return 0;
                                                            }
                                                            catch (std::exception const &ex)
                                                            {
                                                               mng->on_execution_error(ex.what());
                                                               LOG(ERROR)
                                                                   << app << ": installation failed with " << ex.what();
                                                               mng->on_step_done();

                                                               return -1;
                                                            }
                                                         }});
            }

            auto futures = std::vector<std::future<int>>{};
            for (auto &t : tasks)
            {
               futures.push_back(boost::asio::post(pool, std::move(t)));
            }

            for (auto &f : futures)
            {
               f.wait();
            }
            pool.join();

            status_manager.join_step();
            print_info(cfg_file);
         }
      }
      else if (vm.count("scripts"))
      {
         enable_std_out_logging();

         if (!fs::exists(cfg_file))
         {
            LOG(ERROR) << cfg_file.string() << " does not exits: use option --init <channel> first";
            return 1;
         }
         else
         {
            auto base_config = parse(cfg_file);
            auto config_provider = app_group_config{base_config};
            auto apps = installer::get_configured_apps(base_config);

            for (auto &app : apps)
            {
               auto config = config_provider.get_config(app, true);
               auto model = read_update_model(config);
               if (model.has_value())
               {
                  run_scripts(config, model.value());
                  for (auto &[instance, instance_config] : config.instances)
                  {
                     run_scripts(instance_config, model.value());
                  }
               }
               else
               {
                  LOG(WARNING) << "no _json file located for app " << app;
               }
            }

            print_info(cfg_file);
         }
      }
      else if (vm.count("activate"))
      {
         enable_std_out_logging();

         if (!fs::exists(cfg_file))
         {
            LOG(ERROR) << cfg_file.string() << " does not exits: use option --init <channel> first";
            return 1;
         }
         else
         {
            fs::path user_profile{getenv("USERPROFILE")};
            auto path = user_profile / ".bad.properties";

            {
               fs::ofstream out{path};
               out << "channel = " << cfg_file.parent_path().string() << "\n";
            }
            LOG(INFO) << "setting default channel for development to " << path.string() << " as "
                      << cfg_file.parent_path().string();
         }
      }
      else if (vm.count("info"))
      {
         enable_std_out_logging();

         if (!fs::exists(cfg_file))
         {
            LOG(ERROR) << cfg_file.string() << " does not exits: use option --init <channel> first";
            return 1;
         }
         else
         {
            print_info(cfg_file);
         }
      }
      else if (vm.count("override-templates"))
      {
         enable_std_out_logging();

         if (!fs::exists(cfg_file))
         {
            LOG(ERROR) << cfg_file.string() << " does not exits: use option --init <channel> first";
            return 1;
         }
         else
         {
            auto base_config = parse(cfg_file);
            configuration_template::write_templates_to_directory(
                configuration_template::load_templates_from_url(base_config.channel_base_url),
                channel_path / "config-templates");
         }
      }
      else if (vm.count("query-tokens"))
      {
         enable_std_out_logging();

         auto channel_url = vm.at("query-tokens").as<std::string>();
         installer::print_tokens_json(channel_url);

         return 0;
      }
      else
      {
         cout << "Watchdog Version: " << build_info::to_string() << "\n";
         cout << desc << "\n";

         return 1;
      }
   }
   catch (const std::exception &ex)
   {
      enable_std_out_logging();

      indicators::show_console_cursor(true);
      LOG(ERROR) << ex.what();
      std::cerr << "Error occurred: check the install.log file.\n" << ex.what() << std::endl;

      return 1;
   }
   catch (...)
   {
      enable_std_out_logging();

      indicators::show_console_cursor(true);
      LOG(ERROR) << "unkonwn exception";
      std::cerr << "Error occurred: check the install.log file.\n" << std::endl;

      return 1;
   }
   indicators::show_console_cursor(true);
   return 0;
}

void print_info(fs::path cfg_file)
{
   auto base_config = parse(cfg_file);
   auto config_provider = app_group_config{base_config};
   auto apps = installer::get_configured_apps(base_config);
   for (auto const &app : apps)
   {

      auto config = config_provider.get_config(app, true);
      auto cfg = commons::parse_config_file(config.env_path);
      auto msg = commons::get_map_value(cfg, "install.info");

      if (!msg.empty())
      {
         LOG(INFO) << app << ": " << msg;
      }
   }

   LOG(INFO) << "Run command:\n"
             << "watchdog -cg " << cfg_file.parent_path();
}
