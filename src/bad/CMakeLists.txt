set(target bad)

add_executable(${target} src/main.cpp src/console_status.h)

target_link_libraries(${target} ${Boost_LIBRARIES} commons watchdog-lib logging build-info ${EXE_LIBS})

add_custom_command(TARGET ${target} POST_BUILD
        COMMAND ${CMAKE_COMMAND} -E copy $<TARGET_FILE:bad> $<TARGET_FILE_DIR:watchdog>
        )
add_custom_command(TARGET ${target} POST_BUILD
        COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_SOURCE_DIR}/channel.properties $<TARGET_FILE_DIR:watchdog>
        )

if (CMAKE_SYSTEM_NAME STREQUAL "Windows")
    if (CMAKE_BUILD_TYPE STREQUAL "Release" OR CMAKE_BUILD_TYPE STREQUAL "RelWithDebInfo")
        add_custom_command(TARGET ${target} POST_BUILD
                COMMAND ${CMAKE_COMMAND} -E copy $<TARGET_FILE_DIR:bad>/boost_program_options-vc143-mt-x64-${Boost_LIB_VERSION}.dll $<TARGET_FILE_DIR:watchdog>
                )
    else ()
        add_custom_command(TARGET ${target} POST_BUILD
                COMMAND ${CMAKE_COMMAND} -E copy $<TARGET_FILE_DIR:bad>/boost_program_options-vc143-mt-gd-x64-${Boost_LIB_VERSION}.dll $<TARGET_FILE_DIR:watchdog>
                )
    endif ()
endif ()
