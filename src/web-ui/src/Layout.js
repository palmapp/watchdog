import React from "react";
import {getTheme} from "@uifabric/styling";
import {Fabric} from "@fluentui/react";


export function Layout({children}) {
    const theme = getTheme();
    return (<Fabric className="App" style={{
        height: "calc(100% - 2em)",
        width: "calc(100% - 2em)",
        background: theme.palette.white,
        display: "flex",
        alignItems: "stretch",
        flexDirection: "column",
        padding: "1em",

    }}>{children}</Fabric>)
}