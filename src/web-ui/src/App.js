import React, {useEffect, useMemo, useState} from "react";
import {initializeIcons} from 'office-ui-fabric-react/lib/Icons';
import {DefaultButton, MessageBar, MessageBarType, PrimaryButton, Stack} from 'office-ui-fabric-react';

import {AzureThemeDark} from "@uifabric/azure-themes";
import {Layout} from "./Layout";
import {loadTheme} from "@uifabric/styling";
import useWebSocket from "./WebSocket";
import {AppGroupList} from "./AppGroupList";
import {useConst} from "@uifabric/react-hooks";
import {Button, Pivot, PivotItem} from "@fluentui/react";
import {LogView, useLogModel} from "./LogView";
import {List} from "immutable";

const theme = AzureThemeDark;

initializeIcons(/* optional base url */);
loadTheme(theme);

const stackTokens = {childrenGap: 40, padding: 10};

function Config({appState})
{
    const [showConfigInLine, setShowConfigInLine] = useState(true);

    return showConfigInLine ? (<table onClick={()=>setShowConfigInLine(false)}>
        <tr>
            <th style={{textAlign: "left"}}>URL:</th>
            <td><a href={appState.config.channelUrl} style={{color: "white"}} target={"_blank"} onClick={event => event.stopPropagation()}>{appState.config.channelUrl}</a></td>

            <th  style={{textAlign: "left"}}>Path: </th>
            <td>{appState.config.channelPath}</td>

            <th  style={{textAlign: "left"}}>Auto reconfigure on update: </th>
            <td>{appState.config.autoConfiguration ? "ON" : "OFF"}</td>

            <th  style={{textAlign: "left"}}>Force update on start: </th>
            <td>{appState.config.autoUpdateOnStart ? "ON" : "OFF"}</td>

            <th  style={{textAlign: "left"}}>Auto update enabled: </th>
            <td>{appState.config.autoUpdate > 0 ? "every " + appState.config.autoUpdate + "s" : "OFF"}</td>

            <th  style={{textAlign: "left"}}>Disabled apps: </th>
            <td>{appState.config.disabledApps.length > 0 ? appState.config.disabledApps.join(",") : "NONE"}</td>
        </tr>
    </table>) : (<table  onClick={()=>setShowConfigInLine(true)}>
        <tr>
            <th style={{textAlign: "left"}}>URL</th>
            <td><a href={appState.config.channelUrl} style={{color: "white"}} target={"_blank"}>{appState.config.channelUrl}</a></td>
        </tr>
        <tr>
            <th  style={{textAlign: "left"}}>Path</th>
            <td>{appState.config.channelPath}</td>
        </tr>
        <tr>
            <th  style={{textAlign: "left"}}>Auto reconfigure on update</th>
            <td>{appState.config.autoConfiguration ? "ON" : "OFF"}</td>
        </tr>
        <tr>
            <th  style={{textAlign: "left"}}>Force update on start</th>
            <td>{appState.config.autoUpdateOnStart ? "ON" : "OFF"}</td>
        </tr>
        <tr>
            <th  style={{textAlign: "left"}}>Auto update enabled</th>
            <td>{appState.config.autoUpdate > 0 ? "every " + appState.config.autoUpdate + "s" : "OFF"}</td>
        </tr>
        <tr>
            <th  style={{textAlign: "left"}}>Disabled apps</th>
            <td>{appState.config.disabledApps.length > 0 ? appState.config.disabledApps.join(",") : "NONE"}</td>
        </tr>
    </table>) ;

}
function App() {

    const [appState, setAppState] = React.useState({connected: 0});
    const [scripts, setScripts] = React.useState(List());

    const [autoScroll, setAutoScroll] = React.useState(false);

    const [logState, onScriptOutput, onScriptReset, onSetCollapsed] = useLogModel();
    const [logView, setLogView] = useState(null);
    const [checkingForUpdateDisabled, setCheckingForUpdateDisabled] = useState(false);
    const [installingUpdateDisabled, setInstallingUpdateDisabled] = useState(false);

    useEffect(()=>{
        if (appState.connected === 1)
        {
            setCheckingForUpdateDisabled(appState.data.some(row => row.updateState.updateState === "checking_for_update"));
            setInstallingUpdateDisabled(!(appState.data.every(row => row.updateState.updateState === "ready" || row.updateState.updateState === "updated" ||
                row.updateState.updateState === "installation_pending") && appState.data.some(row => row.updateState.updateState === "installation_pending")));
        }
        else {
            setCheckingForUpdateDisabled(true);
            setInstallingUpdateDisabled(true);
        }
    }, [appState]);

    const visibleView = React.useMemo(() => {
        function setDefaultView() {
            for (const key of logState.keys()) {
                if (key === "$$updated$$") {
                    continue;
                }

                return key;
            }

            return null;
        }

        if (logView === null) {
            return setDefaultView();
        } else if (!logState.has(logView)) {
            return setDefaultView();
        } else {
            console.log("visibleView: ", logView);
            return logView;
        }
    }, [logView, logState]);

    const visibleLog = useMemo(() => logState.get(visibleView), [visibleView, logState]);

    const webSocket = useWebSocket(setAppState, onScriptOutput, onScriptReset, setScripts);
    const buildInfo = useConst(() => window.watchdogBuild || "");

    return (
        <Layout>
            <MessageBar
                messageBarType={MessageBarType.info}
                isMultiline={true}

            >
                Watchdog: {buildInfo}
            </MessageBar>
            <div>
                {appState.connected === 1 && <Config appState={appState}/>}
            </div>
            <div style={{padding: 5}}>&nbsp;</div>
            {
                appState.connected < 0 && <MessageBar
                    messageBarType={MessageBarType.error}
                    isMultiline={false}
                    onDismiss={() => window.location.reload(true)}
                    dismissButtonAriaLabel="Refresh"

                >
                    The connection to the server has been lost. Refresh the page and make sure that watchdog is running.

                </MessageBar>
            }
            {
                appState.connected === 0 && <MessageBar
                    messageBarType={MessageBarType.info}
                    isMultiline={false}
                >
                    Connecting to server ...
                </MessageBar>
            }
            {appState.connected === 1 &&
                <div style={{display: "flex", flexDirection: "row", flexGrow: 1, overflowY: "hidden"}}>

                    <Stack style={{overflowY: "auto", paddingRight: 25, minWidth: 550}}>
                        <Stack.Item>
                            <PrimaryButton onClick={()=>{
                                setCheckingForUpdateDisabled(true);
                                webSocket.checkAllUpdates();
                            }}
                           text="Check for updates"
                           iconProps={{iconName: "CloudDownload"}}
                           disabled={checkingForUpdateDisabled}
                            />
                            <DefaultButton onClick={()=>{
                                setCheckingForUpdateDisabled(true);
                                webSocket.installAllUpdates();
                            }}
                                           text="Install updates"
                                           iconProps={{iconName: "Installation"}}
                                           disabled={installingUpdateDisabled}/>
                            {/** <DefaultButton onClick={webSocket.revertAll.bind(webSocket)} text="Revert updates"
                             iconProps={{iconName: "Sad"}}/>*/}
                        </Stack.Item>
                        <Stack.Item>


                            <AppGroupList items={appState.data} webSocket={webSocket}/>


                        </Stack.Item>
                    </Stack>

                    <div style={{
                        flexGrow: 1,
                        borderLeft: "1px solid gray",
                        paddingLeft: 25,
                        minWidth: 550,
                        display: "flex",
                        flexDirection: "column"
                    }}>

                        <div style={{
                            borderBottom: "lightgray",
                            display: "flex",
                            flexDirection: "row",
                            zIndex: 10,
                            background: "rgb(27, 26, 25)"
                        }}>

                            {scripts.map((script) => <DefaultButton text={script.get("name")}
                                                                    key={script.get("name")}
                                                                    title={script.get("script")}
                                                                    onClick={() => webSocket.runScript(script.get("name"))}/>)}
                            <div style={{flexGrow: 1}}>&nbsp;</div>
                            <DefaultButton text={"Clear Log"}
                                           onClick={() => webSocket.clearLog()}/>
                            <DefaultButton text={autoScroll ? "Disable AutoScroll" : "Enable AutoScroll"}
                                           onClick={() => setAutoScroll(!autoScroll)}/>
                        </div>
                        <div style={{background: "#00001a", flexGrow: 1, overflowY: "auto"}}>

                            <LogView output={visibleView} log={visibleLog} setCollapsed={onSetCollapsed}
                                     autoScroll={autoScroll}/>
                        </div>
                        <Pivot onLinkClick={(evt) => {
                            setLogView(evt.props.itemKey);
                        }
                        } selectedKey={visibleView}>
                            {logState.mapKeys((key) => key === "$$updated$$" ? false :
                                <PivotItem key={key} itemKey={key}
                                           headerText={key}></PivotItem>)}
                        </Pivot>
                    </div>
                </div>

            }
        </Layout>
    );
}

export default App;
