import * as _ from "underscore";
import React from "react"
import {fromJS} from "immutable";

function getWebSocketUrl() {
    if (window.production) {
        return `ws://${window.location.host}/messages/`;
    } else {
        return "ws://127.0.0.1:8080/messages/";
    }
}

class WebSocketController {
    constructor(ws) {
        this._ws = ws;
    }

    installAllUpdates() {
        this._ws.send("install_all_updates");
    }

    checkAllUpdates() {
        this._ws.send("check_all_updates");
    }

    restart(appGroup) {
        this._ws.send("restart " + appGroup);
    }

    revert(appGroup) {
        if (window.confirm("Do you want to revert " + appGroup + " to previous version?\nThis operation is not considered to be safe.")) {
            this._ws.send("revert " + appGroup);
        }
    }

    revertAll() {
        if (window.confirm("Do you want to revert all apps to previous version?\nThis operation is not considered to be safe.")) {
            this._ws.send("revert_all");
        }
    }

    runScript(name) {
        this._ws.send("run_script " + name);

    }

    clearLog() {
        this._ws.send("clear_log");

    }

    checkForUpdate(appGroup) {
        this._ws.send("check_for_update " + appGroup);
    }

    installUpdate(appGroup) {
        this._ws.send("install_update " + appGroup);
    }
}

export default function useWebSocket(setAppState, onScriptOutput, onScriptReset, setScripts) {

    const [webSocket, setWebSocket] = React.useState(null);

    React.useEffect(() => {
        console.log("creating websocket connection");

        const ws = new WebSocket(getWebSocketUrl());
        ws.onmessage = (event) => {
            const data = JSON.parse(event.data);
            if (data.hasOwnProperty("script_output")) {
                onScriptOutput(data.script_output);
            } else if (data.hasOwnProperty("script_reset")) {
                onScriptReset(data.script_reset);
            } else {
                const items = [];

                _.each(data.updateInfo, (item, key) => {
                    let runInfo = data.runInfo[key];
                    if (!runInfo)
                        runInfo = {};

                    const appGroupRow = {appGroup: key, updateState: item};
                    let anyRunInfo = false;
                    _.each(runInfo, (appState, appKey) => {
                        anyRunInfo = true;
                        items.push({...appGroupRow, appKey, appState});
                    });

                    if (!anyRunInfo) {
                        items.push({...appGroupRow, appKey: "external", appState: {state: "unknown"}});
                    }
                });

                setAppState({connected: 1, data: items, config: {

                    autoUpdate: data.autoUpdate,
                        autoUpdateOnStart: data.autoUpdateOnStart,
                        channelPath: data.channelPath,
                        channelUrl: data.channelUrl,
                        disabledApps: data.disabledApps,
                        autoConfiguration: data.autoConfiguration
                    }});
            }

            if (data.hasOwnProperty("scripts")) {

                const result = [];
                _.each(data.scripts, (value, key) => {
                    result.push({name: key, script: value});
                });

                setScripts(fromJS(result));
            }

            if (data.hasOwnProperty("watchdog_name")) {
                const {watchdog_name} = data;
                if (_.isEmpty(watchdog_name)) {
                    window.document.title = `${window.location.host} | Watchdog`;
                } else {
                    window.document.title = `${watchdog_name} | Watchdog`;
                }
            }
        };

        ws.onclose = () => {
            setAppState(prevState => ({...prevState, connected: -1}));
        };

        ws.onerror = () => {
            setAppState(prevState => ({...prevState, connected: -1}));
        };

        setWebSocket(new WebSocketController(ws));
        return () => ws.close();
    }, []);


    return webSocket;
}