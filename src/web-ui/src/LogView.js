import React, {useMemo, useState} from "react"
import _ from "lodash";
import {fromJS, List, Map} from "immutable"
import {Button} from "@fluentui/react";
import {ActionButton, DefaultButton} from "office-ui-fabric-react";

function collapseAllStdOutputs(instance, size) {
    instance = instance.setIn([size - 1, "std_out_collapsed"], true);
    instance = instance.setIn([size - 1, "std_err_collapsed"], true);
    return instance.setIn([size - 1, "exception_collapsed"], true);
}

export function useLogModel() {
    const [state, setState] = useState(Map());


    const onScriptOutput = (data) => {
        setState(current =>
            current.updateIn([data.output_name], List(), output => {
                const lastIdx = output.findIndex((exec)=>exec.get("run_id") === data.run_id);
                const last = lastIdx >= 0 ? output.get(lastIdx): null;
                const size = output.size;

                if (!last) {
                    let instance = output;
                    if (size !== 0) {
                        instance = collapseAllStdOutputs(instance, size);
                    }
                    return instance.push(fromJS(data));

                } else {
                    const iData = fromJS(data);
                    return output.updateIn([lastIdx], (value) => value.mergeWith((oldVal, newVal, key) => {
                        if (key == "std_out" || key == "std_err") {
                            return oldVal + newVal;
                        } else {
                            return newVal;
                        }

                    }, iData));
                }

            }).set("$$updated$$", Date.now()));
    };

    const onScriptReset = (data) => {
        let indexed_outputs = Map();
        for (const out of data) {

            const script_output_data = fromJS(out);
            indexed_outputs = indexed_outputs
                .updateIn([out.output_name], List(), output =>
                output.push(script_output_data)
            );
        }


        for (const key of indexed_outputs.keys()) {
            const size = indexed_outputs.get(key).size;

            for (let i = 0, len = size - 1; i !== len; ++i) {
                indexed_outputs = indexed_outputs.setIn([key, i, "std_out_collapsed"], true)
                    .setIn([key, i, "std_err_collapsed"], true)
                    .setIn([key, i, "exception_collapsed"], true);
            }
        }

        setState(indexed_outputs.set("$$updated$$", Date.now()));
    }

    const onSetCollapsed = (output, key, index, value) => {
        setState(current => {
            console.log(current.toJS(), [output, index, key + "_collapsed"].join(","));
            return current.setIn([output, index, key + "_collapsed"], value).set("$$updated$$", Date.now());
        })
    }

    return [state, onScriptOutput, onScriptReset, onSetCollapsed]
}

function OutputView({name, output, collapsed, setCollapsed}) {
    const preparedOutput = useMemo(() => {
        if (collapsed) {
            if (!output) {
                return null;
            }
            const lines = output.split("\n");
            if (lines.length <= 10) {
                return output;
            }

            return lines.slice(Math.max(lines.length - 10, 0)).join("\n");
        } else {
            return output;
        }
    }, [collapsed, output]);

    return _.isEmpty(output) ? false :
        <div style={{borderBottom: "1px solid gray", padding: 10, marginTop: 10}}>
            <DefaultButton onClick={() => setCollapsed(!collapsed)}>{collapsed ? "+" : "-"}{name}</DefaultButton>

            {collapsed && preparedOutput != output &&
                <pre onDoubleClick={() => setCollapsed(!collapsed)} style={{color: "gray"}}>
                          .
                          .
                          .
                      </pre>}

            <pre onDoubleClick={() => setCollapsed(!collapsed)}>{preparedOutput}</pre>
        </div>
}

export function LogView({log, setCollapsed, autoScroll, output}) {
    const [ref, setRef] = useState(null);

    React.useEffect(() => {
        if (autoScroll) {
            const iterval = setInterval(() => {
                if (ref)
                    ref.scrollIntoView(true);
            }, 50);

            return () => {
                clearInterval(iterval);
            }
        }
    }, [ref, autoScroll]);

    return <>{log && log.map((value, key) => {
        const scriptExecution = value.toJS();

        return <div key={key}>
            <div>{scriptExecution.name}&nbsp;
                <span
                    style={scriptExecution.exit_code === null ? {} : {color: "gray"}}>[{scriptExecution.pid}]</span>&nbsp;
                => {scriptExecution.exit_code !== null && (
                    <span
                        style={{color: scriptExecution.exit_code === 0 ? "lightgreen" : "red"}}> Exited: [{scriptExecution.exit_code}]</span>)}
                {scriptExecution.error_message && <span style={{color: "red"}}>Unable to start: {scriptExecution.error_message}</span>}
            </div>

            <div style={{color: "gray"}}>{scriptExecution.command}</div>
            <OutputView name="STD OUTPUT" output={scriptExecution.std_out} collapsed={scriptExecution.std_out_collapsed}
                        setCollapsed={(val) => setCollapsed(output, "std_out", key, val)}/>
            <OutputView name="STD ERROR" output={scriptExecution.std_err} collapsed={scriptExecution.std_err_collapsed}
                        setCollapsed={(val) => setCollapsed(output, "std_err", key, val)}/>
            <OutputView name="EXECUTION EXCEPTION" output={scriptExecution.error_message}
                        collapsed={scriptExecution.exception_collapsed}
                        setCollapsed={(val) => setCollapsed(output, "exception", key, val)}/>
        </div>
    })}

        <div ref={(ref) => setRef(ref)}></div>
    </>
}