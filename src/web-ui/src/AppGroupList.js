import React from "react"
import {useConst} from "@uifabric/react-hooks";
import {
    DetailsList,
    DetailsRow,
    GroupedList,
    SelectionZone,
    Selection,
    GroupHeader,
    Stack,
    PrimaryButton, DefaultButton, ActionButton, IconButton
} from 'office-ui-fabric-react';
import {SelectionMode} from "@uifabric/utilities";

function getDumpUrl() {
    if (window.production) {
        return `http://${window.location.host}/memorydump/`;
    } else {
        return "http://127.0.0.1:8080/memorydump/";
    }
}

function humanFileSize(bytes, si = false, dp = 1) {
    const thresh = si ? 1000 : 1024;

    if (Math.abs(bytes) < thresh) {
        return bytes + ' B';
    }

    const units = si
        ? ['kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
        : ['KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB'];
    let u = -1;
    const r = 10 ** dp;

    do {
        bytes /= thresh;
        ++u;
    } while (Math.round(Math.abs(bytes) * r) / r >= thresh && u < units.length - 1);


    return bytes.toFixed(dp) + ' ' + units[u];
}

export function DownloadProgress({updateState}) {
    const {pendingSize, downloadedSize, downloadingVersionString, downloadingVersion} = updateState;
    if (pendingSize === undefined) {
        return false;
    }
    return (<span
        title={downloadingVersionString}>{downloadingVersion || downloadingVersionString} {humanFileSize(pendingSize, true)} {downloadedSize > 0 && (downloadedSize / pendingSize * 100).toFixed(1) + "%"}</span>);
}

export function AppGroupList({items, webSocket}) {
    const [groups, setGroups] = React.useState(null);
    React.useEffect(() => {
        const groups = [];
        const createGroup = (name, data) => {
            const group = {key: name, name, count: 0, startIndex: i, data};
            groups.push(group);
            return group;
        };


        let i = 0;
        let group = null;
        for (const item of items) {
            if (group === null || item.appGroup !== group.key) {
                group = createGroup(item.appGroup, item);
            }

            ++group.count;
            ++i;
        }

        setGroups(groups);
    }, [items]);


    const columns = useConst(() => {
        return [
            {
                key: "appKey", name: "appKey",

                onRender(item) {

                    return <div style={{minWidth: 200, borderRight: "1px solid gray"}}>{item.appKey}</div>;
                }
            },
            {
                key: "appState", name: "appState",
                onRender(item) {
                    if (item.appState.state === "unknown") {
                        return <div style={{
                            color: "gray",
                            minWidth: 75,
                            borderRight: "1px solid gray"
                        }}>{item.appState.state}</div>;
                    }
                    return <div style={{
                        color: item.appState.state === "running" ? "gray" : "red",
                        minWidth: 75,
                        borderRight: "1px solid gray"
                    }}>{item.appState.state}</div>;
                }
            },
            {
                key: "pid", name: "pid",
                onRender(item) {
                    if (item.appState.state === "unknown") {
                        return "";
                    }
                    return (item.appState.state === "running" &&
                            <> <span style={{color: "gray"}}>PID: {item.appState.pid} </span>
                                <IconButton onClick={() => {
                                    if (window.confirm("Do you really want to pause process and do memory dump? " + item.appKey)) {
                                        fetch(getDumpUrl(), {
                                            body: JSON.stringify({app_group: item.appGroup, app: item.appKey}),
                                            method: 'POST', // *GET, POST, PUT, DELETE, etc.
                                            mode: 'cors', // no-cors, *cors, same-origin
                                            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
                                            credentials: 'same-origin', // include, *same-origin, omit
                                            headers: {
                                                'Content-Type': 'application/json'
                                                // 'Content-Type': 'application/x-www-form-urlencoded',
                                            },
                                        }).then((response) => response.json()).then((json) => {
                                            alert(json.result);
                                        })
                                            .catch(() => {
                                                alert("error occured check the watchdog log file");
                                            });
                                    }
                                }} iconProps={{iconName: "Bug"}} title={"Memory Dump"}
                                            styles={{root: {float: "right", marginTop: -9}}}/> </>)
                        || <span style={{color: "red"}}> Terminated: {item.appState.counter}</span>;
                }
            }
        ]
    });

    const customStyles = {root: {width: "100%", height: "32px"}};

    const onRenderCell = (nestingDepth, item, itemIndex) => {
        return item && typeof itemIndex === 'number' && itemIndex > -1 ? (
            <DetailsRow
                columns={columns}
                groupNestingDepth={nestingDepth}
                item={item}
                itemIndex={itemIndex}

                selectionMode={SelectionMode.none}
                compact={true}
                styles={customStyles}
            />
        ) : null;
    };

    return (
        <GroupedList
            items={items}
            // eslint-disable-next-line react/jsx-no-bind
            onRenderCell={onRenderCell}

            selectionMode={SelectionMode.none}
            groups={groups}
            groupProps={{
                onRenderHeader: (props) => {
                    if (!props.group)
                        return;
                    const hasPendingInstallation = !!props.group.data.updateState.pendingVersionString;
                    const hasLastError = !!props.group.data.updateState.lastError;
                    return <GroupHeader {...props} style={{root: {backgroundColor: "black"}}} onRenderTitle={() =>
                        <Stack horizontal style={{width: "100%"}}>
                            <Stack.Item>
                                <IconButton onClick={() => webSocket.restart(props.group.name)} title="Restart"
                                            iconProps={{iconName: "Refresh"}}/>
                            </Stack.Item>
                            <Stack.Item>
                                <div style={{marginTop: 10}}>
                                    {props.group.name} - <span style={{color: "gray"}}
                                                               title={props.group.data.updateState.currentVersionString}>{props.group.data.updateState.updateState} / {props.group.data.updateState.currentVersion || props.group.data.updateState.currentVersionString} </span>
                                    <DownloadProgress updateState={props.group.data.updateState}/>

                                    {hasPendingInstallation && <span className={{color: "white"}}
                                                                     title={props.group.data.updateState.pendingVersionString}> Pending: {props.group.data.updateState.pendingVersion || props.group.data.updateState.pendingVersionString}</span>}
                                    {hasLastError && <span
                                        className={{color: "red"}}> Pending: {props.group.data.updateState.lastError} </span>}
                                </div>
                            </Stack.Item>
                        </Stack>}/>;
                }
            }}
            compact={true}
        />
    )

}