# Channel.properties - file description

The file is checked regularly for changes and the changes are applied without the need to restart the application.
The file is located in the root of the channel directory.

Watchdog makes sure that it will apply the changes to the running applications without the need to restart watchdog.

## Directory structure relative to the channel root

    # directory containing the configuration files for the applications in the channel
    # applications configuration files are in the stored in of <cfg.repository>\<appname>\env.properties
    # every application has its own directory and env.properties file must be present
    cfg.repository = config

    # directory containing the binaries for the applications in the channel
    # it contains active.properties file for each application where the link to the actual application folder is stored
    # it points to versions directory
    app.repository = bin
    
    # directory containing the logs for the applications in the channel
    app.logging.path = logs
    
    # directory containing the data for the applications in the channel
    app.data.path = data

    # directory containing scripts that are exectuted on updated
    utils = C:\watchdog\utils\Dev

## Update behavior

    # update url - for the channel
    update.url = http://termix-cache.vitrium.com/channel/charlie/
    # update interval in seconds
    update.interval = 0
    
    # timeout for application to stop in seconds
    kill.timeout = 10
    
    # force update on start before any application is started
    update.force.on.start = false
    
    # allow install new applications that are present in the channel but not installed yet
    update.url.init = true
    
    # allow update configuration files be regenerated configuration based on specified tokens
    update.configuration = true

## Application behavior

    # installed applications that are disabled
    # this is the way to temporarily disable an application
    disabled.apps = infmon

## User specification (advanced)

Windows user account must have permission to run the applications and access the resources that the applications are
using.
Do not specify different users if you are running watchdog in command line. It is not working properly.

For the watchdog running under service mode ensure that user has the proper permissions to run the applications and
access the resources that the applications are using.
[Local Security Policy - Log on as a service](https://learn.microsoft.com/en-us/system-center/scsm/enable-service-log-on-sm?view=sc-sm-2022)

If you are running watchdog in command line mode, you can specify the user that is going to be used to run the
applications.

Interactive logon is discouraged and it is not recommended to use it in the production environment.

    # user that is used to run the applications - it applies only to Windows
    # Format: [domain\]username[?interactive]:password
    # domain is optional, interactive is optional and password is required
    # https://learn.microsoft.com/en-us/windows/win32/api/winbase/nf-winbase-logonusera
    # ?interactive - Uses LOGON32_LOGON_INTERACTIVE.
    # otherwise LOGON32_LOGON_SERVICE is used -> please set LocalSecurityPolicy as

    # user for service mode - used to run the applications
    user.my_reference_user = MyAD\testuser:testuser
   
    user.my_testing_user = MyAD\testuser?interactive:testuser
    
    # pick apps that are going to be run under the specified account
    user.<watchdog user>.apps = <app1>,<app2>
    
    # example - run infmon under the specified account
    user.my_reference_user.apps = infmon
    
    # pick all apps to be run under the specified account
    user.my_reference_user.apps = all

## Tokens

    # token to be used for the update
    # overriding system provided token extracted as last directory in the update.url
    token.channel=my-charlie
    # any custom token that can be used in the application configuration
    token.custom_token=xyz


    


