#include "app_group_config.h"
#include <boost/dll/runtime_symbol_info.hpp>
#include <boost/filesystem/fstream.hpp>
#include <commons/make_string.h>
#include <commons/observable_model.h>
#include <commons/properties_parser.h>
#include <commons/uri_builder.h>
#include <easylogging++.h>
#include <mstch/mstch.hpp>
#include <regex>

#include <cpprest/asyncrt_utils.h>

using namespace commons;

namespace
{
   std::string get_default_watchdog_name()
   {
      std::string watchdog_name;

      if (auto *hostname = std::getenv("COMPUTERNAME"); hostname != nullptr)
      {
         watchdog_name = {hostname};
      }
      else if (auto *hostname = std::getenv("HOSTNAME"); hostname != nullptr)
      {
         watchdog_name = {hostname};
      }

      return watchdog_name;
   }
} // namespace

watchdog::app_group_config::app_group_config(watchdog::app_base_config config) : config_(config) {}

watchdog::system_updater_config watchdog::app_group_config::get_config(const std::string &app_name, bool silent) const
{

   using namespace cfg;

   const auto config_file = config_.legacy_mode ? config_.config_repo_path / property_file
                                                : config_.config_repo_path / app_name / property_file;

   system_updater_config updater_config;
   updater_config.app_group_name = app_name;

   const auto env_properties = parse_config_file(config_file);

   updater_config.url = get_map_value(
       env_properties, update_url,
       config_.legacy_mode
           ? std::string{}
           : commons::url::uri_builder(config_.channel_base_url).make_relative(app_name + "/").to_string());

   auto default_status_url =
       config_.status_base_url.empty()
           ? std::string{}
           : commons::url::uri_builder(config_.status_base_url).make_relative(app_name + "/").to_string();

   updater_config.status_url =
       get_map_value(env_properties, update_status_url, config_.legacy_mode ? std::string{} : default_status_url);
   updater_config.remote_update_synchronization_enabled =
       get_bool_map_value(env_properties, update_remote_synchronization_enabled);

   std::string update_stop = get_map_value(env_properties, update_stop_apps, update_stop_apps_after_update);
   updater_config.stop_before_update = update_stop != update_stop_apps_after_update;

   const int interval = get_int_map_value(env_properties, update_interval);
   updater_config.update_check_interval =
       interval == 0 ? config_.update_check_interval : std::chrono::seconds{std::max(interval, 10)};

   const int kill_timeout_prop = get_int_map_value(env_properties, kill_timeout);
   updater_config.kill_timeout =
       kill_timeout_prop == 0 ? config_.kill_timeout : std::chrono::seconds{kill_timeout_prop};

   updater_config.apps_path = get_map_value(env_properties, app_repository,
                                            config_.legacy_mode ? config_.app_repo_path.string()
                                                                : (config_.app_repo_path / app_name).string());
   updater_config.env_path = config_file;
   updater_config.env_dir_path = config_file.parent_path().string();
   updater_config.app_data_path = config_.app_data_path;

   updater_config.logging_dir_path =
       get_map_value(env_properties, app_logging_path, (config_.app_logging_path / app_name).string());

   updater_config.current_version_string = get_current_version(updater_config);

   updater_config.script_after_app_fail = get_map_value(env_properties, script_after_app_fail);
   updater_config.update_force_on_start =
       config_.update_force_on_start || get_bool_map_value(env_properties, update_force_on_start);
   updater_config.app_mode = get_map_value(env_properties, app_mode);
   updater_config.exit_mode = get_map_value(env_properties, exit_mode);

   boost::replace_all(updater_config.exit_mode, "\\r", "\r");
   boost::replace_all(updater_config.exit_mode, "\\n", "\n");
   boost::replace_all(updater_config.exit_mode, "\\t", "\t");
   boost::replace_all(updater_config.exit_mode, "\\b", "\b");
   boost::replace_all(updater_config.exit_mode, "\\v", "\v");
   boost::replace_all(updater_config.exit_mode, "\\a", "\a");
   boost::replace_all(updater_config.exit_mode, "\\f", "\f");
   boost::replace_all(updater_config.exit_mode, "\\\\", "\\");

   if (updater_config.status_url.empty())
   {
      LOG_IF(!silent, INFO) << app_name << ": " << update_status_url << " not found";
      updater_config.remote_update_synchronization_enabled = false;
   }

   if (updater_config.remote_update_synchronization_enabled)
      LOG_IF(!silent, INFO) << app_name << ": 'update.remote.synchronization.enabled' is enabled";

   if (updater_config.stop_before_update)
      LOG_IF(!silent, INFO) << app_name << ": " << update_stop_apps << " is set to " << update_stop_apps_before_update;

   LOG_IF(!silent, INFO)
       << app_name << ": settings\n"
       << update_url << " = " << updater_config.url << "\n"
       << update_interval << " = "
       << std::chrono::duration_cast<std::chrono::seconds>(updater_config.update_check_interval).count()
       << "\nactive version => " << updater_config.current_version_string;

   auto seconds = std::chrono::duration_cast<std::chrono::seconds>(updater_config.update_check_interval).count();
   if (seconds < 60 && seconds > 0)
   {
      LOG_IF(!silent, WARNING)
          << app_name << ": update interval too short "
          << std::chrono::duration_cast<std::chrono::seconds>(updater_config.update_check_interval).count()
          << " seconds";
   }
   else if (seconds == 0)
   {
      LOG_IF(!silent, INFO) << app_name << ": checking for updates is disabled ";
   }

   updater_config.utils_path = config_.utils_path;

   for (auto &instance : get_instances(app_name))
   {
      updater_config.instances[instance] = get_instance_config(updater_config, app_name, instance);
      LOG_IF(!silent, INFO) << app_name << ": loaded instance configuration " << instance;
   }

   updater_config.users = config_.users;

   return updater_config;
}

std::vector<std::string> watchdog::app_group_config::get_instances(const std::string &app_name) const
{
   using namespace cfg;

   std::vector<std::string> result;
   if (!config_.legacy_mode)
   {
      const auto instances_folder = config_.config_repo_path / app_name / "instances";
      if (boost::filesystem::exists(instances_folder))
      {
         for (auto &file : boost::filesystem::recursive_directory_iterator(instances_folder))
         {
            const auto &file_path = file.path();
            if (file_path.filename() == property_file)
            {
               result.push_back(file_path.parent_path().filename().string());
            }
         }
      }
   }

   return result;
}

std::string watchdog::app_group_config::get_current_version(system_updater_config const &config) const
{
   return get_map_value(parse_config_file(config.apps_path / "active.properties"), "current.version");
}

watchdog::system_updater_config watchdog::app_group_config::get_instance_config(system_updater_config const &base,
                                                                                const std::string &app_name,
                                                                                const std::string &instance_name) const
{
   using namespace cfg;

   auto result = base;
   result.env_path = config_.config_repo_path / app_name / "instances" / instance_name / property_file;

   result.env_dir_path = result.env_path.parent_path().string();

   const auto env_properties = parse_config_file(result.env_path);

   result.logging_dir_path =
       get_map_value(env_properties, app_logging_path,
                     (config_.app_logging_path / fmt::format("{}-{}", app_name, instance_name)).string());

   return result;
}

void make_canonincal(watchdog::filesystem_path const &config_dir, watchdog::filesystem_path &path)
{
   boost::system::error_code ec;
   if (!path.is_absolute() && !path.empty())
   {
      path = config_dir / path;
   }

   auto result = boost::filesystem::weakly_canonical(path, config_dir, ec);
   if (!ec)
   {
      path = result.make_preferred();
   }
   else
   {
      LOG(ERROR) << "boost::filesystem::weakly_canonical has failed with " << ec.to_string()
                 << " base_path=" << config_dir.string() << " path=" << path.string();
   }
}

watchdog::app_base_config watchdog::parse(filesystem_path const &config_path, std::optional<app_base_config> parent)
{
   using namespace commons;
   using namespace cfg;
   using namespace boost::algorithm;

   const auto channel_properties = parse_config_file(config_path);

   const auto config_dir = config_path.parent_path();

   app_base_config config;

   if (parent.has_value())
   {
      config.legacy_mode = parent->legacy_mode;
      config.config_repo_path =
          get_map_value(channel_properties, cfg::cfg_repository, parent->config_repo_path.string());
      config.app_repo_path = get_map_value(channel_properties, cfg::app_repository, parent->app_repo_path.string());

      const auto utils_path = boost::dll::program_location().parent_path() / "utils";
      const auto utils_value = get_map_value(channel_properties, utils);

      boost::system::error_code ec;
      config.utils_path = boost::filesystem::canonical(utils_value, utils_path, ec);

      if (ec)
      {
         LOG(WARNING) << "utils = " << utils_value << " is invalid : " << ec.message() << "\nMake sure that directory "
                      << (filesystem_path{utils_value}.is_absolute() ? utils_value
                                                                     : (utils_path / utils_value).string())
                      << " exists.";
         config.utils_path = utils_value;
      }

      config.app_logging_path =
          get_map_value(channel_properties, cfg::app_logging_path, parent->app_logging_path.string());
      config.app_data_path = get_map_value(channel_properties, cfg::app_data_path, parent->app_data_path.string());

      config.channel_base_url = get_map_value(channel_properties, cfg::update_url, parent->channel_base_url);
      config.status_base_url = get_map_value(channel_properties, cfg::update_status_url, parent->status_base_url);

      int interval =
          get_int_map_value(channel_properties, cfg::update_interval,
                            std::chrono::duration_cast<std::chrono::seconds>(parent->update_check_interval).count());
      config.update_check_interval =
          interval == 0 ? std::chrono::seconds{0} : std::chrono::seconds{std::max(interval, 10)};

      config.kill_timeout = std::chrono::seconds{
          std::max(get_int_map_value(channel_properties, cfg::kill_timeout,
                                     std::chrono::duration_cast<std::chrono::seconds>(parent->kill_timeout).count()),
                   10)};

      config.update_force_on_start =
          get_bool_map_value(channel_properties, cfg::update_force_on_start, parent->update_force_on_start);
      config.update_url_init = get_bool_map_value(channel_properties, cfg::update_url_init, parent->update_url_init);
      config.auto_configuration =
          get_bool_map_value(channel_properties, cfg::update_configuration, parent->auto_configuration);
      config.auto_update_enabled =
          get_bool_map_value(channel_properties, cfg::update_auto_enabled, parent->auto_update_enabled);

      auto disabled_apps_property = get_map_value(channel_properties, cfg::disabled_apps);
      if (disabled_apps_property.empty())
      {
         config.disabled_apps = parent->disabled_apps;
      }
      else
      {
         split(config.disabled_apps, disabled_apps_property, is_any_of(" ,.;"), token_compress_on);
      }

      config.config_tokens = parent->config_tokens;
      config.channel_path = parent->channel_path;

      //    LOG(INFO) << "channel: " << parent->channel_path.string();
   }
   else
   {
      ///  LOG(INFO) << "channel: " << config_dir.string();

      config.channel_path = config_dir;
      config.config_repo_path = get_map_value(channel_properties, cfg::cfg_repository, config_dir.string());
      config.app_repo_path = get_map_value(channel_properties, cfg::app_repository, config_dir.string());

      const auto utils_path = boost::dll::program_location().parent_path() / "utils";
      const auto utils_value = get_map_value(channel_properties, utils);

      boost::system::error_code ec;
      config.utils_path = boost::filesystem::canonical(utils_value, utils_path, ec);

      if (ec)
      {
         LOG(WARNING) << "utils = " << utils_value << " is invalid : " << ec.message() << "\nMake sure that directory "
                      << (filesystem_path{utils_value}.is_absolute() ? utils_value
                                                                     : (utils_path / utils_value).string())
                      << " exists.";

         config.utils_path = utils_value;
      }

      config.app_logging_path =
          get_map_value(channel_properties, cfg::app_logging_path, (config.app_repo_path / "logs").string());
      config.app_data_path =
          get_map_value(channel_properties, cfg::app_data_path, (config.app_repo_path / "data").string());

      config.channel_base_url = get_map_value(channel_properties, cfg::update_url);
      config.status_base_url = get_map_value(channel_properties, cfg::update_status_url);

      auto interval = get_int_map_value(channel_properties, cfg::update_interval, 60);
      if (interval == 0)
      {
         config.update_check_interval = std::chrono::seconds{0};
      }
      else
      {
         config.update_check_interval = std::chrono::seconds{std::max(interval, 10)};
      }

      config.kill_timeout =
          std::chrono::seconds{std::max(get_int_map_value(channel_properties, cfg::kill_timeout, 15), 10)};

      config.update_force_on_start = get_bool_map_value(channel_properties, cfg::update_force_on_start);

      config.update_url_init = get_bool_map_value(channel_properties, cfg::update_url_init, true);
      config.auto_configuration = get_bool_map_value(channel_properties, cfg::update_configuration, true);
      config.auto_update_enabled = get_bool_map_value(channel_properties, cfg::update_auto_enabled, false);

      auto disabled_apps_property = get_map_value(channel_properties, cfg::disabled_apps);

      split(config.disabled_apps, disabled_apps_property, is_any_of(" ,.;"), token_compress_on);
   }

   const std::string_view token_string = "token.";
   for (auto const &[key, value] : channel_properties)
   {
      if (boost::starts_with(key, token_string))
      {
         config.config_tokens[key.substr(token_string.size())] = value;
      }
   }

   if (config.update_check_interval == std::chrono::seconds{0})
   {
      config.update_force_on_start = false;
   }

   make_canonincal(config_dir, config.config_repo_path);
   make_canonincal(config_dir, config.app_data_path);
   make_canonincal(config_dir, config.app_repo_path);
   make_canonincal(config_dir, config.app_logging_path);

   for (auto &[key, value] : channel_properties)
   {
      if (boost::starts_with(key, cfg::adv_updater_script_prefix))
      {
         auto script_name = key.substr(strlen(cfg::adv_updater_script_prefix));
         if (!script_name.empty())
         {
            config.scripts.scripts[script_name] = value;
         }
      }
   }

   auto default_watchdog_name = get_default_watchdog_name();
   config.watchdog_name = get_map_value(channel_properties, cfg::watchdog_name, default_watchdog_name);

   auto user_mapping = std::unordered_map<std::string, std::size_t>{};
   const string_view user_prefix = "user.";
   for (auto &[key, value] : channel_properties)
   {
      if (boost::starts_with(key, user_prefix) && std::ranges::count(key, '.') == 1)
      {
         if (auto user_name = key.substr(user_prefix.size()); !user_name.empty())
         {
            if (auto user_val = os_user::parse(value); user_val.has_value())
            {
               user_mapping[user_name] = config.users.size();
               config.users.push_back({user_val.value(), {}});
            }
            else
            {
               LOG(WARNING) << "Invalid user configuration: " << key << " = " << value;
            }
         }
      }
   }

   auto parts = std::vector<std::string>{};
   for (auto &[key, value] : channel_properties)
   {
      if (boost::starts_with(key, user_prefix) && std::ranges::count(key, '.') == 2)
      {
         parts.clear();
         boost::split(parts, key, boost::is_any_of("."));

         if (auto user_name = parts.at(1); !user_name.empty())
         {
            if (auto it = user_mapping.find(user_name); it != user_mapping.end())
            {
               parts.clear();
               boost::split(parts, value, boost::is_any_of(" ,.;"));
               auto &user = config.users.at(it->second);

               if (std::ranges::find(parts, "all") != parts.end())
               {
                  user.run_all = true;
               }
               else
               {
                  for (auto &app : parts)
                  {
                     user.app_groups.insert(app);
                  }
               }
            }
         }
      }
   }

   return config;
}

namespace
{

   template <class T>
   void write(std::ostream &o, const char *key, T const &value)
   {
      o << key << " = " << value << "\n";
   }

} // namespace

bool path_contains_path(boost::filesystem::path dir, boost::filesystem::path file)
{

   // If dir ends with "/" and isn't the root directory, then the final
   // component returned by iterators will include "." and will interfere
   // with the std::equal check below, so we strip it before proceeding.
   if (dir.filename() == ".") dir.remove_filename();

   // If dir has more components than file, then file can't possibly
   // reside in dir.
   auto dir_len = std::distance(dir.begin(), dir.end());
   auto file_len = std::distance(file.begin(), file.end());
   if (dir_len > file_len) return false;

   // This stops checking when it reaches dir.end(), so it's OK if file
   // has more directory components afterward. They won't be checked.
   return std::equal(dir.begin(), dir.end(), file.begin());
}

boost::filesystem::path try_relative(boost::filesystem::path dir, boost::filesystem::path file)
{
   namespace fs = boost::filesystem;

   dir = fs::canonical(dir);
   file = fs::canonical(file);

   if (path_contains_path(dir, file))
   {
      return fs::relative(file, dir);
   }
   else
   {
      return file;
   }
}

void watchdog::write_config(app_base_config const &config, filesystem_path const &config_path)
{
   boost::filesystem::ofstream out{config_path};

   auto config_dir = config_path.parent_path();

   namespace fs = boost::filesystem;

   write(out, cfg::update_url, config.channel_base_url);
   write(out, cfg::update_status_url, config.status_base_url);
   out << "\n";

   write(out, cfg::cfg_repository, try_relative(config_dir, config.config_repo_path).string());
   write(out, cfg::app_repository, try_relative(config_dir, config.app_repo_path).string());
   write(out, cfg::app_logging_path, try_relative(config_dir, config.app_logging_path).string());
   write(out, cfg::app_data_path, try_relative(config_dir, config.app_data_path).string());

   write(out, cfg::utils, config.utils_path.string());
   out << "\n";
   write(out, cfg::update_interval,
         std::chrono::duration_cast<std::chrono::seconds>(config.update_check_interval).count());

   write(out, cfg::kill_timeout, std::chrono::duration_cast<std::chrono::seconds>(config.kill_timeout).count());

   write(out, cfg::update_force_on_start, config.update_force_on_start ? "true" : "false");
   write(out, cfg::update_url_init, config.update_url_init ? "true" : "false");
   write(out, cfg::update_configuration, config.auto_configuration ? "true" : "false");
   write(out, cfg::update_auto_enabled, config.auto_update_enabled ? "true" : "false");

   if (get_default_watchdog_name() != config.watchdog_name)
   {
      write(out, "watchdog.name=", config.watchdog_name);
   }
   else
   {
      write(out, "#watchdog.name=", config.watchdog_name);
   }

   if (!config.disabled_apps.empty())
   {
      write(out, cfg::disabled_apps, boost::join(config.disabled_apps, " "));
   }

   out << "\n";
   write(out, "#token.channel=", app_group_config::parse_channel(config.channel_base_url));
   out << "\n";

   for (auto const &[key, value] : config.config_tokens)
   {
      std::string out_key = "token." + key;
      write(out, out_key.c_str(), value);
   }
   out << "\n";
}

std::string watchdog::app_group_config::parse_channel(const std::string &url)
{
   auto path = utility::conversions::to_utf8string(commons::url::uri_builder{url}.to_uri().path());

   std::regex re{R"regex(\/([^/]+)\/?$)regex"};
   auto begin = std::sregex_iterator(path.begin(), path.end(), re);
   auto end = std::sregex_iterator();

   if (begin != end)
   {
      auto match = *begin;
      auto channel = match.str();
      boost::trim_if(channel, boost::is_any_of("/"));
      return channel;
   }
   return {};
}

watchdog::string_map watchdog::app_base_config::get_script_token_map(std::optional<std::string> app_name) const
{
   auto result = string_map{{"appLoggingPath", app_logging_path.string()},
                            {"appRepoPath", app_repo_path.string()},
                            {"utilsPath", utils_path.string()},
                            {"configRepoPath", config_repo_path.string()},
                            {"appDataPath", app_data_path.string()},
                            {"channel", app_group_config::parse_channel(channel_base_url)},
                            {"channelPath", channel_path.string()},
                            {"app", app_name.value_or(std::string{})}};

   for (auto &[key, value] : config_tokens)
   {
      result[key] = value;
   }

   for (auto &[key, value] : config_tokens)
   {
      result[key] = command::expand_tokens(value, &result, true, false);
   }

   return result;
}
