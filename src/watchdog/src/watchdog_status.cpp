//
// Created by jhrub on 10.11.2020.
//
#include "watchdog_status.h"
#include <boost/algorithm/string/join.hpp>
#include <commons/make_string.h>
#include <easylogging++.h>
#include <serialization/json/buffer.h>

using namespace watchdog;
using namespace std::chrono_literals;

void watchdog_status::print_current_status()
{
   std::lock_guard lock{mutex_};
   std::chrono::steady_clock::time_point zero;
   auto now = std::chrono::steady_clock::now();

   std::stringstream str;
   for (auto &[app_group, apps] : running_apps)
   {
      str << app_group;

      std::vector<std::string> error_apps;
      int running = 0;
      for (auto &app : apps)
      {
         auto &app_counter = app_counters[key({app_group, app})];

         if (app_counter.error_state_counter > 0)
         {
            if (is_in_error_state(app_counter, now))
            {
               error_apps.push_back(commons::make_string(app, " [exited ", app_counter.error_state_counter, "x]"));
            }
            else
            {
               app_counter.error_state_counter = 0;
            }
         }

         if (app_counter.error_state_counter == 0 && app_counter.pid_changed > app_counter.exit_code_changed)
         {
            ++running;
         }
      }
      str << ": running " << running;
      if (!error_apps.empty())
      {

         str << " : following apps might be stuck in run/exit loop -> " << boost::join(error_apps, ",") << "\n";
      }
      else
      {
         str << " : ok\n";
      }
   }

   LOG(INFO) << "watchdog status:\n" << str.str();
}

std::int64_t watchdog_status::get_pid(const std::string &group, const std::string &app_name)
{
   std::lock_guard lock{mutex_};
   if (auto it = app_counters.find(key({group, app_name})); it != app_counters.end())
   {
      return it->second.pid;
   }
   else
   {
      return -1;
   }
}

void watchdog_status::on_event(watchdog::status_event evt)
{
   std::lock_guard lock{mutex_};
   std::visit(*this, evt);
}

void watchdog_status::operator()(watchdog::started_event const &event)
{
   running_apps[event.app_group].insert(event.app_name);
   set_pid(event, event.pid);
}

void watchdog_status::operator()(watchdog::exited_event const &event)
{
   set_exit_code(event, event.code);
}

void watchdog_status::operator()(watchdog::not_started_event const &event)
{
   set_exit_code(event, -666);
}

void watchdog_status::operator()(const term_event &event)
{
   auto &counter = app_counters[key(event)];
   counter.terminating = true;
}

void watchdog_status::set_pid(watchdog::event_base const &event, std::int64_t pid)
{
   auto &counter = app_counters[key(event)];
   counter.pid = pid;
   counter.pid_changed = std::chrono::steady_clock::now();
}

void watchdog_status::set_exit_code(watchdog::event_base const &event, std::int64_t code)
{
   auto &counter = app_counters[key(event)];
   counter.exit_code = code;
   auto now = std::chrono::steady_clock::now();

   if (counter.terminating)
   {
      counter.error_state_counter = 0;
      counter.terminating = false;
   }
   else
   {
      if (is_in_error_state(counter, now))
      {
         ++counter.error_state_counter;
      }
      else
      {
         counter.error_state_counter = 0;
      }
   }
   counter.exit_code_changed = now;
}

void watchdog_status::clear_app_counter(const std::string &group)
{
   std::lock_guard lock{mutex_};
   for (auto const &app : running_apps[group])
   {
      auto &app_counter = app_counters[key({group, app})];
      app_counter.error_state_counter = 0;
   }
}

bool watchdog_status::is_in_error_state(const app_counter &counter, std::chrono::steady_clock::time_point now) const
{
   std::chrono::steady_clock::time_point zero;
   return counter.exit_code_changed != zero && now - counter.exit_code_changed <= 60s;
}

std::string watchdog_status::key(watchdog::event_base const &event) const
{
   return event.app_group + "|" + event.app_name;
}

void watchdog_status::format_json(spider::serialization::json::json_buffer &output)
{
   std::lock_guard lock{mutex_};
   auto now = std::chrono::steady_clock::now();

   output.object(
       [&, this]() mutable
       {
          for (auto &kv : running_apps)
          {
             output.kv_pair_obj(
                 kv.first,
                 [&, this]() mutable
                 {
                    for (auto &app : kv.second)
                    {
                       output.kv_pair_obj(
                           app,
                           [&, this]() mutable
                           {
                              auto &app_counter = app_counters[key({kv.first, app})];

                              if (app_counter.error_state_counter > 0 && is_in_error_state(app_counter, now))
                              {
                                 output.kv_pair("state", "error");
                                 output.kv_pair_num("counter", app_counter.error_state_counter);
                                 output.kv_pair_num("changed", std::chrono::duration_cast<std::chrono::seconds>(
                                                                   now - app_counter.exit_code_changed)
                                                                   .count());
                              }
                              else
                              {
                                 app_counter.error_state_counter = 0;
                                 if (app_counter.terminating)
                                 {
                                    output.kv_pair("state", "terminating");
                                 }
                                 else
                                 {
                                    output.kv_pair("state", app_counter.pid_changed > app_counter.exit_code_changed
                                                                ? "running"
                                                                : "stopped");
                                 }

                                 output.kv_pair_num("changed", std::chrono::duration_cast<std::chrono::seconds>(
                                                                   now - app_counter.pid_changed)
                                                                   .count());

                                 output.kv_pair_num("pid", app_counter.pid);
                              }
                           });
                    }
                 });
          }
       });
}
