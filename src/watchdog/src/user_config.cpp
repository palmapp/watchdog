#include "user_config.h"

std::optional<watchdog::os_user> watchdog::try_to_find_user_for_app(const user_app_vec &users,
                                                                    const std::string &app_name)
{
   if (const auto it =
           std::ranges::find_if(users, [&](auto const &user) { return user.app_groups.contains(app_name); });
       it != users.end())
   {
      return it->user;
   }

   if (const auto it = std::ranges::find_if(users, [&](auto const &user) { return user.run_all; }); it != users.end())
   {
      return it->user;
   }
   return std::nullopt;
}
