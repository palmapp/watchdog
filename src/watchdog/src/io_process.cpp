//
// Created by Jan on 01.11.2020.
//

#include "io_process.h"

#include "commons/time_utils.h"

#include <boost/process/extend.hpp>
#include <commons/logging.h>
#include <commons/make_string.h>
#include <easylogging++.h>
#include <fmt/chrono.h>
#include <fmt/format.h>
#include <functional>
#include <gsl/span>

namespace bp = boost::process;

namespace
{
   void unix_set_perms_to_execute(watchdog::command const &app)
   {
#ifndef WIN32
      auto cmd = commons::make_string("chmod +x ", app.command_string);
      if (::system(cmd.c_str()) != 0) LOG(WARNING) << "command " << cmd << " has failed";
#endif
   }

   template <class T>
   auto windows_creation_flags(T &info, watchdog::os_user_handle *user)
   {
      return [&info, user](auto &exec)
      {
#ifdef WIN32
         exec.creation_flags |= CREATE_NO_WINDOW;

         exec.set_startup_info_ex();
         exec.startup_info_ex.lpAttributeList = info.lpAttributeList;

         exec.logon_user = user != nullptr ? std::optional{user->get_handle()} : std::nullopt;
#endif
      };
   }

#ifdef WIN32
   auto close_fds_except(gsl::span<int>)
   {
      return [](auto &) {};
   }

#else

   auto close_fds_except(gsl::span<int> keep_descriptors)
   {
      return [=](auto &executor)
      {
         int maxfd = sysconf(_SC_OPEN_MAX);

         auto fd_begin = keep_descriptors.begin();
         auto fd_end = keep_descriptors.end();
         for (int fd = 3; fd < maxfd; ++fd)
         {
            if (fd != executor._pipe_sink && std::find(fd_begin, fd_end, fd) == fd_end)
            {
               ::close(fd);
            }
         }
      };
   }

#endif

   std::int64_t safe_file_size(boost::filesystem::path const &file) noexcept
   {
      boost::system::error_code ec;
      auto result = boost::filesystem::file_size(file, ec);
      if (ec)
      {
         result = 0;
      }

      return static_cast<std::int64_t>(result);
   }

   auto log_output(boost::filesystem::ofstream &out, boost::filesystem::path const &path, std::int64_t &size,
                   std::string_view buffer) -> void
   {
      auto flush_buffer = [&]()
      {
         out.clear();
         out << buffer;
         size += buffer.size();

         if (!out)
         {
            LOG(WARNING) << "unable to write : " << path.string() << " bytes = " << buffer.size()
                         << " msg = " << strerror(errno);
         }
      };

      if (out && out.is_open())
      {
         if (size + buffer.size() > 2000000)
         {
            out.clear();
            out.close();
            if (!out)
            {
               LOG(WARNING) << "unable to close : " << path.string() << " msg = " << strerror(errno);
            }
            LOG_IF(out.is_open(), WARNING) << "file is still open " << path.string();

            auto ec = commons::roll_files(path, 5);
            if (ec && ec != boost::system::errc::no_such_file_or_directory)
            {
               LOG(WARNING) << "unable to roll files : " << path.string() << " msg = " << ec.message()
                            << " truncating...";
               out.open(path, std::ios_base::binary | std::ios_base::trunc);
               LOG_IF(!out.is_open(), WARNING) << "unable to open : " << path.string() << " msg = " << strerror(errno);
            }
            else
            {
               out.open(path, std::ios_base::binary | std::ios_base::app);
               LOG_IF(!out.is_open(), WARNING) << "unable to open : " << path.string() << " msg = " << strerror(errno);
            }

            if (!out.is_open())
            {
               return;
            }
            // buffer is flushed
            size = 0;
            flush_buffer();
         }
         else
         {
            flush_buffer();
         }
      }
      else
      {
         size = safe_file_size(path);
         if (size == 0)
         {
            boost::system::error_code ec;
            boost::filesystem::create_directories(path.parent_path(), ec);
         }

         out.clear();
         out.open(path, std::ios_base::binary | std::ios_base::app);
         if (out && out.is_open())
         {
            flush_buffer();
         }
         else
         {
            LOG(WARNING) << "unable to open : " << path.string() << " msg = " << strerror(errno);
         }
      }
   }

#ifdef WIN32
   const char *endline_char = "\r\n";
#else
   const char *endline_char = "\n";
#endif

#ifdef WIN32
   template <size_t N>
   struct thread_attrs
   {
      thread_attrs(std::array<HANDLE, N> const &handles)
      {
         InitializeProcThreadAttributeList(NULL, 1, 0, &size);
         lpAttributeList = reinterpret_cast<LPPROC_THREAD_ATTRIBUTE_LIST>(HeapAlloc(GetProcessHeap(), 0, size));
         if (lpAttributeList != nullptr)
         {
            fInitialized = static_cast<bool>(InitializeProcThreadAttributeList(lpAttributeList, 1, 0, &size));
            if (fInitialized)
            {
               UpdateProcThreadAttribute(lpAttributeList, 0, PROC_THREAD_ATTRIBUTE_HANDLE_LIST,
                                         const_cast<HANDLE *>(handles.data()), handles.size() * sizeof(HANDLE), NULL,
                                         NULL);
            }
            else
            {
               LOG(ERROR) << "lpAttributeList != nullptr : unable to initialize lpAttributeList";
            }
         }
         else
         {
            LOG(ERROR) << "lpAttributeList == nullptr : unable to initialize lpAttributeList";
         }
      }

      ~thread_attrs()
      {
         if (fInitialized) DeleteProcThreadAttributeList(lpAttributeList);
         if (lpAttributeList) HeapFree(GetProcessHeap(), 0, lpAttributeList);
      }

      LPPROC_THREAD_ATTRIBUTE_LIST lpAttributeList = nullptr;
      SIZE_T size = 0;
      bool fInitialized = false;
   };

   struct ctrl_signal_disabler
   {
      ctrl_signal_disabler()
      {
         SetConsoleCtrlHandler(NULL, TRUE);
      }

      ~ctrl_signal_disabler()
      {
         SetConsoleCtrlHandler(NULL, FALSE);
      }
   };

#else
   template <size_t N>
   struct thread_attrs
   {

      thread_attrs(std::array<int, N> const &) {}
   };

   struct ctrl_signal_disabler
   {
   };
#endif
} // namespace

namespace arg = std::placeholders;
using namespace commons;
namespace
{
   watchdog::filesystem_path get_start_dir(watchdog::command const &cmd)
   {
      return cmd.working_directory.value_or(boost::filesystem::current_path());
   }
} // namespace
boost::process::child watchdog::io_process::start(boost::asio::io_context &io, boost::process::group &group,
                                                  const command &app, boost::process::async_pipe &std_out,
                                                  os_user_handle *user)
{
   unix_set_perms_to_execute(app);
   ctrl_signal_disabler disable_ctrl_c{};

   auto handles = std::array{std_out.native_sink()};
   thread_attrs<1> attrs{handles};

#ifndef WIN32
   std::array not_close{static_cast<int>(std_out.native_sink())};
#endif

   return bp::child(app.to_string(), group, bp::start_dir(get_start_dir(app)),
                    bp::std_in<bp::close, bp::std_out> std_out, bp::std_err > bp::null,
                    bp::extend::on_setup = windows_creation_flags(attrs, user),
#ifndef WIN32
                    bp::extend::on_exec_setup = close_fds_except(not_close),
#endif
                    io);
}
boost::process::child watchdog::io_process::start(boost::asio::io_context &io, boost::process::group &group,
                                                  const watchdog::command &app, boost::process::async_pipe &std_out,
                                                  boost::process::async_pipe &std_err, os_user_handle *user)
{
   unix_set_perms_to_execute(app);
   ctrl_signal_disabler disable_ctrl_c{};
   auto handles = std::array{std_out.native_sink(), std_err.native_sink()};
   thread_attrs<2> attrs{handles};
#ifndef WIN32
   std::array not_close{static_cast<int>(std_out.native_sink()), static_cast<int>(std_err.native_sink())};
#endif
   return bp::child(app.to_string(), group, bp::start_dir(get_start_dir(app)),
                    bp::std_in<bp::close, bp::std_out> std_out, bp::std_err > std_err,
                    bp::extend::on_setup = windows_creation_flags(attrs, user),
#ifndef WIN32

                    bp::extend::on_exec_setup = close_fds_except(not_close),
#endif
                    io);
}
boost::process::child watchdog::io_process::start(boost::asio::io_context &io, boost::process::group &group,
                                                  const watchdog::command &app, boost::process::async_pipe &std_out,
                                                  boost::process::async_pipe &std_err,
                                                  boost::process::async_pipe &std_in, os_user_handle *user)
{
   unix_set_perms_to_execute(app);
   ctrl_signal_disabler disable_ctrl_c{};
   auto handles = std::array{std_out.native_sink(), std_err.native_sink(), std_in.native_source()};
   thread_attrs<3> attrs{handles};

#ifndef WIN32
   std::array not_close{static_cast<int>(std_out.native_sink()), static_cast<int>(std_err.native_sink()),
                        static_cast<int>(std_in.native_source())};
#endif
   return bp::child(app.to_string(), group, bp::start_dir(get_start_dir(app)), bp::std_in<std_in, bp::std_out> std_out,
                    bp::std_err > std_err, bp::extend::on_setup = windows_creation_flags(attrs, user),
#ifndef WIN32
                    bp::extend::on_exec_setup = close_fds_except(not_close),
#endif
                    io);
}

watchdog::io_process::io_process(std::shared_ptr<boost::asio::io_context> io, boost::process::group &group,
                                 const watchdog::command &app, bool enable_stdin, io_process_capture_settings settings,
                                 std::optional<os_user> user)
    : settings_(settings), name_(app.command_string), io_(io), timer_(*io), std_out_(*io), std_err_(*io), std_in_(*io)
{
   std_out_buffer_.resize(4096);
   std_err_buffer_.resize(4096);

   if (user.has_value())
   {
      user_handle_.emplace(user.value());
   }

   if (enable_stdin)
   {
      child_ = start(*io, group, app, std_out_, std_err_, std_in_,
                     user_handle_.has_value() ? &user_handle_.value() : nullptr);
   }
   else
   {
      child_ = start(*io, group, app, std_out_, std_err_, user_handle_.has_value() ? &user_handle_.value() : nullptr);
   }

   /*
   std_err_path_ = app.logging_directory_path / (app.name + "-std-err.log");
   std_out_path_ = app.logging_directory_path / (app.name + "-std-out.log");
    */
}

watchdog::io_process::io_process(boost::asio::io_context &io, boost::process::group &group,
                                 const watchdog::command &app, bool enable_stdin, io_process_capture_settings settings,
                                 std::optional<os_user> user)
    : settings_(settings), name_(app.command_string), io_(nullptr), timer_(io), std_out_(io), std_err_(io), std_in_(io)
{
   std_out_buffer_.resize(4096);
   std_err_buffer_.resize(4096);

   if (user.has_value())
   {
      user_handle_.emplace(user.value());
   }

   if (enable_stdin)
   {
      child_ = start(io, group, app, std_out_, std_err_, std_in_,
                     user_handle_.has_value() ? &user_handle_.value() : nullptr);
   }
   else
   {
      child_ = start(io, group, app, std_out_, std_err_, user_handle_.has_value() ? &user_handle_.value() : nullptr);
   }

   /*
   std_err_path_ = app.logging_directory_path / (app.name + "-std-err.log");
   std_out_path_ = app.logging_directory_path / (app.name + "-std-out.log");
    */
}

void watchdog::io_process::write_stdin(std::string_view data)
{
   std::error_code running_ec;
   if (child_.running(running_ec))
   {
      auto self = shared_from_this();
      auto buffer = std::make_shared<std::string>(data.data(), data.size());
      boost::asio::async_write(std_in_, boost::asio::buffer(*buffer),
                               [self, buffer](const boost::system::error_code &ec, std::size_t bytes_transferred)
                               {
                                  if (ec)
                                  {
                                     LOG(WARNING) << "unable to write to stdin";
                                  }
                                  else
                                  {
                                     LOG(TRACE) << "write_stdin completed";
                                  }
                               });
   }
}

boost::process::child &watchdog::io_process::get() noexcept
{
   return child_;
}

std::string const &watchdog::io_process::name() noexcept
{
   return name_;
}

void watchdog::io_process::on_std_output(const boost::system::error_code &ec, std::size_t size)
{
   if (!ec)
   {
      auto data = std::string_view{std_out_buffer_.data(), size};

      std::visit(
          [&](auto const &settings)
          {
             using settings_t = std::decay_t<decltype(settings)>;
             if constexpr (std::is_same_v<settings_t, std_capture_to_file>)
             {
                log_output(std_out_file_, settings.stdout_path, std_out_size_, data);
             }
             if constexpr (std::is_same_v<settings_t, std_handle_output>)
             {
                if (script_output *output = settings.output; output != nullptr)
                {
                   output->on_std_out(data);
                }
             }
          },
          settings_);

      begin_std_out();
   }
   else
   {
      std_out_.close();
   }
   /* else
    {
       LOG(WARNING) << name_ << " : on_std_output failed with " << ec.message();
    }*/
}

void watchdog::io_process::on_std_error(const boost::system::error_code &ec, std::size_t size)
{
   if (!ec)
   {
      auto data = std::string_view{std_err_buffer_.data(), size};

      std::visit(
          [&](auto const &settings)
          {
             using settings_t = std::decay_t<decltype(settings)>;
             if constexpr (std::is_same_v<settings_t, std_capture_to_file>)
             {
                log_output(std_err_file_, settings.stderr_path, std_err_size_, data);
             }
             if constexpr (std::is_same_v<settings_t, std_handle_output>)
             {
                if (script_output *output = settings.output; output != nullptr)
                {
                   output->on_std_error(data);
                }
             }
          },
          settings_);

      begin_std_err();
   }
   else
   {
      std_err_.close();
   }
}

void watchdog::io_process::begin_std_out()
{
   std::error_code running_ec;

   auto self = shared_from_this();
   if (std_out_.is_open())
   {
      std_out_.async_read_some(boost::asio::buffer(std_out_buffer_),
                               [this, self](const boost::system::error_code &ec, std::size_t size) mutable
                               { on_std_output(ec, size); });
   }
}

void watchdog::io_process::begin_std_err()
{
   std::error_code running_ec;
   if (std_err_.is_open())
   {
      auto self = shared_from_this();
      std_err_.async_read_some(boost::asio::buffer(std_err_buffer_),
                               [this, self](const boost::system::error_code &ec, std::size_t size) mutable
                               { on_std_error(ec, size); });
   }
}
void watchdog::io_process::start_read()
{

   std::visit(
       [&](auto const &settings)
       {
          using settings_t = std::decay_t<decltype(settings)>;
          if constexpr (std::is_same_v<settings_t, std_capture_to_file>)
          {
             auto message =
                 fmt::format("{} : starting pid: {} {}", time_utils::format_local_iso_time(time_utils::now_in_millis()),
                             child_.id(), endline_char);

             log_output(std_out_file_, settings.stderr_path, std_out_size_, message);
             log_output(std_err_file_, settings.stderr_path, std_err_size_, message);
          }
          if constexpr (std::is_same_v<settings_t, std_handle_output>)
          {
             if (script_output *output = settings.output; output != nullptr)
             {
                output->on_process_started(child_.id());
             }
          }
       },
       settings_);

   begin_std_out();
   begin_std_err();

   begin_timer();
}

void watchdog::io_process::begin_timer()
{
   auto self = shared_from_this();
   timer_.expires_from_now(std::chrono::seconds{1});
   timer_.async_wait(
       [self, this](boost::system::error_code const &ec)
       {
          if (!ec)
          {
             if (std_out_file_.is_open()) std_out_file_.flush();

             if (std_err_file_.is_open()) std_err_file_.flush();

             begin_timer();
          }
       });
}
void watchdog::io_process::close_file_handles() noexcept
{
   if (std_out_file_.is_open()) std_out_file_.close();

   if (std_err_file_.is_open()) std_err_file_.close();
}

#ifdef WIN32
#include <Windows.h>
namespace
{
   JOBOBJECT_EXTENDED_LIMIT_INFORMATION create_job_extended_info()
   {
      JOBOBJECT_EXTENDED_LIMIT_INFORMATION jobInfo = {};
      memset(&jobInfo, 0, sizeof(JOBOBJECT_EXTENDED_LIMIT_INFORMATION));

      jobInfo.BasicLimitInformation.LimitFlags = JOB_OBJECT_LIMIT_KILL_ON_JOB_CLOSE;
      return jobInfo;
   }
} // namespace

void watchdog::io_process::set_windows_termination_flags(boost::process::group &group)
{
   static JOBOBJECT_EXTENDED_LIMIT_INFORMATION job_info = create_job_extended_info();

   if (!::SetInformationJobObject(group.native_handle(), JobObjectExtendedLimitInformation, &job_info,
                                  sizeof(JOBOBJECT_EXTENDED_LIMIT_INFORMATION)))
   {
      boost::system::error_code ec(::GetLastError(), boost::system::system_category());
      throw boost::system::system_error(ec);
   }
}

#else
void watchdog::io_process::set_windows_termination_flags(boost::process::group &group) {}
#endif

std::shared_ptr<watchdog::io_process>
watchdog::io_process::start_io_process(std::shared_ptr<boost::asio::io_context> io, boost::process::group &group,
                                       const watchdog::command &app, bool enable_stdin,
                                       watchdog::io_process_capture_settings settings, std::optional<os_user> user)
{
   return std::shared_ptr<watchdog::io_process>(
       new io_process(io, group, app, enable_stdin, settings, std::move(user)));
}
std::shared_ptr<watchdog::io_process>
watchdog::io_process::start_io_process(boost::asio::io_context &io, boost::process::group &group,
                                       const watchdog::command &app, bool enable_stdin,
                                       watchdog::io_process_capture_settings settings, std::optional<os_user> user)
{
   return std::shared_ptr<watchdog::io_process>(
       new io_process(io, group, app, enable_stdin, settings, std::move(user)));
}