//
// Created by jhrub on 02.11.2020.
//
#include "send_wm_quit.h"
#include <easylogging++.h>
#ifdef WIN32
#include <windows.h>
#endif

#ifdef WIN32
namespace
{
   struct search_context
   {
      boost::process::pid_t pid = 0;
      HWND window = nullptr;
   };
   BOOL iterator(HWND hwdn, LPARAM ptr)
   {
      auto ctx = reinterpret_cast<search_context *>(ptr);
      DWORD window_pid = 0;
      GetWindowThreadProcessId(hwdn, &window_pid);

      if (window_pid == ctx->pid)
      {
         ctx->window = hwdn;
         return FALSE;
      }

      return TRUE;
   };
} // namespace

#endif

void watchdog::send_wm_quit(boost::process::pid_t pid)
{
#ifdef WIN32
   search_context ctx;
   ctx.pid = pid;

   EnumWindows(&iterator, reinterpret_cast<LPARAM>(&ctx));

   if (ctx.window != nullptr)
   {
      PostMessageW(ctx.window, WM_QUIT, 0, 0);
      LOG(TRACE) << "WM_QUIT send to " << pid;
   }
   else
   {
      LOG(TRACE) << "WM_QUIT could not be send to " << pid;
   }
#else
   LOG(WARNING) << "wmquit exit method is not implemented on this platform";
#endif
}
