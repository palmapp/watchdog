#include "watched_app.h"
#include <boost/algorithm/string.hpp>

namespace
{
   using namespace boost;
   using namespace std::string_literals;

   std::vector<std::string> get_apps(const commons::properties_map &properties)
   {
      std::vector<std::string> apps;
      const auto it = properties.find("watchdog.apps");
      if (it != properties.end())
      {
         split(apps, it->second, [](char c) { return c == ' '; });
      }

      return apps;
   }

   template <class CallbackT>
   void get_value(const commons::properties_map &properties, std::string key, CallbackT value_callback)
   {
      const auto it = properties.find(key);
      if (it != properties.end())
      {
         value_callback(it->second);
      }
   }
} // namespace

watchdog::watched_apps watchdog::watched_app::parse_from_map(const commons::properties_map &properties,
                                                             const filesystem_path &exe_dir)
{
   watchdog::watched_apps result;

   for (auto &app : get_apps(properties))
   {
      watchdog::watched_app app_obj;
      app_obj.name = app;
      get_value(properties, app + ".exe",
                [&](const std::string &value)
                {
                   filesystem_path relative_path{value};
                   if (relative_path.is_relative())
                   {
                      app_obj.cmd.command_string = absolute(exe_dir / relative_path).string();
                   }
                   else
                   {
                      app_obj.cmd.command_string = absolute(relative_path).string();
                   }
                });

      get_value(properties, app + ".args",
                [&](const std::string &args) { app_obj.cmd.arguments = command::parse_command_string(args); });

      get_value(properties, app + ".exit",
                [&](const std::string &exit_method)
                {
                   app_obj.exit_method = exit_method;
                   boost::replace_all(app_obj.exit_method, "\\r", "\r");
                   boost::replace_all(app_obj.exit_method, "\\n", "\n");
                   boost::replace_all(app_obj.exit_method, "\\t", "\t");
                   boost::replace_all(app_obj.exit_method, "\\b", "\b");
                   boost::replace_all(app_obj.exit_method, "\\v", "\v");
                   boost::replace_all(app_obj.exit_method, "\\a", "\a");
                   boost::replace_all(app_obj.exit_method, "\\f", "\f");
                   boost::replace_all(app_obj.exit_method, "\\\\", "\\");
                });

      if (app_obj.cmd.command_string.empty()) continue;

      if (boost::starts_with(app_obj.exit_method, "input:"))
      {
         app_obj.enable_std_in = true;
      }

      result.push_back(std::move(app_obj));
   }

   return result;
}
