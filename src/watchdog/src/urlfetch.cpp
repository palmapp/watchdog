#include "urlfetch.h"
#include "io_process.h"
#include <algorithm>
#include <boost/algorithm/string/trim.hpp>
#include <boost/dll/runtime_symbol_info.hpp>
#include <boost/process.hpp>
#include <boost/process/extend.hpp>
#include <commons/process_killer.h>
#include <commons/run_safe.h>
#include <easylogging++.h>
#include <fmt/format.h>
#include <optional>

#ifndef WIN32
#include <signal.h>
#include <sys/types.h>
constexpr const char *url_fetch_exe = "urlfetch";
#else
constexpr const char *url_fetch_exe = "urlfetch.exe";
#endif

namespace
{

   int run_urlfetch(std::vector<std::string> arguments, std::chrono::milliseconds timeout,
                    commons::cancellation_token token, std::string *result_string = nullptr)
   {
      namespace bp = boost::process;
      using namespace std::chrono_literals;
      using namespace watchdog;

      command app;

      auto url_fetch_exe = []
      {
         auto path = boost::dll::program_location().parent_path() / "urlfetch";
         auto path_exe = boost::dll::program_location().parent_path() / "urlfetch.exe";

         if (boost::filesystem::exists(path))
         {
            return path.string();
         }
         else if (boost::filesystem::exists(path_exe))
         {
            return path_exe.string();
         }
         else
         {
            return (boost::dll::program_location().parent_path().parent_path() / "urlfetch" / "urlfetch").string();
         }
      }();

      app.command_string = url_fetch_exe;
      app.arguments = arguments;

      boost::asio::io_context ios;
      struct pipe_with_buffer
      {
         bp::async_pipe pipe;
         std::vector<char> buffer;
         std::vector<char> data;

         pipe_with_buffer(boost::asio::io_context &io) : pipe(io)
         {
            buffer.resize(4096);
            data.reserve(4096);
         }

         void on_read(boost::system::error_code const &ec, size_t len)
         {
            if (!ec)
            {
               std::copy(buffer.begin(), buffer.begin() + len, std::back_inserter(data));
               begin();
            }
         }

         void begin()
         {
            pipe.async_read_some(boost::asio::buffer(buffer),
                                 [this](boost::system::error_code const &ec, size_t len) { on_read(ec, len); });
         }

         std::string_view to_string_view()
         {
            return {data.data(), data.size()};
         }

         std::string to_string()
         {
            return {data.data(), data.size()};
         }
      };

      pipe_with_buffer std_out(ios);
      pipe_with_buffer std_err(ios);

      bp::group g;
      io_process::set_windows_termination_flags(g);

      bp::child process = io_process::start(ios, g, app, std_out.pipe, std_err.pipe);

      std_out.begin();
      std_err.begin();

      auto expires_at = timeout > std::chrono::milliseconds{0}
                            ? std::chrono::steady_clock::now() + timeout
                            : std::chrono::steady_clock::now() + std::chrono::hours{1};

      while (process.running())
      {
         ios.run_for(1s);

         bool is_cancelled = token.is_cancelled();
         if (is_cancelled || expires_at < std::chrono::steady_clock::now())
         {
            commons::process_killer::send_kill(process.id());

            if (is_cancelled)
            {
               throw std::logic_error(fmt::format("{}: operation canceled", app.to_string(false)));
            }
            else
            {
               throw std::logic_error(fmt::format("{}: operation timed out", app.to_string(false)));
            }
         }
      }

      ios.poll();

      int code = process.exit_code();
      if (code < 200 || code > 299)
      {
         std::string error_string = std_err.to_string();
         boost::trim(error_string);

         LOG(ERROR) << app.to_string(false) << "\n\t=> [" << code << "] : " << error_string;
      }
      else if (result_string != nullptr)
      {
         *result_string = std_out.to_string();
      }

      return code;
   }

   std::string format_headers(const watchdog::url::header_container &headers)
   {
      std::stringstream args;

      for (auto &&header : headers)
      {
         args << " \"" << header.first << "\"";
         args << " \"" << header.second << "\"";
      }

      return args.str();
   }
} // namespace

int watchdog::url::fetch_to_string(std::string &result, const std::string &url,
                                   const watchdog::url::header_container &headers, commons::cancellation_token token,
                                   std::chrono::milliseconds timeout)
{
   return run_urlfetch({"-s", url, format_headers(headers)}, timeout, token, &result);
}

int watchdog::url::send_get_request(const std::string &url, const watchdog::url::header_container &headers,
                                    commons::cancellation_token token, std::chrono::milliseconds timeout)
{
   return run_urlfetch({"-s", url, format_headers(headers)}, timeout, token);
}

int watchdog::url::fetch_to_file(const boost::filesystem::path &output_path, const std::string &url,
                                 const watchdog::url::header_container &headers, commons::cancellation_token token,
                                 std::chrono::milliseconds timeout)
{
   return run_urlfetch({"-f", url, format_headers(headers), output_path.string()}, timeout, token);
}
