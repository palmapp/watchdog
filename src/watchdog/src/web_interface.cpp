//
// Created by jhrub on 10.11.2020.
//

#include "web_interface.h"
#include "app_group_controller.h"
#include "memory_dump.h"
#include "overload.h"
#include "script_buffer_output.h"
#include "script_engine_resolver.h"
#include "system_updater.h"
#include <boost/dll/runtime_symbol_info.hpp>
#include <commons/run_safe.h>
#include <gsl/narrow>
#include <react_app.h>
#include <spider/api_app_base.h>
#include <spider/detail/default_websocket_manager.h>
#include <spider/spider.h>

using namespace std::chrono_literals;
namespace watchdog
{
   struct memory_dump_request
   {
      std::string app_group;
      std::string app;
   };

   struct memory_dump_result
   {
      std::string result;
   };

   update_manager_active_object::update_manager_active_object() : commons::active_object<update_manager_command>() {}

   void update_manager_active_object::on_message(update_manager_command &message)
   {
      std::visit(overload{[&](set_update_manager_cmd &msg) { update_manager_ = std::move(msg.update_manager); },
                          [&](download_update_cmd &msg)
                          {
                             if (update_manager_ != nullptr) update_manager_->download_update(msg.cfg, msg.output);
                          },
                          [&](install_update_cmd &msg)
                          {
                             if (update_manager_ != nullptr) update_manager_->install_update(msg.output);
                          }},
                 message);
   }
} // namespace watchdog
VISITABLE_STRUCT(watchdog::memory_dump_request, app_group, app);
VISITABLE_STRUCT(watchdog::memory_dump_result, result);

namespace watchdog
{

   namespace
   {
      boost::filesystem::path find_debug_react_index_html(boost::filesystem::path const &exe_path)
      {
         int max_depth = 10;
         std::string relative_path = "../web-ui/public/index_debug.html";

         while (--max_depth > 0)
         {
            relative_path = "../" + relative_path;
            auto lookup_path = exe_path / relative_path;
            if (boost::filesystem::exists(lookup_path))
            {
               return boost::filesystem::canonical(exe_path / relative_path).make_preferred();
            }

            LOG(DEBUG) << lookup_path.string() << " does not exist";
         }

         throw std::logic_error{"unable to find web-ui/public/index_debug.html"};
      }

      class websocket_handler : public spider::websocket_handler
      {
       public:
         websocket_handler(websocket_status_bridge &bridge) : bridge_(bridge) {}

         void on_text_message(spider::websocket_connection &connection, std::string message) override
         {
            commons::run_safe(
                [this, &message]()
                {
                   if (message == "clear_log")
                   {
                      bridge_.clear_log();
                   }
                   if (message == "install_all_updates")
                   {
                      bridge_.install_all_updates();
                   }
                   else if (message == "check_all_updates")
                   {
                      bridge_.check_all_updates();
                   }
                   else if (message == "revert_all")
                   {
                      bridge_.revert_all();
                   }
                   else
                   {
                      std::vector<std::string> result;
                      boost::split(result, message, boost::is_any_of(" "));

                      if (result.size() == 2)
                      {
                         const auto &cmd = result.front();
                         const auto &app_group = result[1];

                         if (cmd == "restart")
                         {
                            bridge_.restart(app_group);
                         }
                         else if (cmd == "run_script")
                         {
                            bridge_.run_script(app_group);
                         }
                      }
                   }
                },
                "on_text_message");
         }

         void on_connected(spider::websocket_connection &connection) override
         {
            LOG(INFO) << "on_connected << " << connection.id_;
            connection.send(bridge_.get_status_string());
            connection.send(bridge_.get_script_output());
         }

         void on_disconnected(spider::websocket_connection &connection) override {}

       private:
         websocket_status_bridge &bridge_;
      };

      class memory_dump_app : public spider::api_app_base
      {
       public:
         memory_dump_app(std::string path, web_interface &updater) : api_app_base(std::move(path))
         {
            on(beast::http::verb::post, "/",
               [&](spider::request &, memory_dump_request request)
               {
                  auto result = updater.memory_dump(request.app_group, request.app);
                  return memory_dump_result{result};
               });
         }
      };

   } // namespace
} // namespace watchdog

watchdog::web_interface::web_interface(commons::cancellation_token token, bind_settings settings,
                                       std::string dump_folder)
    : cancellation_token_(token), bind_settings_(std::move(settings)), dump_folder_(std::move(dump_folder)),
      notify_clients_(false)
{
}

watchdog::web_interface::~web_interface()
{
   if (thread_.has_value())
   {
      thread_->join();
   }
}

void watchdog::web_interface::thread_run() noexcept
{
   commons::run_safe(
       [this]() mutable
       {
          using namespace spider;

          init_loggers();

          app_configuration app_cfg;

          if (bind_settings_.bind_ip.has_value())
          {
             app_cfg.bind_ip_addresses.push_back(bind_settings_.bind_ip.value());
             app_cfg.bind_loop_back_only = false;
          }
          else
          {
             app_cfg.bind_loop_back_only = !bind_settings_.bind_all;
          }
          app_cfg.tcp_port = bind_settings_.http_port;

          spider::default_websocket_manager manager{};
          websocket_handler handler{*this};

          manager.add_ws_handler(handler);
          buffer_.on_change.connect(
              [&](script_execution const &execution)
              {
                 fmt::memory_buffer buffer;
                 spider::serialization::json::json_buffer output{buffer};
                 output.object(
                     [&]()
                     {
                        output.string("script_output");
                        spider::serialization::json::write_json(output, execution);
                     });

                 manager.send_all(to_string(buffer));
              });

          buffer_.on_reset.connect([&]() { manager.send_all(get_script_output()); });
          auto exe_path = boost::dll::program_location().parent_path();
          auto resources = exe_path / "mng";

#ifdef NDEBUG
          auto index_html = resources / "index.html";
#else
          auto index_html = find_debug_react_index_html(exe_path);
#endif

          running_app_ = run_spider_app_additional(
              app_cfg,
              [&](init_context &ctx, app &root)
              {
                 on_url<websocket_app>(root, "/messages/", manager);
                 on_url<memory_dump_app>(root, "/memorydump/", *this);
                 on_url<react_app>(root, "/", index_html);
                 on_url<static_file_handler>(root, "/", resources, spider::cache_control::make_cache_public());
              });

          while (!cancellation_token_.wait_for(1s))
          {
             format_new_status_string();
             manager.send_all(current_json_status_);
          }

          running_app_->stop();
          running_app_->wait();
       },
       "advanced_update_manager::thread_run");
}

void watchdog::web_interface::format_new_status_string()
{
   std::lock_guard lock{mutex_};

   fmt::memory_buffer buffer;
   spider::serialization::json::json_buffer output{buffer};

   auto format_version_info = [](std::optional<update_model> const &update_model)
   {
      std::string current_version = "unknown";
      std::string current_build_number;

      if (update_model.has_value())
      {
         for (auto &kv : update_model.value().metadata)
         {
            if (kv.key == "current_version")
            {
               current_version = kv.value;
            }
            else if (kv.key == "current_build_number")
            {
               current_build_number = fmt::format(" (build {})", kv.value);
            }
         }
      }

      return fmt::format("{}{}", current_version, current_build_number);
   };

   auto cfg = app_group_config{app_base_config_};

   output.object(
       [&, this]()
       {
          output.kv_pair_num("autoUpdate", app_base_config_.auto_update_enabled
                                               ? static_cast<int>(std::chrono::duration_cast<std::chrono::seconds>(
                                                                      app_base_config_.update_check_interval)
                                                                      .count())
                                               : 0);
          output.kv_pair_num("autoUpdateOnStart",
                             app_base_config_.auto_update_enabled && app_base_config_.update_force_on_start ? 1 : 0);
          output.kv_pair_num("autoConfiguration", app_base_config_.auto_configuration ? 1 : 0);
          output.kv_pair("channelPath", app_base_config_.channel_path.string());
          output.kv_pair("channelUrl", app_base_config_.channel_base_url);

          output.kv_pair_arr("disabledApps",
                             [&]
                             {
                                for (auto &app : app_base_config_.disabled_apps)
                                {
                                   output.string(app);
                                }
                             });

          output.string("runInfo");
          status_.format_json(output);

          output.kv_pair_obj(
              "updateInfo",
              [&, this]() mutable
              {
                 for (auto &kv : current_update_status_.groups)
                 {
                    auto &group = kv.second.config;
                    output.kv_pair_obj(
                        kv.first,
                        [&, this]() mutable
                        {
                           output.kv_pair("updateState", get_update_status(kv.second));
                           output.kv_pair("currentVersionString", group.current_version_string);
                           output.kv_pair("currentVersion", format_version_info(read_update_model(group)));

                           if (kv.second.error_message.has_value())
                           {
                              output.kv_pair("lastError", kv.second.error_message.value());
                           }

                           if (kv.second.current_state == app_group_status::state::pending &&
                               kv.second.model.has_value())
                           {
                              auto &update_model = kv.second.model.value();
                              output.kv_pair("pendingVersionString", update_model.hash);
                              output.kv_pair("pendingVersion", format_version_info(update_model));
                           }

                           if (kv.second.current_state == app_group_status::state::downloading &&
                               kv.second.model.has_value())
                           {
                              auto &update_model = kv.second.model.value();
                              output.kv_pair_num("pendingSize", update_model.size);
                              output.kv_pair_num("downloadedSize", get_downloaded_size(group, update_model));
                              output.kv_pair("downloadingVersionString", update_model.hash);
                              output.kv_pair("downloadingVersion", format_version_info(update_model));
                           }
                        });
                 }
              });

          output.string("scripts");
          spider::serialization::json::write_json(output, app_base_config_.scripts.scripts);
          output.kv_pair("watchdog_name", app_base_config_.watchdog_name);
       });

   current_json_status_ = output.to_string();
}
std::string_view watchdog::web_interface::get_update_status(const app_group_status &status) const
{
   std::string_view update_status = "ready";
   switch (status.current_state)
   {
   case watchdog::app_group_status::state::checking:
      update_status = "checking_for_update";
      break;
   case watchdog::app_group_status::state::downloading:
      update_status = "downloading_update";
      break;

   case watchdog::app_group_status::state::installing:
      update_status = "installing_update";
      break;
   case watchdog::app_group_status::state::pending:
      update_status = "installation_pending";
      break;
   case watchdog::app_group_status::state::none:
      update_status = "updated";
      break;
   }
   return update_status;
}

void watchdog::web_interface::restart(std::string app_group)
{
   auto [ctrl_ptr, app_base_config] = [&]
   {
      std::lock_guard lock{mutex_};
      return std::make_pair(ctrl_, app_base_config_);
   }();

   if (ctrl_ptr != nullptr)
   {
      ctrl_ptr->restart(app_group, app_group_config{app_base_config}.get_config(app_group, true), false);
   }
}

std::string watchdog::web_interface::get_status_string()
{
   std::lock_guard lock{mutex_};
   return current_json_status_;
}

void watchdog::web_interface::check_all_updates()
{
   auto config = [&]
   {
      std::lock_guard lock{mutex_};
      return app_base_config_;
   }();

   update_manager_.post_message(download_update_cmd{config, &buffer_});
}

void watchdog::web_interface::install_all_updates()
{
   update_manager_.post_message(install_update_cmd{&buffer_});
}

void watchdog::web_interface::revert_all()
{
   LOG(WARNING) << "revert all is not implmented";
}

void watchdog::web_interface::on_event(status_event evt)
{
   status_.on_event(evt);
   notify_clients();
}

void watchdog::web_interface::notify_clients() noexcept
{
   notify_clients_.store(true);
}
std::string watchdog::web_interface::memory_dump(std::string app_group, std::string app)
{
   try
   {
      std::int64_t pid = status_.get_pid(app_group, app);

      LOG(INFO) << app_group << " " << app << " memory dump requested for PID " << pid;
      if (pid == -1)
      {
         LOG(WARNING) << "there is no running app : " << app_group << " " << app;
         return "app is not running";
      }
      else
      {
         return do_memory_dump(gsl::narrow_cast<std::uint32_t>(pid), {dump_folder_},
                               fmt::format("{}_{}", app_group, app));
      }
   }
   catch (std::exception const &ex)
   {
      LOG(ERROR) << app_group << " " << app << " memory dump crashed with " << ex.what();
      return ex.what();
   }
   catch (...)
   {
      LOG(ERROR) << app_group << " " << app << " memory dump crashed with unknown exception";
      return "unknown exception";
   }
}
void watchdog::web_interface::set_app_base_config(const watchdog::app_base_config &cfg)
{
   auto copy_cfg = cfg;

   std::lock_guard lk{mutex_};
   app_base_config_ = copy_cfg;
}

std::string watchdog::web_interface::get_script_output()
{
   auto state = buffer_.get_history();

   fmt::memory_buffer buffer;
   spider::serialization::json::json_buffer output{buffer};
   output.object(
       [&]()
       {
          output.string("script_reset");
          spider::serialization::json::write_json(output, state);
       });

   return to_string(buffer);
}

void watchdog::web_interface::run_script(std::string script_name)
{
   std::optional<command> script;
   {
      std::lock_guard lk{mutex_};
      if (auto it = app_base_config_.scripts.scripts.find(script_name); it != app_base_config_.scripts.scripts.end())
      {
         script = command::parse(it->second);
         if (script.has_value())
         {
            auto tokens = app_base_config_.get_script_token_map();
            script = to_script_engine(script.value()).apply_tokens(&tokens);
            script->working_directory = app_base_config_.channel_path;
         }
      }
   }

   if (script.has_value())
   {
      scripts_executor_.async_run_script(script_name, script.value(), buffer_);
   }
}
void watchdog::web_interface::clear_log()
{
   buffer_.reset_buffer();
}

void watchdog::web_interface::set_app_group_controller(std::shared_ptr<watchdog::app_group_controller> ctrl)
{
   std::lock_guard lk{mutex_};
   ctrl_ = std::move(ctrl);
}

void watchdog::web_interface::set_update_manager(std::shared_ptr<watchdog::default_update_manager> manager)
{
   update_manager_.post_message(set_update_manager_cmd{manager});

   manager->on_state_changed.connect(
       [&](update_status status)
       {
          {
             std::lock_guard lg{mutex_};
             current_update_status_ = std::move(status);
          }
          format_new_status_string();
          notify_clients();
       });
}
void watchdog::web_interface::start_thread()
{
   auto self = shared_from_this();
   thread_.emplace([self] { self->thread_run(); });
}
