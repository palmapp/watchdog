#include "watchdog.h"
#include <thread>

#include "io_process.h"
#include "scope_exit.h"

#include <commons/make_string.h>
#include <commons/process_killer.h>

#include <easylogging++.h>

#include "send_wm_quit.h"
#include <boost/algorithm/string.hpp>
#include <condition_variable>
#include <set>
using namespace watchdog;
using namespace std::chrono_literals;

namespace
{
   struct app_state
   {
      std::chrono::steady_clock::time_point started;
      std::optional<std::chrono::steady_clock::time_point> stopped;
      int failed_counter = 0;
   };

   using watched_process = std::pair<watched_app const &, std::shared_ptr<io_process>>;
   using watched_processes = std::vector<watched_process>;
   using runtime_state = std::unordered_map<std::string, app_state>;

   enum class running_state
   {
      created,
      running,
      stopped
   };

   class session : public watchdog::app_session
   {
    public:
      session(watchdog::app_session_settings settings) : settings_(std::move(settings))
      {
         running_ = running_state::created;
         io_ = std::make_shared<boost::asio::io_context>();
         thread_ = std::jthread(&session::run_checker, this);

         io_process::set_windows_termination_flags(group_);
      }

      ~session()
      {
         stop_and_wait();
      }

      void stop() override
      {
         LOG(TRACE) << settings_.app_group << ": stop";
         {
            std::unique_lock lk{mutex_};
            if (running_ == running_state::stopped) return;

            cv_.wait(lk, [this]() { return running_ != running_state::created; });
            running_ = running_state::stopped;
         }
         cv_.notify_all();
      }

      void stop_and_wait() override
      {
         LOG(TRACE) << settings_.app_group << ": stop_and_wait - begin";
         stop();

         if (thread_.joinable()) thread_.join();

         LOG(TRACE) << settings_.app_group << ": stop_and_wait - end";
      }

    private:
      enum class exit_mode
      {
         term,
         file,
         interrupt,
         kill,
         input,
         wm_quit
      };

      void clean_exit_files(const std::vector<boost::filesystem::path> &exit_files) const
      {
         for (auto const &exit_file : exit_files)
         {
            boost::system::error_code ec;
            boost::filesystem::remove(exit_file, ec);
         }
      }

      exit_mode parse_exit_mode(std::string const &mode, exit_mode def = exit_mode::file) const
      {
         if (mode.empty())
         {
            return def;
         }
         exit_mode result = mode == "interrupt"                  ? exit_mode::interrupt
                            : mode == "term"                     ? exit_mode::term
                            : mode == "wmquit"                   ? exit_mode::wm_quit
                            : mode == "kill"                     ? exit_mode::kill
                            : boost::starts_with(mode, "input:") ? exit_mode::input

                                                                 : def;

         return result;
      }

      std::string_view parse_exit_mode_input_sequence(std::string_view mode, std::string_view default_mode) const
      {
         if (boost::starts_with(mode, "input:") && mode.size() > 6)
         {
            return mode.substr(6);
         }
         else if (boost::starts_with(default_mode, "input:") && default_mode.size() > 6)
         {
            return default_mode.substr(6);
         }
         else
         {
            return {"\r\n"};
         }
      }

      std::vector<boost::filesystem::path> terminate_processes()
      {
         std::vector<boost::filesystem::path> exit_files;
         std::error_code ec;

         using namespace commons::process_killer;
         auto default_exit_mode = parse_exit_mode(settings_.exit_mode);

         for (auto &[app, handle] : processes_)
         {
            settings_.observer(term_event{settings_.app_group, app.name});

            auto mode = parse_exit_mode(app.exit_method, default_exit_mode);
            if (handle && handle->get().running(ec))
            {
               auto &process = handle->get();
               switch (mode)
               {
               case exit_mode::term:
                  LOG(TRACE) << settings_.app_group << ":" << app.name << " send_term";
                  commons::process_killer::send_term(process.id());
                  break;
               case exit_mode::kill:
                  LOG(TRACE) << settings_.app_group << ":" << app.name << " send_kill";
                  commons::process_killer::send_kill(process.id());
                  break;
               case exit_mode::file:
               {

                  auto file = commons::process_killer::create_exit_file(
                      filesystem_path{app.cmd.command_string}.stem().string(), process.id());
                  if (file.has_value())
                  {
                     LOG(TRACE) << settings_.app_group << ":" << app.name << " create_exit_file "
                                << file.value().string();
                     exit_files.push_back(file.value());
                  }
                  else
                  {
                     LOG(TRACE) << settings_.app_group << ":" << app.name << " send_term";
                     commons::process_killer::send_term(process.id());
                  }
                  break;
               }
               case exit_mode::interrupt:
                  LOG(TRACE) << settings_.app_group << ":" << app.name << " send_interrupt";
                  commons::process_killer::send_interrupt(process.id());
                  break;
               case exit_mode::input:
               {

                  auto sequence = parse_exit_mode_input_sequence(app.exit_method, settings_.exit_mode);
                  LOG(TRACE) << settings_.app_group << ":" << app.name << " write_stdin \"" << sequence << "\"";
                  handle->write_stdin(sequence);
                  break;
               }
               case exit_mode::wm_quit:
                  send_wm_quit(process.id());
                  break;
               }
            }
         }
         return exit_files;
      }

      void run_app_fail_script(const watchdog::filesystem_path &exe_path, const std::string &app_name,
                               int exit_code) const
      {
         if (!settings_.fail_script.empty())
         {
            std::string script = boost::replace_all_copy(settings_.fail_script, "$appname", app_name);
            boost::replace_all(script, "$exitcode", std::to_string(exit_code));
#ifdef WIN32
            const char *command_separator = "& ";
#else
            const char *command_separator = "; ";
#endif
            std::string command =
                commons::make_string("cd \"", exe_path.parent_path(), "\"", command_separator, script);
            int r = std::system(command.c_str());
            LOG(INFO) << "executing cmd: " << script << " with result: " << r;
         }
      }

      watched_processes start_all_processes()
      {
         watched_processes result;
         for (auto const &app : settings_.apps)
         {
            result.push_back(safe_start_process(app));
            process_runtime_state_[app.name] = {std::chrono::steady_clock::now()};
         }

         return result;
      }

      watched_process safe_start_process(watchdog::watched_app const &app)
      {
         watched_process result{app, {}};

         std::string not_started_message;
         try
         {
            result.second = start_process(app);
         }
         catch (std::exception const &ex)
         {
            not_started_message = ex.what();
         }
         catch (...)
         {
            not_started_message = "unknown exception";
         }

         if (!not_started_message.empty())
         {
            LOG(ERROR) << app.name << " failed with:" << not_started_message;

            settings_.observer(not_started_event{settings_.app_group, app.name, std::move(not_started_message)});
            run_app_fail_script(filesystem_path{app.cmd.command_string}, app.name, -666);
         }
         else
         {
            settings_.observer({started_event{settings_.app_group, app.name, result.second->get().id()}});
         }

         return result;
      }

      std::shared_ptr<io_process> start_process(watchdog::watched_app const &app)
      {
         auto result = io_process::start_io_process(
             io_, group_, app.cmd, app.enable_std_in,
             std_capture_to_file{/*.stdout_path =*/app.logging_directory_path / (app.name + "-std-out.log"),
                                 /*.stderr_path = */ app.logging_directory_path / (app.name + "-std-err.log")},
             settings_.user);

         result->start_read();

         return std::move(result);
      }

      watchdog::watched_app const &get_watched_app(std::string const &name) const
      {
         auto it = std::find_if(settings_.apps.begin(), settings_.apps.end(),
                                [&](watchdog::watched_app const &app) { return app.name == name; });

         if (it != settings_.apps.end())
         {
            return *it;
         }
         else
         {
            LOG(FATAL) << "unable to find app with name " << name;
         }
      }

      void find_not_running_processes(std::vector<watched_processes::iterator> &result)
      {
         std::error_code ec;
         const auto not_running_condition = [&](watched_process const &proc)
         { return !proc.second || !proc.second->get().running(ec); };

         auto end = processes_.end();
         auto now = std::chrono::steady_clock::now();

         for (auto it = processes_.begin(); it != end; ++it)
         {
            if (not_running_condition(*it))
            {
               result.push_back(it);
            }
            else
            {
               auto &state = process_runtime_state_[it->first.name];
               if (state.failed_counter != 0 && now - state.started > std::chrono::minutes{1})
               {
                  LOG(INFO) << "app is running stable : " << it->first.name;
                  state.failed_counter = 0;
               }
            }
         }
      }

      void run_checker()
      {
         {
            std::lock_guard lock{mutex_};
            processes_ = start_all_processes();
            running_ = running_state::running;
         }
         cv_.notify_all();

         std::vector<watched_processes::iterator> not_running_processes;
         while (running_ == running_state::running)
         {
            find_not_running_processes(not_running_processes);
            if (!not_running_processes.empty())
            {
               emit_exited_events(not_running_processes);
               restart_processes(not_running_processes);

               not_running_processes.clear();
            }

            if (processes_.empty())
            {
               settings_.token.wait_for(500ms);
            }
            else
            {
               io_->run_for(500ms);
            }

            if (settings_.token.is_cancelled())
            {
               break;
            }
         }

         LOG(TRACE) << settings_.app_group << ": exiting";

         if (!wait_for_termination())
         {
            LOG(WARNING) << settings_.app_group << ": there are still apps running after timeout -> sending kill";
            kill_the_rest();
         }

         LOG(TRACE) << settings_.app_group << ": thread exited";
      }

      void kill_the_rest()
      {
         std::error_code ec;
         const auto running_condition = [&](watched_process const &proc)
         { return proc.second && proc.second->get().running(ec); };

         for (auto const &proc : processes_)
         {
            if (running_condition(proc))
            {
               commons::process_killer::send_kill(proc.second->get().id());
               proc.second->get().wait(ec);

               auto code = proc.second->get().exit_code();
               settings_.observer(exited_event{settings_.app_group, proc.first.name, code});
            }
         }

         io_->run_for(100ms);
      }

      bool wait_for_termination()
      {
         std::vector<watched_processes::iterator> not_running_processes;
         auto exit_times_out_at = std::chrono::steady_clock::now() + std::chrono::seconds{settings_.kill_timeout};
         auto exit_files = terminate_processes();
         watchdog::stupid_msvc_scope_exit on_scope_exit([&] { clean_exit_files(exit_files); });

         std::size_t exited_count = 0;
         std::set<watched_processes::iterator> exited_apps;

         const auto running_condition = [&](watched_process const &proc)
         {
            std::error_code ec;
            return proc.second && proc.second->get().running(ec);
         };

         while (exited_count < processes_.size() && exit_times_out_at > std::chrono::steady_clock::now())
         {
            find_not_running_processes(not_running_processes);
            exited_count = not_running_processes.size();

            for (auto const &it : not_running_processes)
            {
               if (exited_apps.insert(it).second)
               {
                  emit_exited_events({it}, true);
               }
            }

            not_running_processes.clear();

            auto it = std::find_if(processes_.begin(), processes_.end(), running_condition);
            if (it != processes_.end())
            {
               io_->run_for(10ms);
               it->second->get().wait_for(10ms);
            }
         }

         io_->run_for(100ms);

         return exited_count == processes_.size();
      }

      void restart_processes(const std::vector<watched_processes::iterator> &not_running_processes)
      {
         auto now = std::chrono::steady_clock::now();
         for (auto it : not_running_processes)
         {
            auto &state = process_runtime_state_[it->first.name];
            if (it->second)
            {
               it->second->close_file_handles();
               state.stopped = now;

               if (now - state.started < std::chrono::seconds{30})
               {
                  ++state.failed_counter;

                  if (state.failed_counter > 5)
                  {
                     LOG(ERROR) << "app is stuck in an infinite loop: " << it->first.name
                                << " slowing down restarts to 30s interval";
                  }
               }
            }

            it->second.reset();

            if (state.failed_counter <= 1 ||
                state.stopped.value() + std::chrono::seconds{state.failed_counter > 5 ? 30 : 5} < now)
            {
               if (state.failed_counter <= 1)
               {
                  LOG(WARNING) << "app has been terminated : " << it->first.name << " doing immediate restart";
               }

               it->second = safe_start_process(it->first).second;
               if (it->second != nullptr)
               {
                  state.started = now;
                  state.stopped.reset();
               }
            }
         }
      }

      void emit_exited_events(const std::vector<watched_processes::iterator> &not_running_processes,
                              bool exiting = false) const
      {
         for (auto it : not_running_processes)
         {
            if (it->second)
            {
               auto code = it->second->get().exit_code();
               settings_.observer(exited_event{settings_.app_group, it->first.name, code});

               if (!exiting)
               {
                  run_app_fail_script(it->first.cmd.command_string, it->first.name, code);
               }
            }
         }
      }
      boost::process::group group_;

      std::atomic<running_state> running_;
      std::vector<boost::filesystem::path> exit_files_;
      watchdog::app_session_settings settings_;
      watched_processes processes_;
      runtime_state process_runtime_state_;
      std::shared_ptr<boost::asio::io_context> io_;

      std::jthread thread_;
      std::condition_variable cv_;
      mutable std::mutex mutex_;
   };
} // namespace

std::unique_ptr<watchdog::app_session> watchdog::watch_apps(app_session_settings settings)
{
   auto ptr = std::make_unique<session>(std::move(settings));
   return std::unique_ptr<watchdog::app_session>(ptr.release());
}
