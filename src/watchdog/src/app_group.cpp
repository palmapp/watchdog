#include "app_group.h"
#include "app_group_config.h"
#include "overload.h"
#include "script_engine_resolver.h"

#include <boost/dll/runtime_symbol_info.hpp>
#include <boost/filesystem/fstream.hpp>
#include <commons/file_system.h>
#include <commons/make_string.h>
#include <commons/properties_parser.h>
#include <cstdlib>
#include <easylogging++.h>
#include <mstch/mstch.hpp>

using namespace std::chrono_literals;

watchdog::app_group::app_group(std::string name, status_observer_func observer)
    : commons::active_object<app_group_message>(commons::stopping_policy::process_all), name_(name),
      status_observer_(std::move(observer))
{
}

std::string watchdog::app_group::get_exit_mode(const watchdog::system_updater_config &updater_config) const
{
   auto exit_mode = updater_config.exit_mode;

   if (exit_mode.empty())
   {
#ifdef WIN32
      exit_mode = "file";
#else
      exit_mode = "term";
#endif
   }
   return exit_mode;
}

void watchdog::app_group::print_app_info(const watchdog::watched_apps &apps) const
{
   std::stringstream app_info;
   for (auto &app : apps)
   {
      app_info << "app: " << app.name << "\n\t" << app.cmd.to_string() << "\n";
   }

   LOG(INFO) << this->name_ << " : running " << app_info.str();
}

watchdog::watched_apps
watchdog::app_group::get_watched_apps(const watchdog::system_updater_config &updater_config) const
{
   auto active_version_directory = updater_config.apps_path / "versions" / updater_config.current_version_string;
   auto active_version_directory_cpy = active_version_directory;

   boost::system::error_code ec;
   active_version_directory = boost::filesystem::canonical(active_version_directory, ec);
   if (ec)
   {
      LOG(WARNING) << name_ << ": unable to perform canonical call : " << ec.message() << " path "
                   << active_version_directory_cpy.string();
      return {};
   }
   else
   {
      active_version_directory = active_version_directory.make_preferred();
   }

   auto active_version_directory_string = active_version_directory.string();

   const auto watchdog_properties = commons::parse_config_file(active_version_directory / "watchdog.properties");
   auto apps = watchdog::watched_app::parse_from_map(watchdog_properties, active_version_directory);

   std::vector<watched_app> result_apps;
   result_apps.reserve((updater_config.instances.size() + 1) * (apps.size() + 1));

   for (auto &[instance, config] : updater_config.instances)
   {
      if (boost::starts_with(config.app_mode, "iisexpress:"))
      {
         result_apps.push_back(create_iisexpress_app(config, active_version_directory));
      }
      boost::filesystem::create_directory(config.logging_dir_path, ec);

      auto instance_tokens = string_map{
          {"envpath", config.env_path.string()},        {"datadir", config.app_data_path.string()},
          {"envdir", config.env_dir_path.string()},     {"loggingdir", config.logging_dir_path.string()},
          {"version", active_version_directory_string},
      };

      for (auto app : apps)
      {
         app.name = fmt::format("{}-{}", instance, app.name);
         app.cmd = to_script_engine(app.cmd).apply_tokens(&instance_tokens);

         fill_app_session_props(config, active_version_directory_string, app);
         result_apps.push_back(std::move(app));
      }
   }

   if (boost::starts_with(updater_config.app_mode, "iisexpress:"))
   {
      apps.push_back(create_iisexpress_app(updater_config, active_version_directory));
   }

   boost::filesystem::create_directory(updater_config.logging_dir_path, ec);
   auto runtime_tokens = string_map{
       {"envpath", updater_config.env_path.string()},    {"datadir", updater_config.app_data_path.string()},
       {"envdir", updater_config.env_dir_path.string()}, {"loggingdir", updater_config.logging_dir_path.string()},
       {"version", active_version_directory_string},
   };
   for (auto &app : apps)
   {
      app.cmd = to_script_engine(app.cmd).apply_tokens(&runtime_tokens);

      fill_app_session_props(updater_config, active_version_directory_string, app);
      result_apps.push_back(std::move(app));
   }

   return result_apps;
}
void watchdog::app_group::fill_app_session_props(const watchdog::system_updater_config &updater_config,
                                                 std::string active_version_directory_string, watched_app &app) const
{
   const auto fix_cwd_spec = [&](std::string &arg)
   {
      boost::replace_all(arg, "./", active_version_directory_string + "/");
      boost::replace_all(arg, ".\\", active_version_directory_string + "\\");
   };

   for (auto &arg : app.cmd.arguments)
   {
      fix_cwd_spec(arg);
   }
   fix_cwd_spec(app.cmd.command_string);

   app.logging_directory_path = updater_config.logging_dir_path;
}

watchdog::watched_app
watchdog::app_group::create_iisexpress_app(const system_updater_config &config,
                                           const boost::filesystem::path active_version_directory) const
{
   mstch::map root;
   root["name"] = name_;
   root["path"] = active_version_directory.string();
   root["port"] = config.app_mode.substr(config.app_mode.find(':') + 1);

   const auto iis_conf_path = config.env_dir_path / "applicationhost.config";

   auto template_file_path = config.utils_path / "Templates" / "iis_application_host_config.tmpl";

   auto iis_application_host_config = commons::read_file(template_file_path);
   boost::replace_all(iis_application_host_config, "\r\n", "\n");

   if (iis_application_host_config.empty())
   {
      LOG(WARNING) << "unable to open template path " << template_file_path.string();
   }
   else
   {

      boost::filesystem::ofstream out{iis_conf_path, std::ios_base::trunc};
      out << mstch::render(iis_application_host_config, root, {});
      out.flush();
      if (out.fail())
      {
         LOG(WARNING) << iis_conf_path.string() << " : cannot be flushed";
      }

      out.close();

      if (out.fail())
      {
         LOG(WARNING) << iis_conf_path.string() << " : cannot be closed";
      }
   }

   static boost::filesystem::path program_files = getenv("ProgramFiles");
   auto iisexe = program_files / "IIS Express" / "iisexpress.exe";
   if (!boost::filesystem::exists(iisexe))
   {
      LOG(WARNING) << "iis express is not installed on this computer at : " << iisexe.string();
   }

   return watchdog::watched_app{"iisexpress",
                                {
                                    iisexe.string(),
                                    {fmt::format("/config:{}", iis_conf_path.string()), fmt::format("/site:{}", name_),
                                     "/apppool:Clr4IntegratedAppPool"},
                                },
                                "wmquit",
                                config.logging_dir_path,
                                true};
}
void watchdog::app_group::on_message(watchdog::app_group_message &message)
{
   auto &message_base =
       std::visit([](app_group_message_base &base) -> app_group_message_base & { return base; }, message);
   if (message_base.new_config.has_value())
   {
      config_ = std::move(message_base.new_config.value());
   }

   std::visit(overload{[&](app_group_message_restart &)
                       {
                          if (current_session_ != nullptr)
                          {
                             current_session_->stop_and_wait();
                          }

                          if (config_.has_value())
                          {
                             create_new_session();
                          }
                       },
                       [&](app_group_message_status &) {

                       },
                       [&](app_group_refresh_config &cfg)
                       {
                          if (!config_.has_value())
                          {
                             config_ = std::move(cfg.new_config);
                             create_new_session();
                          }
                          else
                          {
                             const auto old_user = try_to_find_user_for_app(config_.value().users, name_);
                             const auto new_user = try_to_find_user_for_app(cfg.new_config.users, name_);
                             if (old_user != new_user)
                             {
                                if (current_session_ != nullptr)
                                {
                                   current_session_->stop_and_wait();
                                }

                                config_ = std::move(cfg.new_config);
                                create_new_session();
                             }
                          }
                       },
                       [&](app_group_message_pause &)
                       {
                          if (current_session_ != nullptr)
                          {
                             current_session_->stop_and_wait();
                             current_session_.reset(nullptr);
                          }
                       },
                       [&](app_group_message_resume &)
                       {
                          if (current_session_ != nullptr)
                          {
                             current_session_->stop_and_wait();
                          }

                          if (config_.has_value())
                          {
                             create_new_session();
                          }
                       }},
              message);

   message_base.promise.set_value();
}

void watchdog::app_group::create_new_session()
{
   auto &config = config_.value();
   watched_apps apps = get_watched_apps(config);
   print_app_info(apps);

   std::string exit_mode = get_exit_mode(config);

   current_session_ =
       watch_apps({name_, status_observer_, apps, config.stop_before_update, config.script_after_app_fail,
                   static_cast<int>(std::chrono::duration_cast<std::chrono::seconds>(config.kill_timeout).count()),
                   exit_mode, try_to_find_user_for_app(config.users, name_), cts_.get_cancellation_token()});
}

void watchdog::app_group::on_stopped() noexcept
{
   cts_.cancel();
   LOG(INFO) << name_ << ": stopped";
}

void watchdog::app_group::on_exception(const watchdog::app_group_message &x, const std::exception &ex) noexcept
{
   LOG(ERROR) << name_ << ":[" << x.index() << "]: exception " << ex.what();
}
void watchdog::app_group::on_unknown_exception(const watchdog::app_group_message &x) noexcept
{
   LOG(ERROR) << name_ << ":[" << x.index() << "]: unknown exception";
}

std::future<void> watchdog::app_group::process(watchdog::app_group_message message)
{
   auto &message_base =
       std::visit([](app_group_message_base &base) -> app_group_message_base & { return base; }, message);

   auto future = message_base.promise.get_future();
   if (!post_message(std::move(message)))
   {
      throw std::logic_error{"unable to queue the message"};
   }

   return future;
}
