#include "script_runner.h"
#include "command_parser.h"
#include "io_process.h"
#include <boost/process.hpp>
#include <easylogging++.h>
#include <fmt/format.h>
#include <string_view>

using namespace std::string_view_literals;
using namespace std::chrono_literals;
namespace bp = boost::process;

bool watchdog::script_runner::run_script(std::string_view name, command const &cmd, script_output &output,
                                         std::optional<std::chrono::steady_clock::duration> time_out) noexcept
{

   try
   {
      auto whole_command = cmd.to_string();

      LOG(INFO) << fmt::format("running : {} {}", name, whole_command);
      output.on_script_begin(name, whole_command);

      auto context = boost::asio::io_context{};
      auto group = boost::process::group{};

      auto process = io_process::start_io_process(context, group, cmd, false, std_handle_output{&output});
      process->start_read();

      auto &child = process->get();

      std::error_code ec;
      do
      {
         context.run_for(20ms);
      } while (child.running(ec));

      auto exit_code = child.exit_code();
      output.on_script_finished(exit_code);

      LOG(INFO) << fmt::format("{} : {}", name, exit_code);

      return exit_code == 0;
   }
   catch (std::exception const &ex)
   {
      output.on_execution_error({ex.what()});
   }
   catch (...)
   {
      output.on_execution_error("unknown exception");
   }
   return false;
}