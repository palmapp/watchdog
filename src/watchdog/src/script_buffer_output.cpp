//
// Created by jhrub on 27.09.2022.
//
#include "script_buffer_output.h"
#include <mutex>
#include <stdexcept>
#include <commons/time_utils.h>
#include <atomic>

void watchdog::script_execution_impl::on_script_begin(std::string_view script_name, std::string_view command)
{

   static std::atomic<std::int64_t> s_counter = 0;

   on_change(make_diff([&] ()mutable {
                          this->run_id = s_counter++;
                          this->name = static_cast<std::string>(script_name);
                          this->command = static_cast<std::string>(command);
   }));

   if (next_output != nullptr)
      next_output->on_script_begin(script_name, command);
}

void watchdog::script_execution_impl::on_process_started(int pid)
{


   on_change(make_diff([&]()mutable {
                          this->pid = pid;

                       }));

   if (next_output != nullptr)
      next_output->on_process_started(pid);
}

void watchdog::script_execution_impl::on_std_out(std::string_view buffer)
{
   auto diff_update = make_diff([&]()mutable {
                                   std_out += buffer;

                                });
   diff_update.std_out = static_cast<std::string>(buffer);

   on_change(diff_update);

   if (next_output != nullptr)
      next_output->on_std_out(buffer);
}

void watchdog::script_execution_impl::on_std_error(std::string_view buffer)
{


   auto diff_update = make_diff([&]()mutable {
                                   std_err += buffer;

                                });
   diff_update.std_err = static_cast<std::string>(buffer);

   on_change(diff_update);

   if (next_output != nullptr)
      next_output->on_std_error(buffer);
}

void watchdog::script_execution_impl::on_execution_error(std::string_view what)
{
   auto diff_update = make_diff([&]()mutable {
                                   error_message = static_cast<std::string>(what);
                                });

   diff_update.error_message = static_cast<std::string>(what);

   on_change(diff_update);

   if (next_output != nullptr)
      next_output->on_execution_error(what);
}

void watchdog::script_execution_impl::on_script_finished(int status)
{
   auto diff_update = make_diff([&]()mutable {
                                   exit_code = status;
                                });
   diff_update.exit_code = status;

   on_change(diff_update);

   if (next_output != nullptr)
      next_output->on_script_finished(status);
}

template <class T>
watchdog::script_execution watchdog::script_execution_impl::make_diff(T fun)
{
   std::lock_guard lk{mutex_};
   fun();

   auto result = script_execution();
   result.output_name = output_name;
   result.run_id = run_id;
   result.name = name;
   result.command = command;
   result.pid = pid;

   return result;
}

std::vector<watchdog::script_execution> watchdog::script_buffer_output::get_history()
{
   std::vector<watchdog::script_execution> result;

   std::vector<std::shared_ptr<script_execution_impl>> copy;

   {
      std::lock_guard lk{mutex_};
      result.reserve(buffer_.execution_history.size());
      copy = buffer_.execution_history;
   }

   for (auto &ptr : copy)
   {
      std::lock_guard ptr_lk{ptr->mutex_};
      result.push_back(static_cast<script_execution const &>(*ptr));
   }

   return result;
}

void watchdog::script_buffer_output::reset_buffer()
{
   std::vector<script_execution_ptr> preserve;

   {
      std::lock_guard lk{mutex_};

      for (auto ptr : buffer_.execution_history)
      {
         if (!ptr->error_message.has_value() && ptr->pid.has_value() && !ptr->exit_code.has_value())
         {
            preserve.push_back(ptr);
         }
      }

      buffer_.execution_history = std::move(preserve);
   }

   on_reset();
}


std::shared_ptr<watchdog::script_output> watchdog::script_buffer_output::begin(std::string_view output_name)
{
   auto execution_ptr = std::make_shared<script_execution_impl>();
   execution_ptr->output_name = static_cast<std::string>(output_name);
   execution_ptr->on_change.connect(on_change);

   {
      std::lock_guard lk{mutex_};
      buffer_.execution_history.push_back(execution_ptr);
   }
   return execution_ptr;
}
void watchdog::script_buffer_output::on_execution_error(std::string_view what) {}
