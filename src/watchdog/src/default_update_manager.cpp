#include "default_update_manager.h"
#include "installer.h"
#include "system_updater.h"
#include <easylogging++.h>
#include <fmt/format.h>
#include <ranges>

namespace ranges = std::ranges;

const auto update_single_group = [](std::string group_name, auto fun)
{
   return [=](watchdog::update_status &state)
   {
      auto &group_state = state.groups[group_name];
      group_state = fun(group_state);
      return group_state;
   };
};

watchdog::default_update_manager::default_update_manager(int threads) : thread_pool_(threads) {}

bool watchdog::default_update_manager::download_update(watchdog::app_base_config config, script_buffer_output *output)
{
   auto debug_output = [&]() -> std::shared_ptr<script_output>
   { return output != nullptr ? output->begin("install_update") : std::make_shared<null_script_output>(); }();
   auto print_debug = [=](std::string message)
   {
      message += '\n';
      debug_output->on_std_out(message);
   };
   debug_output->on_script_begin("download_update", {});
   debug_output->on_process_started(-1);

   auto apps = installer::get_configured_apps(config);

   update_group_state(
       [&](update_status &state)
       {
          for (auto &app : apps)
          {
             auto &status = state.groups[app];
             status.current_state = app_group_status::state::checking;
             status.model.reset();
          }
       });

   print_debug("checking for configuration changes");

   try
   {
      auto configuration = [&]() -> installer::app_configuration
      {
         if (!config.legacy_mode)
         {
            return installer::run_app_configuration(config.channel_path, config.channel_path / "channel.properties", true);
         }
         else
         {
            return installer::app_configuration{.config = config, .apps = apps};
         }
      }();

      print_debug("checking for update");

      if (!config.legacy_mode)
      {
         // we could have few new apps waiting for installation
         update_group_state(
             [&](update_status &state)
             {
                for (auto &app : configuration.apps)
                {
                   auto &status = state.groups[app];
                   status.current_state = app_group_status::state::checking;
                   status.model.reset();
                }
             });
      }

      auto tasks = std::vector<std::packaged_task<int()>>{};
      for (auto &app : configuration.apps)
      {
         tasks.push_back(std::packaged_task<int()>{
             [&]
             {
                try
                {
                   auto pending_model = [&]
                   {
                      std::lock_guard lk{mutex_};
                      if (auto it = this->status_.groups.find(app); it != this->status_.groups.end())
                      {
                         return it->second.pending_model;
                      }
                      return optional<update_model>{};
                   }();

                   auto config = configuration.config.get_config(app, true);
                   auto update_model = ::watchdog::check_for_update(config, {});
                   if (update_model.has_value() &&
                       (!pending_model.has_value() || pending_model->hash != update_model->hash) &&
                       config.current_version_string != update_model->hash)
                   {

                      update_group_state(update_single_group(
                          app,
                          [&](app_group_status const &current)
                          {
                             return app_group_status{.current_state = app_group_status::state::downloading,
                                                     .model = update_model,
                                                     .config = current.config,
                                                     .pending_model = current.pending_model};
                          }));

                      auto &unwrapped = update_model.value();
                      LOG(INFO) << app << ": downloading : " << unwrapped.hash << " : " << unwrapped.size;
                      print_debug(fmt::format("{}: downloading", app));

                      if (download_and_extract_update(unwrapped, config, {}))
                      {

                         LOG(INFO) << app << ": downloaded : " << unwrapped.hash << " : " << unwrapped.size;

                         print_debug(fmt::format("{}: download_and_extract_update {}: {}", app, unwrapped.hash,
                                                 unwrapped.size));

                         update_group_state(update_single_group(
                             app,
                             [&](app_group_status const &current)
                             {
                                return app_group_status{.current_state = app_group_status::state::pending,
                                                        .model = update_model,
                                                        .config = current.config,
                                                        .pending_model = update_model};
                             }));
                      }
                      else
                      {
                         print_debug(fmt::format("{}: download_and_extract_update has failed", app));

                         update_group_state(update_single_group(app,
                                                                [&](app_group_status const &current)
                                                                {
                                                                   return app_group_status{
                                                                       .current_state = app_group_status::state::none,
                                                                       .error_message =
                                                                           std::string{
                                                                               "download_and_extract_update has failed",
                                                                           },
                                                                       .config = current.config,
                                                                       .pending_model = current.pending_model};
                                                                }));

                         return 1;
                      }
                   }
                   else if (pending_model.has_value() && pending_model->hash == update_model->hash)
                   {
                      print_debug(fmt::format("{}: is downloaded already: {}", app, pending_model->hash));

                      LOG(INFO) << app << ": is downloaded already : " << pending_model->hash;
                      update_group_state(update_single_group(app,
                                                             [&](app_group_status const &current)
                                                             {
                                                                return app_group_status{
                                                                    .current_state = app_group_status::state::pending,
                                                                    .model = pending_model,
                                                                    .config = current.config,
                                                                    .pending_model = pending_model};
                                                             }));
                      return 1;
                   }
                   else
                   {
                      print_debug(
                          fmt::format("{}: is already at the current version: {}", app, config.current_version_string));

                      LOG(INFO) << app << ": is already at the current version : " << config.current_version_string;
                      update_group_state(update_single_group(app,
                                                             [&](app_group_status const &current) {
                                                                return app_group_status{
                                                                    .current_state = app_group_status::state::none,
                                                                    .config = current.config};
                                                             }));
                      return 1;
                   }

                   return 0;
                }
                catch (std::exception const &ex)
                {
                   print_debug(fmt::format("{}: operation failed with: {}", app, ex.what()));

                   LOG(ERROR) << app << ": installation failed with " << ex.what();
                   update_group_state(update_single_group(
                       app,
                       [&](app_group_status const &current)
                       {
                          return app_group_status{.current_state = app_group_status::state::none,
                                                  .error_message =
                                                      std::string{"download_and_extract_update has failed"},
                                                  .config = current.config,
                                                  .pending_model = current.pending_model};
                       }));

                   return 1;
                }
             }});
      }

      auto futures = std::vector<std::future<int>>{};
      for (auto &t : tasks)
      {
         futures.push_back(boost::asio::post(thread_pool_, std::move(t)));
      }

      int i = 0;
      for (auto &f : futures)
      {
         i += f.get();
      }

      config_ = configuration.config;

      debug_output->on_script_finished(0);
      return true;
   }
   catch (std::exception const &ex)
   {
      print_debug(fmt::format("operation failed with: {}", ex.what()));
   }

   // failed case
   update_group_state(
       [&](update_status &state)
       {
          for (auto &app : apps)
          {
             auto &status = state.groups[app];
             status.current_state = app_group_status::state::none;
             status.model.reset();
          }
       });

   debug_output->on_script_finished(-1);
   return false;
}

void watchdog::default_update_manager::install_update(script_buffer_output *output)
{

   if (!config_.has_value())
   {
      LOG(ERROR) << "config_ is not set call download_update first";
      return;
   }
   auto debug_output = [&]() -> std::shared_ptr<script_output>
   { return output != nullptr ? output->begin("install_update") : std::make_shared<null_script_output>(); }();
   auto print_debug = [=](std::string message)
   {
      message += '\n';
      debug_output->on_std_out(message);
   };

   auto &config = config_.value();
   auto &channel_config = config.get_channel_config();
   if (!channel_config.legacy_mode && channel_config.auto_configuration)
   {
      bool failed = false;
      try
      {
         LOG(INFO) << fmt::format("existing app reconfiguration");

         print_debug(fmt::format("existing app reconfiguration"));

         installer::run_app_configuration(channel_config.channel_path,
                                          channel_config.channel_path / "channel.properties", false);
      }
      catch(std::exception const& ex)
      {
         print_debug(fmt::format("operation failed with: {}", ex.what()));
         failed = true;
      }

      if (failed)
      {
         update_group_state(
             [&](update_status &state)
             {
                for (auto &[app, status] : state.groups)
                {
                   status.current_state = app_group_status::state::none;
                   status.model.reset();
                }
             });

         debug_output->on_script_finished(-1);
      }
   }

   auto tasks = std::vector<std::packaged_task<void()>>{};

   auto groups = get_groups_for_installation();
   auto group_names =
       groups | ranges::views::transform([](std::pair<std::string, app_group_status> const &kv) { return kv.first; });

   LOG(INFO) << fmt::format("install_update: {}", fmt::join(group_names, ","));
   print_debug(fmt::format("install_update: {} ",fmt::join(group_names, ",")));

   for (auto &[app, status] : groups)
   {
      tasks.push_back(std::packaged_task<void()>{
          [&]
          {
             auto group_cfg = status.config;
             if (group_cfg.stop_before_update)
             {
                LOG(INFO) << fmt::format("stopping before installation: {}", app);

                on_stop_app_group_before_update(app);
             }

             LOG(INFO) << fmt::format("installing: {}", app);

             update_group_state(update_single_group(app,
                                                    [&](app_group_status const &current)
                                                    {
                                                       return app_group_status{.current_state =
                                                                                   app_group_status::state::installing,
                                                                               .model = status.model,
                                                                               .config = current.config,
                                                                               .pending_model = current.pending_model};
                                                    }));

             if (watchdog::install_update(config.get_config(app), status.model.value(), output))
             {
                group_cfg.current_version_string = status.model.value().hash;

                LOG(INFO) << fmt::format("installed: {} = {}", app, group_cfg.current_version_string);

                update_group_state(update_single_group(
                    app, [&](app_group_status const &current)
                    { return app_group_status{.current_state = app_group_status::state::none, .config = group_cfg}; }));

                LOG(INFO) << fmt::format("begin on_restart_app_group: {} = {}", app, group_cfg.current_version_string);
                on_restart_app_group(app, group_cfg);

                LOG(INFO) << fmt::format("end on_restart_app_group: {} = {}", app, group_cfg.current_version_string);
             }
             else
             {
                update_group_state(update_single_group(
                    app,
                    [&](app_group_status const &current)
                    {
                       return app_group_status{.current_state = app_group_status::state::none,
                                               .model = {},
                                               .error_message = std::string{"install update has failed, check the log"},
                                               .config = current.config,
                                               .pending_model = current.pending_model};
                    }));

                LOG(ERROR) << fmt::format("install update has failed: {} = {}", app, group_cfg.current_version_string);
             }
          }});
   }

   auto futures = std::vector<std::future<void>>{};
   for (auto &t : tasks)
   {
      futures.push_back(boost::asio::post(thread_pool_, std::move(t)));
   }

   for (auto &f : futures)
   {
      f.wait();
   }
   debug_output->on_script_finished(0);
}

std::vector<std::pair<std::string, watchdog::app_group_status>>
watchdog::default_update_manager::get_groups_for_installation() const
{
   std::vector<std::pair<std::string, app_group_status>> result;

   std::lock_guard lk{mutex_};
   result.reserve(status_.groups.size());

   for (auto &pair : status_.groups)
   {
      if (pair.second.current_state == app_group_status::state::pending && pair.second.model.has_value())
      {
         result.push_back(pair);
      }
   }

   return result;
}

std::vector<std::string>
watchdog::default_update_manager::check_configured_apps(watchdog::app_base_config const &config)
{
   auto apps = installer::get_configured_apps(config);
   auto provider = app_group_config{config};
   auto configs = std::vector<system_updater_config>{};
   configs.reserve(apps.size());

   for (auto &app : apps)
   {
      configs.push_back(provider.get_config(app, true));
   }

   auto status = [&]
   {
      std::lock_guard lg{mutex_};
      config_ = config;

      for (size_t i = 0, len = configs.size(); i != len; ++i)
      {
         status_.groups[apps[i]].config = configs[i];
      }
      return status_;
   }();

   on_state_changed(status);

   return apps;
}
