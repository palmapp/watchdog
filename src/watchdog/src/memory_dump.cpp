
// clang-format off
#include <easylogging++.h>
#include "memory_dump.h"

#include <commons/run_safe.h>
#include <commons/time_utils.h>
#include <fmt/format.h>


#include <fmt/chrono.h>
#ifdef _WIN32
   #include <tchar.h>
   #include <windows.h>
   #include <DbgHelp.h>
#endif
#include <thread>
#include "scope_exit.h"

// clang-format on

using namespace boost::filesystem;
using namespace std;
using namespace commons;

namespace
{
   void report_error(std::string error_text)
   {
      LOG(ERROR) << "do_memory_dump : " << error_text;
   }
} // namespace


#ifdef _WIN32
std::string watchdog::do_memory_dump(uint32_t pid, path output_directory, std::string dump_file_prefix)
{
   create_directories(output_directory);

   const auto hProc = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, pid);

   if (hProc != NULL)
   {
      stupid_msvc_scope_exit close_hProc([&] { CloseHandle(hProc); });

      const auto current_time = chrono::system_clock::to_time_t(chrono::system_clock::now());

      const auto file_name =
          fmt::format("{}-{}-{:%Y-%m-%d%H%M%S}.dmp", dump_file_prefix, pid, fmt::localtime(current_time));
      const auto output_file_path = output_directory / file_name;

      const auto hFile = CreateFileW(output_file_path.c_str(), GENERIC_WRITE, FILE_SHARE_WRITE, NULL, CREATE_ALWAYS,
                                     FILE_ATTRIBUTE_NORMAL, NULL);

      if (hFile == INVALID_HANDLE_VALUE)
      {
         report_error(fmt::format("unable to create file for memory dump : {}", output_file_path.string()));
      }
      else
      {
         stupid_msvc_scope_exit close_hFile([&] { CloseHandle(hFile); });

         static auto hModule = LoadLibraryA("Dbghelp.dll");
         static BOOL (*MiniDumpWriteDump)(HANDLE, DWORD, HANDLE, DWORD, VOID *, VOID *, VOID *) =
             reinterpret_cast<decltype(MiniDumpWriteDump)>(GetProcAddress(hModule, "MiniDumpWriteDump"));

         if (MiniDumpWriteDump == NULL)
         {
            report_error("unable to load Dbghelp.dll to invoke MiniDumpWriteDump");
         }
         else
         {
            LOG(INFO) << "memory dump created at:" << output_file_path.string();
            auto bSuccess = MiniDumpWriteDump(hProc, pid, hFile, 2, NULL, NULL, NULL);
            if (bSuccess)
            {
               return output_file_path.string();
            }
         }
      }
   }
   else
   {
      report_error(fmt::format("unable to OpenProcess with PID : {}", pid));
   }

   return {};
}
#else
std::string watchdog::do_memory_dump(uint32_t pid, path output_directory, std::string dump_file_prefix)
{
   report_error("memory dump is not supported on this platform");
   return {};
}
#endif
