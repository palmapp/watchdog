#include "installer.h"
#include "system_updater.h"
#include <algorithm>
#include <boost/filesystem/fstream.hpp>
#include <commons/file_system.h>
#include <commons/properties_parser.h>
#include <commons/run_safe.h>
#include <easylogging++.h>
#include <fmt/chrono.h>
#include <mstch/mstch.hpp>
#include <regex>
#include <serialization/json/buffer.h>

using namespace std::ranges;
using namespace std::chrono_literals;
using namespace std::string_literals;

namespace watchdog::installer
{
   watchdog::optional<filesystem_path> backup_file(std::string session, filesystem_path const &path);
   std::string format_error_details_for_missing_tokens(std::map<std::string, std::set<std::string>> const &tokens);
   void try_to_create_directories(const std::string &configuration);
} // namespace watchdog::installer

void watchdog::installer::install_app(watchdog::app_group_config const &config_provider, const std::string &app,
                                      step_manager &mng)
{
   auto config = config_provider.get_config(app, true);

   mng.on_update_step("checking for update");
   auto update_model = check_for_update(config, {});
   if (update_model.has_value())
   {
      mng.on_update_step("checking for update");
      auto unwrapped = update_model.value();
      LOG(INFO) << app << ": downloading : " << unwrapped.hash << " : " << unwrapped.size;

      mng.on_update_step("downloading and extracting");
      mng.begin_download(config.apps_path / "downloads" / (unwrapped.hash + ".zip"), unwrapped.size);

      if (download_and_extract_update(unwrapped, config, {}))
      {
         mng.finish_download();
         mng.on_update_step("installing");
         if (install_update(config, unwrapped, &mng))
         {
            LOG(INFO) << app << ": done";
         }
         else
         {
            mng.on_execution_error("error occurred");
         }
      }
      else
      {
         mng.on_execution_error("error occurred");
      }
   }
   else
   {
      mng.on_update_step("up to date");
      mng.finish_download();
      LOG(INFO) << app << ": is already at the current version : " << config.current_version_string;
   }
}

std::vector<std::string> watchdog::installer::get_configured_apps(const watchdog::app_base_config &config)
{
   if (config.legacy_mode)
   {
      return {std::string{"default"}};
   }
   else
   {
      std::vector<std::string> existing_apps;
      for (auto entry : boost::filesystem::directory_iterator(config.config_repo_path))
      {
         if (entry.status().type() == boost::filesystem::directory_file)
         {
            auto path = entry.path();
            auto app_name = path.stem().string();
            if (boost::starts_with(app_name, ".") || boost::starts_with(app_name, "__"))
            {
               continue;
            }

            if (exists(path / cfg::property_file))
            {
               existing_apps.push_back(app_name);
            }
         }
      }
      return existing_apps;
   }
}

watchdog::optional<watchdog::filesystem_path> watchdog::installer::backup_file(std::string session,
                                                                               filesystem_path const &path)
{
   namespace fs = boost::filesystem;
   watchdog::optional<watchdog::filesystem_path> result;

   if (fs::exists(path))
   {
      auto backup_root = path.parent_path() / "__backup__" / session;
      auto ec = boost::system::error_code{};
      fs::create_directories(backup_root, ec);

      if (ec)
      {
         LOG(WARNING) << "unable to create backup dir " << backup_root.string() << " with " << ec.message();
      }

      result = backup_root / path.filename();

      fs::copy_file(path, result.value());
   }

   return result;
}

std::string
watchdog::installer::format_error_details_for_missing_tokens(std::map<std::string, std::set<std::string>> const &tokens)
{

   auto buffer = fmt::memory_buffer{};
   buffer.reserve(1000);

   fmt::format_to(std::back_inserter(buffer), "FOLLOWING TOKENS ARE MISSING:\n");

   std::set<std::string> unique_tokens;
   for (auto &[file, missing_tokens] : tokens)
   {
      unique_tokens.insert(missing_tokens.begin(), missing_tokens.end());
      fmt::format_to(std::back_inserter(buffer), "{}: {}\n", file, fmt::join(missing_tokens, ","));
   }

   fmt::format_to(std::back_inserter(buffer), "ADD AND CONFIGURE FOLLOWING TOKENS TO channel.properties FILE:\n");
   for (auto &token : unique_tokens)
   {
      fmt::format_to(std::back_inserter(buffer), "token.{} = \n", token);
   }

   return fmt::to_string(buffer);
}

void watchdog::installer::configure_apps(const installer::app_configuration &config)
{
   auto now = std::time(nullptr);
   auto update_session_string = fmt::format("{:%Y-%m-%d}T{:%H%M%S}", fmt::localtime(now), fmt::localtime(now));

   auto configured_apps = get_configured_apps(config.config.get_channel_config());
   auto const get_cfg_template = [&](std::string const &name) -> const configuration_template &
   {
      static configuration_template empty{"empty", {}};
      if (auto it = config.configuration_templates.find(name); it != config.configuration_templates.end())
      {
         return it->second;
      }
      return empty;
   };

   const auto for_each_app = [&](auto fun)
   {
      for (auto &app_name : config.apps)
      {
         auto tokens = config.config.get_channel_config().get_script_token_map(app_name);
         auto configuration = config.config.get_config(app_name, true);

         auto is_app_configured = find(configured_apps, app_name) != configured_apps.end();
         if (!is_app_configured || config.is_configuration_migration_enabled())
         {
            fun(app_name, tokens);
         }
      }

      // I am not touching __global__ config file if new_only == true
      if (!config.new_only &&
          (!boost::filesystem::exists(config.config.get_channel_config().config_repo_path / "env.properties") ||
           config.is_configuration_migration_enabled()))
      {
         auto tokens = config.config.get_channel_config().get_script_token_map("__global__");
         fun("__global__", tokens);
      }
   };

   const auto missing_tokens = [&]
   {
      auto missing_tokens_by_file = std::map<std::string, std::set<std::string>>{};

      const auto find_missing_tokens = [&](std::string const &name, string_map const &tokens)
      {
         auto required_tokens = get_cfg_template(name).get_required_tokens();
         auto required_tokens_iter = std::vector<std::string>{required_tokens.begin(), required_tokens.end()};

         for (auto &token : required_tokens_iter)
         {
            if (auto expanded_token = command::expand_token(token, &tokens); expanded_token.has_value())
            {
               required_tokens.erase(token);
            }
         }

         return required_tokens;
      };

      for_each_app(
          [&](std::string const &name, string_map const &tokens)
          {
             auto print_name = name == "__global__" ? ""s : fmt::format("{}/", name);

             auto missing_tokens = find_missing_tokens(name, tokens);
             if (!missing_tokens.empty())
             {
                missing_tokens_by_file[fmt::format("{}env.properties", print_name)] = missing_tokens;
             }

             for (auto &child_name : get_cfg_template(name).get_child_configs())
             {
                auto missing_tokens = find_missing_tokens(child_name, tokens);
                if (!missing_tokens.empty())
                {
                   missing_tokens_by_file[fmt::format("{}{}", print_name, child_name)] = missing_tokens;
                }
             }
          });

      return missing_tokens_by_file;
   }();

   if (!missing_tokens.empty())
   {
      throw std::logic_error{format_error_details_for_missing_tokens(missing_tokens)};
   }

   for_each_app(
       [&](std::string const &name, string_map const &tokens)
       {
          auto folder_name = name == "__global__" ? ""s : fmt::format("{}/", name);
          const auto &tmpl = get_cfg_template(name);
          auto app_folder = config.config.get_channel_config().config_repo_path / folder_name;

          const auto write_properties = [&](const configuration_template &cfg_template, filesystem_path const &cfg_path)
          {
             auto content = cfg_template.format(tokens);
             auto old_content = configuration_template::read_file(cfg_path);
             if (!old_content.empty() && old_content != content)
             {
                if (auto backed_up_file = backup_file(update_session_string, cfg_path); backed_up_file.has_value())
                {
                   LOG(INFO) << "file has been backed up: " << backed_up_file.value().string()
                             << " before overwritten.";
                }
             }

             try_to_create_directories(content);
             boost::filesystem::create_directories(cfg_path.parent_path());

             boost::filesystem::ofstream out{cfg_path, std::ios_base::trunc};
             out << content;
          };

          write_properties(tmpl, app_folder / "env.properties");
          for (auto &child_name : get_cfg_template(name).get_child_configs())
          {
             write_properties(get_cfg_template(child_name), app_folder / child_name);
          }
       });
}

void watchdog::installer::try_to_create_directories(const std::string &configuration)
{
   commons::parse_properties(configuration.begin(), configuration.end(),
                             [&](std::string const &key, std::string const &value)
                             {
                                if (boost::ends_with(key, ".path"))
                                {
                                   filesystem_path path = value;
                                   if (path.has_extension())
                                   {
                                      path = path.parent_path();
                                   }

                                   boost::system::error_code ec;
                                   boost::filesystem::create_directories(path, ec);
                                }
                             });
}

void watchdog::installer::print_tokens_json(const std::string &channel_url)
{
   using namespace std::string_literals;
   auto templates = configuration_template::load_templates_from_url(channel_url);
   auto watchdog_tokens = app_base_config{}.get_script_token_map();
   auto all_tokens = std::set<std::string>{};

   for (auto [key, tmpl] : templates)
   {
      for (auto &token : tmpl.get_required_tokens())
      {
         all_tokens.insert(token);
      }
   }

   auto buffer = fmt::memory_buffer{};
   buffer.reserve(2048);

   auto json = spider::serialization::json::json_buffer{buffer};
   json.object(
       [&]
       {
          json.kv_pair_arr("system_tokens",
                           [&]
                           {
                              for (auto &[token, val] : watchdog_tokens)
                              {
                                 json.string(token);
                              }
                           });
          json.kv_pair_obj("files",
                           [&]
                           {
                              for (auto [key, tmpl] : templates)
                              {
                                 auto file = key == "__global__" ? "env.properties"s
                                             : key.find('.') == std::string::npos
                                                 ? fmt::format("{}/env.properties", key)
                                                 : key;
                                 json.kv_pair_arr(file,
                                                  [&]
                                                  {
                                                     for (auto &token : tmpl.get_required_tokens())
                                                     {
                                                        json.string(token);
                                                     }
                                                  });
                              }
                           });

          json.kv_pair_arr("required_tokens",
                           [&]
                           {
                              for (auto &token : all_tokens)
                              {
                                 json.string(token);
                              }
                           });
       });

   std::cout << to_string(buffer) << std::endl;
}

watchdog::installer::app_configuration
watchdog::installer::run_app_configuration(watchdog::filesystem_path const &channel_path,
                                           watchdog::filesystem_path const &cfg_file, bool new_only)
{
   namespace fs = boost::filesystem;
   using namespace std;

   auto base_config = parse(cfg_file);

   auto config_provider = app_group_config{base_config};

   auto &&templates = [&]
   {
      if (fs::exists(channel_path / "config-templates"))
         return configuration_template::load_templates_from_directory(channel_path / "config-templates");
      else
         return configuration_template::load_templates_from_url(base_config.channel_base_url);
   }();

   auto load_apps = [&](bool load_new_only)
   {
      auto cfg_apps = std::set<string>{};

      for (auto &app : installer::get_configured_apps(base_config))
      {
         cfg_apps.insert(app);
      }

      auto apps = load_new_only ? std::set<string>{} : cfg_apps;

      if (base_config.update_url_init)
      {
         for (auto &[app, tmpl] : templates)
         {
            if (app != "__global__" && app.find('.') == std::string::npos)
            {
               if (!load_new_only || (load_new_only && !cfg_apps.contains(app)))
               {
                  apps.insert(app);
               }
            }
         }
      }
      else
      {
         LOG(INFO) << "configuring new apps is disabled : update.url.init = false - configured are only those that "
                      "have configuration file placed within its directory";
      }

      return std::vector<std::string>{apps.begin(), apps.end()};
   };

   auto result = installer::app_configuration{.config = config_provider,
                                              .apps = load_apps(new_only),
                                              .configuration_templates = templates,
                                              .new_only = new_only};
   installer::configure_apps(result);

   if (new_only)
   {
      // we need to load them all for result
      result.apps = load_apps(false);
   }

   return result;
}