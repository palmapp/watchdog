//
// Created by jhrub on 09.01.2023.
//

#include "app_group_controller.h"

watchdog::app_group_controller::app_group_controller(status_observer_func fun) : fun_(std::move(fun)) {}

void watchdog::app_group_controller::pause(const std::string &app_group, bool sync)
{
   if (auto it = groups_.find(app_group); groups_.end() != it)
   {
      if (sync)
      {
         it->second->process(app_group_message_pause{}).wait();
      }
      else
      {
         it->second->post_message(app_group_message_pause{});
      }
   }
}
void watchdog::app_group_controller::restart(const std::string &app_group, optional<system_updater_config> new_config,
                                             bool sync)
{
   if (auto it = groups_.find(app_group); groups_.end() != it)
   {
      auto msg = app_group_message_restart{};
      msg.new_config = std::move(new_config);

      if (sync)
      {
         it->second->process(std::move(msg)).wait();
      }
      else
      {
         it->second->post_message(std::move(msg));
      }
   }
}
void watchdog::app_group_controller::status(const std::string &app_group, bool sync)
{
   if (auto it = groups_.find(app_group); groups_.end() != it)
   {
      if (sync)
      {
         it->second->process(app_group_message_status{}).wait();
      }
      else
      {
         it->second->post_message(app_group_message_status{});
      }
   }
}

void watchdog::app_group_controller::ensure_group(const std::string &group, system_updater_config config)
{
   auto new_group = std::unique_ptr<app_group>{new app_group(group, fun_)};

   auto msg = app_group_message_restart{};
   msg.new_config = std::move(config);

   new_group->post_message(std::move(msg));

   groups_[group] = std::move(new_group);
}

bool watchdog::app_group_controller::group_exists(const std::string &app_group) const
{
   auto it = groups_.find(app_group);
   return it != groups_.end();
}
void watchdog::app_group_controller::disable_group(const std::string &app_group)
{
   groups_.erase(app_group);
}

void watchdog::app_group_controller::update_user_configuration_if_needed(const std::string &app,
                                                                         system_updater_config config)
{

   if (auto it = groups_.find(app); groups_.end() != it)
   {
      auto msg = app_group_refresh_config{};
      msg.new_config = std::move(config);

      auto &group = *it;
      group.second->post_message(std::move(msg));
   }
}
void watchdog::app_group_controller::stop_all() const
{
   for (auto &[key, value] : groups_)
   {
      value->post_message(app_group_message_pause{});
   }

   for (auto &[key, value] : groups_)
   {
      value->stop();
   }
}
