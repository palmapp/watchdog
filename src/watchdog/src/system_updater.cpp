#include "system_updater.h"

#include "script_buffer_output.h"
#include "script_engine_resolver.h"
#include "script_runner.h"
#include "system_update_model.h"
#include "types.h"
#include <boost/filesystem/fstream.hpp>
#include <chrono>
#include <commons/file_system.h>
#include <commons/hex_encode.h>
#include <commons/make_string.h>
#include <commons/md5.h>
#include <commons/run_safe.h>
#include <commons/unzip.h>
#include <commons/uri_builder.h>
#include <iostream>

#include <commons/guid.h>
#include <easylogging++.h>
#include <fmt/chrono.h>
#include <fmt/format.h>
#include <fmt/ostream.h>

#include "app_group_config.h"

using namespace boost::filesystem;
using namespace commons;
namespace http = watchdog::url;

using namespace std::chrono_literals;

using namespace watchdog;

namespace
{
   std::vector<std::pair<std::time_t, boost::filesystem::path>>
   list_instance_directories(system_updater_config const &config)
   {
      std::vector<std::pair<std::time_t, path>> directories;
      for (auto &entry : boost::make_iterator_range(directory_iterator(config.apps_path / "versions"), {}))
      {
         boost::system::error_code ec;
         auto path = entry.path();
         if (path.filename().string() != config.current_version_string && is_directory(path, ec))
         {
            if (!ec)
            {
               directories.emplace_back(last_write_time(path), std::move(path));
            }
         }
      }

      return directories;
   }

   void clean_old_instances(system_updater_config const &config) noexcept
   {
      commons::run_safe(
          [&]()
          {
             auto instance_directories = list_instance_directories(config);
             if (instance_directories.size() > 1)
             {
                std::sort(instance_directories.begin(), instance_directories.end());
                instance_directories.pop_back();

                for (auto &[time, directory_path] : instance_directories)
                {
                   LOG(INFO) << "removing old instance " << directory_path.string();
                   boost::system::error_code ec;
                   remove_all(directory_path, ec);

                   if (ec)
                   {
                      LOG(WARNING) << "unable to clean old instance : " << directory_path.string() << ": ["
                                   << ec.value() << "] " << ec.message();
                   }
                }
             }

             for (auto &download : directory_iterator(config.apps_path / "downloads"))
             {
                boost::system::error_code ec;
                remove(download.path(), ec);

                if (ec)
                {
                   LOG(WARNING) << "unable to delete download : " << download.path().string() << " " << ec.message();
                }
             }
          },
          "clean_old_instances");
   }

   void finish_install_update(system_updater_config const &config, const update_model &model)
   {
      const auto tmp = config.apps_path / "active.tmp.properties";
      const auto old = config.apps_path / "active.old.properties";
      const auto active = config.apps_path / "active.properties";

      remove(tmp);
      {
         std::ofstream stream(tmp.c_str());
         stream << "#this properties file is overwritten during update\n";
         stream << "current.version = " << model.hash;
      }

      remove(old);

      boost::system::error_code ec;
      rename(active, old, ec);

      if (ec)
      {
         LOG(WARNING) << "unable to rename from active=" << active << " to old=" << old << ", error code=" << ec;
      }

      rename(tmp, active);
      remove(old);

      clean_old_instances(config);
   }

   std::string build_update_url(system_updater_config const &config)
   {
      auto uri_builder = commons::url::uri_builder{config.url};

      try
      {
         auto json = read_file((config.apps_path / "versions" / config.current_version_string / "_json"));
         watchdog::update_model model;
         if (spider::serialization::parse_structure(model, json, spider::serialization::content_type::json) &&
             !model.metadata.empty())
         {
            auto metadata_map = std::map<std::string, std::string>();
            for (auto m : model.metadata)
            {
               metadata_map[m.key] = m.value;
            }
            uri_builder = uri_builder.add_query(metadata_map);
         }
      }
      catch (...)
      {
         LOG(WARNING) << "unable to send metadata in update json url";
      }

      std::string url = uri_builder.to_string();
      return url;
   }

   void execute_script(system_updater_config const &config, const std::string &intent, const update_model &model,
                       std::string script, const std::string &old_version_string, int failed_attempts,
                       script_output_factory *output)
   {

      if (auto script_cmd = command::parse(script); script_cmd.has_value())
      {

         const std::string old_version_path = (config.apps_path / "versions" / old_version_string).string();
         const std::string new_version_path = (config.apps_path / "versions" / model.hash).string();
         const std::string utils_path = config.utils_path.string();

         auto tokens = string_map{
             {"utilspath", utils_path},
             {"oldpath", old_version_path},
             {"newpath", new_version_path},
             {"envdir", config.env_dir_path.string()},
             {"envpath", config.env_path.string()},
         };
         auto cmd_to_run = to_script_engine(script_cmd.value()).apply_tokens(&tokens);
         cmd_to_run.working_directory = new_version_path;

         auto output_ptr = output != nullptr ? output->begin("install_update") : std::shared_ptr<script_output>();
         do
         {
            script_execution_impl proc_output;
            proc_output.next_output = output_ptr != nullptr ? output_ptr.get() : nullptr;

            bool result = script_runner::run_script(intent, cmd_to_run, proc_output);

            LOG(INFO) << "result of cmd [" << intent << "]: " << cmd_to_run.to_string() << " = "
                      << proc_output.exit_code.value_or(-1);

            if (!result)
            {
               LOG(WARNING) << "script execution has failed ( remaining attempts " << failed_attempts
                            << ") with std out:\n"
                            << proc_output.std_out << "\n=====\nstd err:\n"
                            << proc_output.std_err;

               if (proc_output.error_message.has_value())
               {
                  LOG(ERROR) << "proc exception:" << proc_output.error_message.value();
               }
            }
            else
            {
               LOG(DEBUG) << "std out:\n" << proc_output.std_out << "\n=====\nstd err:\n" << proc_output.std_err;
               return;
            }
         } while (failed_attempts-- > 0);

         throw std::runtime_error(
             commons::make_string("Script: \"", cmd_to_run.to_string(), "\" has not been  successfully executed."));
      }
   }

   bool verify_download(const update_model &model, const filesystem_path &path)
   {
      auto file_hash = commons::hash::calculate_file_checksum(path);
      auto file_hash_string = encoding::hex_encode(reinterpret_cast<const void *>(file_hash.begin()), file_hash.size());

      return boost::iequals(model.hash.data(), file_hash_string.data());
   }

   void download_update(const update_model &model, const filesystem_path &path, cancellation_token token)
   {
      if (exists(path))
      {
         if (static_cast<std::size_t>(file_size(path)) == model.size && verify_download(model, path))
         {
            return;
         }

         for (int i = 0; i != 5; ++i)
         {
            if (!exists(path)) break;

            boost::system::error_code ec;
            remove(path, ec);
            if (ec)
            {
               if (!exists(path)) break;

               if (i == 4)
               {
                  throw spider::exception(
                      make_string("unable to delete file: ", path.string(), " with ", ec.message()));
               }
               else
               {
                  std::this_thread::sleep_for(std::chrono::milliseconds{500});
               }
            }
         }
      }

      http::header_container headers;
      http::fetch_to_file(path, model.update_url, headers, token);
      if (!verify_download(model, path))
      {
         remove(path);
         throw spider::exception(make_string("unable to verify checksum of ", model.update_url, " by ", model.hash));
      }
   }

   void extract_update(system_updater_config const &config, const update_model &model, const filesystem_path &path)
   {
      const auto to = config.apps_path / "versions" / model.hash;
      if (boost::filesystem::exists(to))
      {

         boost::system::error_code ec;
         boost::filesystem::remove_all(to, ec);

         if (ec)
         {
            LOG(ERROR) << "unable to delete unused version folder " << to.string() << " with " << ec.message()
                       << " please delete it manually";

            throw boost::system::system_error{ec};
         }
      }
      unzip(path, to);

      boost::filesystem::ofstream out_file(to / "_json");
      out_file << spider::serialization::json::stringify_json(model);
   }

} // namespace

void send_update_status(system_updater_config const &config, const std::string &version, bool success)
{
   if (!config.status_url.empty())
   {
      try
      {
         auto uri_builder = commons::url::uri_builder{config.status_url};
         std::string url =
             uri_builder
                 .add_query({{"type", "1"}, {"version_string", version}, {"success", success ? "true" : "false"}})
                 .to_string();
         http::send_get_request(url, {}, {}, 5s);
      }
      catch (...)
      {
         LOG(WARNING) << "could not send the update status of version: " << version;
      }
   }
}

std::optional<update_model> watchdog::check_for_update(system_updater_config const &config,
                                                       commons::cancellation_token token) noexcept
{

   std::optional<update_model> result;

   commons::run_safe(
       [&]()
       {
          std::string url = build_update_url(config);

          std::string body;
          int status = url::fetch_to_string(body, url, {}, token, 60000ms);
          if (status == 200)
          {

             update_model model;
             if (spider::serialization::parse_structure(model, body, spider::serialization::content_type::json) &&
                 !model.update_url.empty() && !model.hash.empty())
             {
                if (model.hash != config.current_version_string) result = model;
             }
             else
             {
                LOG(WARNING) << " body parse failed : " << body;
             }
          }
          else
          {
             LOG(WARNING) << "url::fetch_to_string : " << url << " : " << status;
          }
       },
       "check_for_update");

   return result;
}

bool watchdog::download_and_extract_update(update_model const &model, system_updater_config const &config,
                                           commons::cancellation_token token)
{

   auto old_version_string = config.current_version_string;
   filesystem_path path = config.apps_path / "downloads" / (model.hash + ".zip");
   bool success = commons::run_safe(
       [&]()
       {
          create_directories(path.parent_path());
          download_update(model, path, token);
          extract_update(config, model, path);
       },
       "download_and_extract_update");

   return success;
}

std::int64_t watchdog::get_downloaded_size(const system_updater_config &config, const update_model &model)
{
   filesystem_path path = config.apps_path / "downloads" / (model.hash + ".zip");

   boost::system::error_code ec;
   auto result = boost::filesystem::file_size(path, ec);
   if (!ec)
   {
      return gsl::narrow<std::int64_t>(result);
   }
   else
   {
      return 0;
   }
}

namespace watchdog
{
   namespace fs = boost::filesystem;

   struct update_status_log_file
   {
      update_status_log_file(system_updater_config const &config)
      {
         auto update_status_file = config.apps_path / "update.status";
         boost::system::error_code ec;

         if (auto size = fs::file_size(update_status_file, ec); !ec && size > 0xffff)
         {
            fs::remove(update_status_file, ec);
         }

         update_status_.open(update_status_file, std::ios_base::app);
      }

      void set_update_status(std::string_view status, std::string_view current_version,
                             std::optional<std::string_view> new_version)
      {
         auto now = std::chrono::system_clock::now();

         if (new_version.has_value())
         {
            fmt::print(update_status_, "{:%F %T} From: {} To:{} :{}", now, current_version, new_version.value(),
                       status);
         }
         else
         {
            fmt::print(update_status_, "{:%F %T} To:{} :{}", now, current_version, status);
         }

         update_status_ << std::endl;
      }

    private:
      boost::filesystem::ofstream update_status_;
   };
} // namespace watchdog

static auto g_before_update_mutex = std::mutex{};
static auto g_update_mutex = std::mutex{};
static auto g_after_update_mutex = std::mutex{};

void watchdog::run_scripts(const system_updater_config &config, const update_model &model,
                           script_output_factory *output)
{

   {
      auto lock = std::unique_lock{g_before_update_mutex};
      execute_script(config, "run_before_update", model, model.before_update_script, config.current_version_string,
                     model.before_update_script_fail_attempts, output);
   }

   {
      auto lock = std::unique_lock{g_update_mutex};
      execute_script(config, "run_update", model, model.update_script, config.current_version_string,
                     model.update_script_fail_attempts, output);
   }
}

bool watchdog::install_update(system_updater_config const &config, const watchdog::update_model &model,
                              script_output_factory *output)
{
   update_status_log_file status{config};
   bool success = commons::run_safe(
       [&]()
       {
          status.set_update_status("updating", config.current_version_string, model.hash);

          run_scripts(config, model, output);
          for (auto &[instance, config] : config.instances)
          {
             run_scripts(config, model, output);
          }

          finish_install_update(config, model);

          {
             auto lock = std::unique_lock{g_after_update_mutex};
             execute_script(config, "run_after_update", model, model.after_update_script, config.current_version_string,
                            model.after_update_script_fail_attempts, output);

             for (auto &[instance, config] : config.instances)
             {
                execute_script(config, "run_after_update", model, model.after_update_script,
                               config.current_version_string, model.after_update_script_fail_attempts, output);
             }
          }

          status.set_update_status("updated", model.hash, {});
       },
       "update");

   if (!success)
   {
      status.set_update_status("failed", model.hash, {});
   }

   return success;
}

void watchdog::run_scripts_ignore_errors(const system_updater_config &config, const update_model &model,
                                         script_output_factory *output)
{
   commons::run_safe(
       [&]()
       {
          execute_script(config, "run_before_update", model, model.before_update_script, config.current_version_string,
                         model.before_update_script_fail_attempts, output);
       },
       "run_before_update");

   commons::run_safe(
       [&]()
       {
          execute_script(config, "run_update", model, model.update_script, config.current_version_string,
                         model.update_script_fail_attempts, output);
       },
       "run_update");

   commons::run_safe(
       [&]()
       {
          execute_script(config, "run_after_update", model, model.after_update_script, config.current_version_string,
                         model.after_update_script_fail_attempts, output);
       },
       "run_after_update");
}

std::optional<watchdog::update_model> watchdog::read_update_model(const system_updater_config &config)
{
   auto json = read_file((config.apps_path / "versions" / config.current_version_string / "_json"));
   watchdog::update_model model;
   if (spider::serialization::parse_structure(model, json, spider::serialization::content_type::json))
   {
      return model;
   }
   else
   {
      return {};
   }
}

bool watchdog::revert_update(system_updater_config const &config, script_output_factory *output)
{
   namespace fs = boost::filesystem;
   bool result = false;
   commons::run_safe(
       [&]()
       {
          std::optional<watchdog::update_model> model;
          std::time_t current_version_write_time =
              fs::last_write_time(config.apps_path / "versions" / config.current_version_string);
          std::time_t time = 0;

          system_updater_config cfg = config;
          for (auto &f : fs::directory_iterator{config.apps_path / "versions"})
          {
             const auto path = f.path();
             const auto version_string = path.filename().string();
             fs::file_status status = f.status();
             if (status.type() == fs::file_type::directory_file && version_string != config.current_version_string)
             {
                cfg.current_version_string = version_string;
                auto this_model = read_update_model(cfg);
                if (this_model.has_value())
                {
                   auto this_time = fs::last_write_time(path);
                   if (this_time > time && current_version_write_time > this_time)
                   {
                      time = this_time;
                      model = this_model;
                   }
                }
             }
          }

          if (model.has_value())
          {
             LOG(INFO) << config.app_group_name << " reverting to " << model.value().hash;
             run_scripts_ignore_errors(config, model.value());

             for (auto &[instance, config] : config.instances)
             {
                run_scripts_ignore_errors(config, model.value(), output);
             }

             finish_install_update(config, model.value());

             result = true;
             LOG(INFO) << config.app_group_name << " reverted to " << model.value().hash;
          }
          else
          {
             LOG(WARNING) << config.app_group_name << ": unable to revert app group";
          }
       },
       "revert_update");

   return result;
}