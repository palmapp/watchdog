#include "configuration_template.h"
#include "urlfetch.h"
#include <boost/algorithm/string.hpp>
#include <commons/file_system.h>

#include "command_parser.h"
#include <boost/filesystem/fstream.hpp>
#include <commons/properties_parser.h>
#include <easylogging++.h>
#include <fmt/format.h>
#include <formats/picojson.h>

using namespace std::chrono_literals;

std::set<std::string> watchdog::configuration_template::get_required_tokens() const
{
   return command::parse_tokens(content, false);
}

std::string watchdog::configuration_template::format(const watchdog::string_map &tokens, bool allow_env_lookup) const
{
   return command::expand_tokens(content, &tokens, allow_env_lookup, false);
}

std::unordered_map<std::string, watchdog::configuration_template>
watchdog::configuration_template::load_templates_from_directory(const watchdog::filesystem_path &templates_dir)
{
   std::unordered_map<std::string, watchdog::configuration_template> files;
   for (auto &path : boost::filesystem::directory_iterator{templates_dir})
   {
      auto file_path = path.path();
      auto template_name = [&]()
      {
         if (file_path.extension() == ".properties")
         {
            return file_path.stem().string();
         }
         else
         {
            return file_path.filename().string();
         }
      }();

      auto content = configuration_template::read_file(file_path);
      files[template_name] = {template_name, content};
   }

   return files;
}

namespace watchdog
{
   namespace
   {
      std::optional<std::string> get_config_template(picojson::value &version, const std::string &key)
      {

         if (auto *version_obj = version.evaluate_as_object(); version_obj != nullptr)
         {
            auto props_it = version_obj->find(key);
            if (props_it != version_obj->end())
            {
               auto result = props_it->second.to_str();
               boost::replace_all(result, "\r\n", "\n");
               return result;
            }
         }
         return {};
      }

      std::vector<std::pair<std::string, std::string>> get_list_of_values(picojson::value &version,
                                                                          const std::string &key)
      {
         std::vector<std::pair<std::string, std::string>> result;
         if (auto *version_obj = version.evaluate_as_object(); version_obj != nullptr)
         {
            auto props_it = version_obj->find(key);
            if (props_it != version_obj->end())
            {
               if (auto *files = props_it->second.evaluate_as_object(); files != nullptr)
               {
                  for (auto &[key, value] : *files)
                  {
                     auto file = std::pair{key, value.to_str()};
                     boost::replace_all(file.second, "\r\n", "\n");

                     result.push_back(file);
                  }
               }
            }
         }
         return result;
      }
   } // namespace
} // namespace watchdog

std::unordered_map<std::string, watchdog::configuration_template>
watchdog::configuration_template::load_templates_from_url(const std::string &url)
{
   std::unordered_map<std::string, watchdog::configuration_template> loaded_templates;

   std::string result;
   int code = url::fetch_to_string(result, url, {}, {}, 10s);
   if (code == 200)
   {
      picojson::value val;
      picojson::parse(val, result);

      auto *obj = val.evaluate_as_object();
      if (obj != nullptr)
      {

         for (auto &[app, version] : *obj)
         {
            if (app == "__global__")
            {
               if (auto tmpl = get_config_template(version, "props"); tmpl.has_value())
               {
                  loaded_templates[app] = {app, std::move(tmpl.value())};
               }

               for (auto &[file_name, content] : get_list_of_values(version, "files"))
               {
                  loaded_templates[file_name] = {file_name, content};
               }
            }
            else
            {
               if (auto tmpl = get_config_template(version, "props"); tmpl.has_value())
               {
                  loaded_templates[app] = {app, std::move(tmpl.value())};
               }
            }
         }
      }
      else
      {
         throw std::logic_error{fmt::format("server returned an invalid JSON : body={}", result)};
      }
   }
   else
   {
      throw std::logic_error{fmt::format("server returned : status={}", code)};
   }

   return loaded_templates;
}

void watchdog::configuration_template::write_templates_to_directory(
    const std::unordered_map<std::string, configuration_template> &templates,
    const watchdog::filesystem_path &templates_dir)
{
   boost::system::error_code ec;
   boost::filesystem::create_directories(templates_dir, ec);
   if (ec)
   {
      LOG(WARNING) << "unable to create templates output directory: " << templates_dir.string()
                   << " with message = " << ec.message();
   }

   for (auto &[file, value] : templates)
   {
      auto output_file = templates_dir / file;
      if (output_file.extension().empty())
      {
         output_file += ".properties";
      }

      boost::filesystem::ofstream output{output_file, std::ios_base::out | std::ios_base::trunc};
      output << value.content;
   }
}

std::set<std::string> watchdog::configuration_template::get_child_configs() const
{
   std::set<std::string> result;
   auto can_parse = [this]
   {
      auto ext = filesystem_path{name}.extension();
      if (ext.empty() || ext == ".properties")
      {
         return true;
      }
      else
      {
         return false;
      }
   }();

   if (can_parse)
   {
      commons::parse_properties(content.begin(), content.end(),
                                [&](std::string_view key, std::string_view value)
                                {
                                   if (key == "watchdog.logging")
                                   {
                                      result.insert(static_cast<std::string>(value));
                                   }
                                   else if (key == "watchdog.configure")
                                   {
                                      boost::split(result, value, boost::is_any_of(",; "));
                                   }
                                });

      // safety measure to disallow infinite recursion
      result.erase(name);
   }

   return result;
}

std::string watchdog::configuration_template::read_file(const watchdog::filesystem_path &path)
{
   auto content = commons::read_file(path);
   boost::replace_all(content, "\r\n", "\n");

   return content;
}
