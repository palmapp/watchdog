//
// Created by jhrub on 22.09.2022.
//
#include "command_parser.h"
#include <boost/spirit/home/x3.hpp>
#include <fmt/format.h>
#include <string_view>
using namespace std::string_view_literals;
namespace x3 = boost::spirit::x3;
using x3::char_;
using x3::lexeme;
using x3::lit;
using x3::standard::space;

std::optional<watchdog::command> watchdog::command::parse(std::string const &cmd)
{
   auto result = parse_command_string(cmd);
   std::optional<watchdog::command> result_cmd{};

   if (!result.empty())
   {
      watchdog::command command;
      command.command_string = result[0];
      result.erase(result.begin());
      command.arguments = std::move(result);

      result_cmd = std::move(command);
   }

   return result_cmd;
}

std::string watchdog::command::to_string(bool include_command_string) const
{
   fmt::memory_buffer buffer{};
   buffer.reserve(256);

   const auto append_part = [&](const std::string &part)
   {
      if (buffer.size() > 0)
      {
         buffer.append(" "sv);
      }

      if (part.find(' ') == std::string::npos && part.find('\t') == std::string::npos)
      {
         buffer.append(part);
      }
      else
      {
         fmt::format_to(std::back_inserter(buffer), "\"{}\"", part);
      }
   };

   if (include_command_string)
   {
      append_part(command_string);
   }

   std::ranges::for_each(arguments, append_part);

   return fmt::to_string(buffer);
}

watchdog::command watchdog::command::apply_tokens(const string_map *tokens, bool allow_env_lookup) const
{
   auto result = *this;
   result.command_string = expand_tokens(result.command_string, tokens, allow_env_lookup);

   for (auto &arg : result.arguments)
   {
      arg = expand_tokens(arg, tokens, allow_env_lookup);
   }

   return result;
}

namespace watchdog
{
   namespace
   {

      template <bool allow_bash_style_variables>
      void _parse_command_string_tokens(std::string const &str, optional<char> &delimiter, auto on_dolar_var,
                                        auto on_var, auto on_string)
      {
         const auto my_space = char_(" \t");

         const auto variable_token = []()
         {
            if constexpr (allow_bash_style_variables)
            {
               return char_('$') | x3::lit("{{{");
            }
            else
            {
               return x3::lit("{{{");
            }
         }();

         const auto token_delimiter = char_('/') | char_('\\') | my_space | char_('.') | variable_token;

         const auto push_char = [&](char c) { delimiter = c; };

         const auto make_push_delimiter = [&](char c)
         {
            const auto action = [=](auto &ctx) { push_char(c); };

            return char_(c)[action];
         };

         const auto token_delimiter_push = make_push_delimiter('/') | make_push_delimiter('\\') | variable_token |
                                           make_push_delimiter('\t') | make_push_delimiter(' ') |
                                           make_push_delimiter('.');

         const auto mustache_variable = lexeme["{{{" >> +(char_ - "}}}") >> "}}}"];
         const auto dolar_variable = lexeme['$' >> +(char_ - token_delimiter)];
         const auto string = lexeme[+(char_ - variable_token)];

         const auto grammar = [&]()
         {
            if constexpr (allow_bash_style_variables)
            {
               return lexeme[(dolar_variable[on_dolar_var] | mustache_variable[on_var]) % variable_token];
            }
            else
            {
               return lexeme[mustache_variable[on_var] % variable_token];
            }
         }();

         auto it = str.begin();
         while (it != str.end())
         {
            x3::phrase_parse(it, str.end(), grammar, string[on_string]);
         }
      }
   } // namespace
} // namespace watchdog

std::string watchdog::command::expand_tokens(std::string const &str, const watchdog::string_map *tokens,
                                             bool allow_env_lookup, bool allow_bash_style_variables)
{
   if (tokens == nullptr && allow_env_lookup == false)
   {
      return str;
   }

   optional<char> delimiter;

   std::string buffer;
   buffer.reserve(str.size());

   const auto on_var = [&](auto &attr)
   {
      std::string var = boost::spirit::x3::_attr(attr);
      auto token_value = expand_token(var, tokens, allow_env_lookup);
      if (token_value.has_value())
      {
         buffer.append(token_value.value());
      }

      if (delimiter.has_value())
      {
         buffer += delimiter.value();
         delimiter.reset();
      }
   };

   const auto on_dolar_var = [&](auto &attr)
   {
      std::string var = boost::spirit::x3::_attr(attr);
      if (allow_bash_style_variables)
      {
         auto token_value = expand_token(var, tokens, allow_env_lookup);
         if (token_value.has_value())
         {
            buffer.append(token_value.value());
         }
      }
      else
      {
         buffer += '$';
         buffer += var;
      }

      if (delimiter.has_value())
      {
         buffer += delimiter.value();
         delimiter.reset();
      }
   };

   const auto on_string = [&](auto &attr)
   {
      /*adding string to a buffer */
      buffer += boost::spirit::x3::_attr(attr);
   };

   if (allow_bash_style_variables)
   {
      _parse_command_string_tokens<true>(str, delimiter, on_dolar_var, on_var, on_string);
   }
   else
   {
      _parse_command_string_tokens<false>(str, delimiter, on_dolar_var, on_var, on_string);
   }
   return buffer;
}

void watchdog::command::push_command(const watchdog::command &cmd)
{
   arguments.push_back(cmd.command_string);
   arguments.insert(arguments.end(), cmd.arguments.begin(), cmd.arguments.end());
}

std::vector<std::string> watchdog::command::parse_command_string(const std::string &str)
{
   const auto my_space = char_(" \t");

   const auto double_quoted_string = lexeme['"' >> +(char_ - '"') >> '"'];
   const auto single_quoted_string = lexeme['\'' >> +(char_ - '\'') >> '\''];
   const auto no_quotes_string = +(char_ - my_space);

   const auto item = single_quoted_string | double_quoted_string | no_quotes_string;

   std::vector<std::string> result;
   x3::parse(str.begin(), str.end(), (item % my_space), result);

   return result;
}

std::set<std::string> watchdog::command::parse_tokens(const std::string &str, bool allow_bash_style_variables)
{
   auto delimiter = optional<char>{};

   auto result = std::set<std::string>{};
   const auto on_dolar_var = [&](auto &attr)
   {
      if (allow_bash_style_variables)
      {
         std::string value = boost::spirit::x3::_attr(attr);
         result.insert(std::move(value));
      }
   };

   const auto on_var = [&](auto &attr)
   {
      std::string value = boost::spirit::x3::_attr(attr);
      result.insert(std::move(value));
   };

   if (allow_bash_style_variables)
   {
      _parse_command_string_tokens<true>(str, delimiter, on_dolar_var, on_var,
                                         [&](auto &) {

                                         });
   }
   else
   {
      _parse_command_string_tokens<false>(str, delimiter, on_dolar_var, on_var,
                                          [&](auto &) {

                                          });
   }

   return result;
}

std::optional<std::string> watchdog::command::expand_token(const std::string &var, const watchdog::string_map *tokens,
                                                           bool allow_env_lookup)
{
   if (tokens != nullptr)
   {
      if (auto it = tokens->find(var); it != tokens->end())
      {
         return {it->second};
      }
   }

   if (allow_env_lookup)
   {
      const char *env = std::getenv(var.c_str());
      if (env != nullptr && *env != 0)
      {
         return {{env}};
      }
   }

   return {};
}
