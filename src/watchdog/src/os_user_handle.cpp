#include "os_user_handle.h"

#include <Windows.h>
#include <boost/system/system_error.hpp>
#include <codecvt>
#include <gsl/narrow>
#include <locale>
#include <utf8cpp/utf8.h>

namespace watchdog
{
   struct os_user_handle_impl
   {
      const HANDLE handle;
      const std::string name;
      explicit os_user_handle_impl(const HANDLE handle, std::string name) noexcept
          : handle(handle), name(std::move(name))
      {
      }

      ~os_user_handle_impl() noexcept
      {
         CloseHandle(handle);
      }
   };
} // namespace watchdog

namespace
{
   auto get_wchar_ptr(std::u16string &str) -> wchar_t *
   {
      if (str.empty()) return nullptr;

      return reinterpret_cast<wchar_t *>(str.data());
   }
} // namespace
watchdog::os_user_handle::os_user_handle(os_user const &user)
{
   auto name_16 = utf8::utf8to16(user.name);
   auto password_16 = utf8::utf8to16(user.password);
   auto domain_16 = utf8::utf8to16(user.domain);

   HANDLE handle = {};
   auto result = LogonUserW(get_wchar_ptr(name_16), get_wchar_ptr(domain_16), get_wchar_ptr(password_16),
                            user.interactive ? LOGON32_LOGON_INTERACTIVE : LOGON32_LOGON_SERVICE,
                            LOGON32_PROVIDER_DEFAULT, &handle);

   if (!result)
   {
      DWORD lastError = GetLastError();
      auto ec = boost::system::error_code(gsl::narrow<int>(lastError), boost::system::system_category());
      throw boost::system::system_error(ec, "Failed to logon user");
   }

   impl_ = std::make_unique<os_user_handle_impl>(handle, user.name);
}

watchdog::os_user_handle::~os_user_handle() noexcept {}

watchdog::os_user_handle::os_user_handle(os_user_handle &&other) noexcept : impl_(std::move(other.impl_)) {}

watchdog::os_user_handle &watchdog::os_user_handle::operator=(os_user_handle &&other) noexcept
{
   impl_ = std::move(other.impl_);
   return *this;
}

std::string watchdog::os_user_handle::get_name() const
{
   return impl_->name;
}

boost::winapi::HANDLE_ watchdog::os_user_handle::get_handle() const
{
   return impl_->handle;
}

#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/fusion/include/std_pair.hpp>
#include <boost/fusion/include/std_tuple.hpp>

#include <boost/spirit/home/x3.hpp>

BOOST_FUSION_ADAPT_STRUCT(watchdog::os_user, domain, name, interactive, password)

namespace os_user_grammar
{
   namespace x3 = boost::spirit::x3;
   const auto skipper = x3::char_('\\') | '?' | ':';
   const auto string = x3::no_skip[+(x3::char_ - skipper)];
   const auto username = string;
   const auto domain = string;
   const auto password = string;
   const auto interactive = x3::lit("interactive") >> x3::attr(true);

   const auto domain_user_parser = -(domain >> '\\') >> username >> -('?' >> interactive) >> -(':' >> password);

} // namespace os_user_grammar

std::optional<watchdog::os_user> watchdog::os_user::parse(std::string_view config_string)
{
   using namespace os_user_grammar;

   {
      os_user user;
      auto first = config_string.begin();
      auto end = config_string.end();
      if (boost::spirit::x3::phrase_parse(first, end, domain_user_parser, x3::space, user) && first == end)
      {
         return user;
      }
   }

   return std::nullopt;
}