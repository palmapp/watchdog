//
// Created by jhrub on 13.11.2020.
//

#include "react_app.h"
#include <boost/algorithm/string.hpp>
#include <chrono>
#include <commons/file_system.h>
#include <commons/make_string.h>
#include <easylogging++.h>
#include <mstch/mstch.hpp>
#include <watchdog/build.h>

using namespace std;

watchdog::react_app::react_app(std::string scope_path, spider::filesystem_path react_index_html_path)
    : scoped_app_base(std::move(scope_path)), react_index_html_path_(std::move(react_index_html_path))
{
   reload_template_if_changed();
}
bool watchdog::react_app::is_current_scoped_path(spider::string_view scope)
{
   using namespace boost;
   return scoped_app_base::is_current_scoped_path(scope) && ends_with(scope, "/") && !starts_with(scope, "/api/");
}
void watchdog::react_app::handle_request_internal(spider::request &request, spider::request_handler_callback callback)
{
   mstch::map context;
   context["custom_script"] = generate_custom_script(request);
   auto body = mstch::render(template_, context);

   auto response = request.make_response();
   response->set("content-type", "text/html");
   response->body() = body;

   request.write_response(response, callback);
}
std::string watchdog::react_app::load_react_app_template() const
{
   auto content = commons::read_file(react_index_html_path_);
   if (content.empty())
   {
      content =
          "<!DOCTYPE html><html><body><h1>React template is not loaded. Check log for details.</h1></body></html>";
      LOG(WARNING) << "react application is not present (path = '" << react_index_html_path_.string() << "'";
   }
   else
   {
      boost::replace_all(content, "<head>", "<head><script type='text/javascript'>{{{custom_script}}}</script>");
   }

   return content;
}

std::string watchdog::react_app::generate_custom_script(spider::request &request)
{
   return fmt::format("window.production = true; window.watchdogBuild=\'{}\'", build_info::to_string());
}

void watchdog::react_app::reload_template_if_changed()
{
   boost::system::error_code ec;
   auto time_point = chrono::system_clock::from_time_t(last_write_time(react_index_html_path_, ec));

   if (ec)
   {
      LOG(ERROR) << "unable to check last_write_time : \"" << react_index_html_path_.string() << "\" [" << ec.value()
                 << "] " << ec.message();
      template_ = load_react_app_template();
   }
   else if (time_point != last_change_)
   {
      LOG(DEBUG) << "realoading react template";

      template_ = load_react_app_template();
      last_change_ = time_point;
   }
   else
   {
      LOG(DEBUG) << "no react template reaload needed";
   }
}
