#include <boost/application.hpp>

#include "installer.h"
#include "types.h"
#include "watchdog.h"
#include <commons/cmd_parser.h>

#include <string>
#include <thread>

#include "urlfetch.h"
#include <app_group.h>
#include <app_group_config.h>
#include <boost/dll/runtime_symbol_info.hpp>
#include <commons/file_system.h>
#include <commons/logging.h>
#include <commons/make_string.h>

#define ELPP_NO_DEFAULT_LOG_FILE
#include <easylogging++.h>

#include "default_update_manager.h"

#include "app_group_controller.h"
#include "web_interface.h"
#include <commons/ignore_sig_pipe.h>
#include <set>
#include <watchdog/build.h>
#include <watchdog_status.h>

using namespace watchdog;
using namespace std::chrono_literals;
using namespace std::string_literals;

INITIALIZE_EASYLOGGINGPP
filesystem_path g_exe_directory;

struct init_config
{
   filesystem_path config_path;
   filesystem_path dump_file_path;
   bool group_mode;
   bind_settings settings;
};

class watchdog_app
{
 public:
   watchdog_app(boost::application::context &context) : context_(context) {}

   std::optional<app_base_config> get_legacy_mode_config_base(init_config const &init)
   {

      if (!init.group_mode)
      {
         app_base_config config;
         config.legacy_mode = true;
         config.config_repo_path = init.config_path.parent_path();
         config.app_repo_path = init.config_path.parent_path();
         config.app_logging_path = init.config_path.parent_path() / "logs";
         config.kill_timeout = std::chrono::seconds{15};
         config.auto_configuration = false;
         config.auto_update_enabled = web_interface_ == nullptr;
         config.update_url_init = false;

         auto loaded_config = parse(init.config_path);
         config.update_force_on_start = loaded_config.update_force_on_start;
         config.update_check_interval = loaded_config.update_check_interval;
         config.kill_timeout = loaded_config.kill_timeout;
         config.channel_base_url = loaded_config.channel_base_url;
         config.auto_update_enabled = web_interface_ == nullptr || loaded_config.auto_update_enabled;

         return {config};
      }
      else
      {
         return {};
      }
   }

   void run_in_group_mode(init_config const &init)
   {
      using namespace boost::filesystem;

      auto update_manager = std::make_shared<default_update_manager>();
      auto controller =
          std::make_shared<app_group_controller>(std::bind(&watchdog_app::on_event, this, std::placeholders::_1));

      if (web_interface_ != nullptr)
      {
         web_interface_->set_app_group_controller(controller);
         web_interface_->set_update_manager(update_manager);
      }

      update_manager->on_stop_app_group_before_update.connect([=](std::string app_group)
                                                              { controller->pause(app_group); });

      update_manager->on_restart_app_group.connect([=](std::string app_group, system_updater_config cfg)
                                                   { controller->restart(app_group, std::move(cfg)); });

      auto counter = 0ll;
      auto st = context_.find<boost::application::status>();
      auto last_update_check = std::chrono::steady_clock::now();

      while (st->state() != boost::application::status::application_state::stopped)
      {
         auto now = std::chrono::steady_clock::now();
         auto config = parse(init.config_path / "channel.properties", get_legacy_mode_config_base(init));
         auto auto_update_enabled = web_interface_ == nullptr || config.auto_update_enabled;

         if (auto_update_enabled && counter == 0 && config.update_force_on_start)
         {
            LOG(INFO) << "forcing update on start";
            if (update_manager->download_update(config))
            {
               update_manager->install_update(nullptr);
            }
         }

         if (web_interface_ != nullptr)
         {
            // updates scripts without restart of the watchdog
            web_interface_->set_app_base_config(config);
         }

         auto apps = update_manager->check_configured_apps(config);
         check_app_group_status(*controller, config, apps);

         if (auto_update_enabled && config.update_check_interval > std::chrono::seconds{0} &&
             now - last_update_check >= config.update_check_interval)
         {
            LOG(INFO) << "auto checking for update";
            if (update_manager->download_update(config))
            {
               LOG(INFO) << "updates downloaded : installing";
               update_manager->install_update(web_interface_ != nullptr ? &web_interface_->get_script_buffer_output()
                                                                        : nullptr);
            }
            else
            {
               LOG(INFO) << "no updates available";
            }

            last_update_check = std::chrono::steady_clock::now();
         }

         if ((counter % 12) == 0)
         {
            if (counter > 0) status_.print_current_status();
         }

         std::this_thread::sleep_for(5s);
         ++counter;
      }

      controller->stop_all();
   }

   void check_app_group_status(app_group_controller &ctrl, const app_base_config &config,
                               const std::vector<std::string> &existing_apps)
   {
      for (auto const &app : existing_apps)
      {
         if (ctrl.group_exists(app))
         {
            if (config.disabled_apps.find(app) != config.disabled_apps.end())
            {
               LOG(INFO) << "disabling app group : " << app;
               ctrl.disable_group(app);
            }
            else
            {
               ctrl.update_user_configuration_if_needed(app, app_group_config{config}.get_config(app, true));
            }
         }
         else if (config.disabled_apps.find(app) == config.disabled_apps.end())
         {
            LOG(INFO) << "starting app group : " << app;
            ctrl.ensure_group(app, app_group_config{config}.get_config(app, true));
         }
      }
   }

   int operator()()
   {
      auto init = *context_.find<init_config>();

      if (init.settings.http_port > 0)
      {
         auto ip_string = [&]() -> std::string
         {
            if (init.settings.bind_all)
            {
               return "<all IP addresses>"s;
            }
            else
            {
               return init.settings.bind_ip.value_or(boost::asio::ip::address_v4::loopback()).to_string();
            }
         }();

         LOG(INFO) << "web interface is active on http://" << ip_string << ":" << init.settings.http_port;

         commons::ignore_sig_pipe();

         web_interface_ = std::make_shared<web_interface>(advanced_update_manager_cts_.get_cancellation_token(),
                                                          init.settings, init.dump_file_path.string());
         web_interface_->start_thread();
      }

      run_in_group_mode(init);

      LOG(INFO) << "program exited";

      return 0;
   }

   bool stop()
   {
      LOG(INFO) << "stop signalled";

      advanced_update_manager_cts_.cancel();

      return true; // return true to stop, false to ignore
   }

   // windows specific (ignored on posix)

   bool pause()
   {
      return false; // return true to pause, false to ignore
   }

   bool resume()
   {
      return false; // return true to resume, false to ignore
   }

   void on_event(status_event evt)
   {
      status_.on_event(evt);
      if (web_interface_)
      {
         web_interface_->on_event(evt);
      }
   }

 private:
   boost::application::context &context_;
   watchdog_status status_;

   commons::cancellation_token_source advanced_update_manager_cts_;
   std::shared_ptr<web_interface> web_interface_;

   std::mutex mutex_;
};

int main(int argc, const char *argv[])
{
   try
   {
      g_exe_directory = boost::dll::program_location().parent_path();
      init_config cfg;
      cfg.config_path = g_exe_directory;
      cfg.group_mode = false;
      cfg.settings.http_port = 0;
      auto &a_settings = cfg.settings;

      if (!commons::cmd::try_get_arg(cfg.config_path, "-c", argc, argv))
      {
         if (commons::cmd::try_get_arg(cfg.config_path, "-cg", argc, argv))
         {
            cfg.group_mode = true;
         }
      }
      else
      {
         if (!cfg.config_path.is_absolute())
         {
            if (cfg.config_path.string().at(0) != '.')
            {
               cfg.config_path = boost::filesystem::path{"."} / cfg.config_path;
            }
            cfg.config_path = boost::filesystem::weakly_canonical(cfg.config_path);
         }
      }

      if (!commons::cmd::try_get_arg(cfg.dump_file_path, "--dump-files", argc, argv))
      {
         cfg.dump_file_path = "C:\\temp";
      }

      std::string manual_update_http_port;

      if (commons::cmd::try_get_arg(manual_update_http_port, "--use-advanced-updater", argc, argv))
      {
         std::vector<std::string> parts;
         boost::split(parts, manual_update_http_port, boost::is_any_of(":"));

         if (parts.size() == 1)
         {
            a_settings.http_port = atoi(manual_update_http_port.c_str());
            if (a_settings.http_port <= 0 || a_settings.http_port > 0xffff)
            {
               LOG(ERROR) << manual_update_http_port << " is not a valid TCP port number. Exiting...";
               return 1;
            }

            a_settings.bind_all = false;
         }
         else if (parts.size() == 2)
         {
            if (parts[0] == "public")
            {
               a_settings.bind_all = true;
            }
            else
            {
               boost::system::error_code ec;
               a_settings.bind_ip = boost::asio::ip::address::from_string(parts.at(0), ec);

               if (ec)
               {
                  LOG(ERROR) << parts.at(0) << " is not a valid ip address. " << ec.message() << " Exiting ...";
                  return 1;
               }
            }

            a_settings.http_port = atoi(parts.at(1).c_str());
            if (a_settings.http_port <= 0 || a_settings.http_port > 0xffff)
            {
               LOG(ERROR) << manual_update_http_port << " is not a valid TCP port number. Exiting...";
               return 1;
            }
         }
         else
         {
            LOG(ERROR) << manual_update_http_port << " is not a valid  <ip address>:<port>. Exiting ...";
            return 1;
         }
      }

      boost::system::error_code ec;
      auto canonized = boost::filesystem::canonical(cfg.config_path, ec);
      if (!ec)
      {
         cfg.config_path = canonized;
      }

      LOG(INFO) << "Watchdog " << watchdog::build_info::to_string();
      LOG(INFO) << "starting with : " << cfg.config_path
                << (cfg.group_mode ? " group mode enabled" : " legacy mode enabled (no support for bad utility)");

      commons::init_easylogging(argc, argv, "watchdog", cfg.config_path, nullptr, true);

      boost::application::context ctx;
      ctx.insert(boost::application::csbl::make_shared<init_config>(cfg));

      boost::application::auto_handler<watchdog_app> app(ctx);
      boost::system::error_code code;

      std::string mode;
      if (!commons::cmd::try_get_arg(mode, "--mode", argc, argv) || mode == "console")
      {
         LOG(INFO) << "starting as a console";

         auto res = boost::application::launch<boost::application::common>(app, ctx, code);
         if (code)
         {
            LOG(FATAL) << "result error code: " << code.value() << " => " << code.message();
         }

         return res;
      }
      else if (mode == "service")
      {
         LOG(INFO) << "starting as a service";
         /* code */
         auto res = boost::application::launch<boost::application::server>(app, ctx, code);
         if (code)
         {
            LOG(FATAL) << "result error code: " << code.value() << " => " << code.message();
         }

         return res;
      }
      else
      {
         LOG(ERROR) << "invalid startup mode : " << mode
                    << " supported modes are service or console, the default mode is console";
         return 1;
      }
   }
   catch (const std::exception &ex)
   {
      LOG(FATAL) << "exception has been thrown: " << ex.what();
   }
   catch (...)
   {
      LOG(FATAL) << "uknown exception has been thrown";
   }
}