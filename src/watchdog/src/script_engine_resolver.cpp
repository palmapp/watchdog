//
// Created by jhrub on 22.09.2022.
//
#include "script_engine_resolver.h"
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <boost/process/search_path.hpp>

using boost::process::search_path;

namespace watchdog
{
   namespace
   {
      std::string get_command_extension(const filesystem_path &command)
      {
         return boost::to_lower_copy(command.extension().string());
      }

   } // namespace
} // namespace watchdog

watchdog::command watchdog::to_script_engine(command const &cmd)
{
   auto ext = get_command_extension({cmd.command_string});
   if (ext == ".exe" || ext.empty())
   {
      return cmd;
   }
   else if (ext == ".cmd" || ext == ".bat")
   {
      auto result_command = command{};
      result_command.command_string = search_path({"cmd.exe"}).string();
      result_command.arguments.push_back("/C");
      result_command.push_command(cmd);

      return result_command;
   }
   else if (ext == ".ps1")
   {
      auto result_command = command{};
      result_command.command_string = search_path({"powershell.exe"}).string();
      result_command.arguments.push_back("-Command");
      result_command.push_command(cmd);

      return result_command;
   }
   else if (ext == ".sh")
   {
      auto result_command = command{};
      result_command.command_string = search_path({"bash"}).string();
      result_command.push_command(cmd);
      return result_command;
   }
   else if (ext == ".py")
   {
      auto result_command = command{};
      result_command.command_string = search_path({"python"}).string();
      result_command.push_command(cmd);

      return result_command;
   }
   else if (ext == ".js")
   {
      auto result_command = command{};
      result_command.command_string = search_path({"node"}).string();
      result_command.push_command(cmd);

      return result_command;
   }

   return cmd;
}
