//
// Created by jhrub on 08.09.2022.
//
#include "advanced_script_executor.h"

#include "script_engine_resolver.h"
#include "script_runner.h"
using namespace std::string_view_literals;

void watchdog::advanced_script_executor::async_run_script(std::string name, watchdog::command cmd,
                                                          watchdog::script_output_factory &output)
{
   auto output_ptr = output.begin("scripts");
   boost::asio::post(thread_, [=]() { script_runner::run_script(name, cmd, *output_ptr); });
}
