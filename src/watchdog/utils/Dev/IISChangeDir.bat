@echo off

SETLOCAL ENABLEDELAYEDEXPANSION

set oldSitePath=%1
set newSitePath=%2
set envProperties=%3

%~dp0/CreateDatabase/CreateDatabase.exe %envProperties%

if exist %2/webupdater/WebUpdater.exe (
	@echo Running WebUpdater 
	@echo %newSitePath%/webupdater/WebUpdater.exe %envProperties% %newSitePath%\Web.config
	%newSitePath%/webupdater/WebUpdater.exe %envProperties% %newSitePath%\Web.config
	if NOT errorlevel 0 (
		echo Web updater failed
		EXIT /B 1		
	)
)
ENDLOCAL

:end
	EXIT /B 0		