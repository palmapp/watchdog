@echo off
rem This script sends updating to the current VBUS instance

set UTILS=%~dp0
PowerShell -File %UTILS%RunNotify.ps1 updating %1

EXIT /B 0