$exe="$PSScriptRoot\py.exe" 
$args="$PSScriptRoot\notify.py $args"

echo $args
$proc = Start-Process -FilePath $exe -ArgumentList $args -WindowStyle hidden -PassThru
$timeouted = $null # reset any previously set timeout
Wait-Process -Id $proc.Id -Timeout 5 -ErrorVariable timeouted -ErrorAction SilentlyContinue

if ($timeouted)
{
	$proc | kill
	echo "Process has timedout"
}
else
{
	echo Finished
}
