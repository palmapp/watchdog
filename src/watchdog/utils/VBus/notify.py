import os
import sys
from os import path

ALPHA_SERVER = 0
BETA_SERVER = 1
USA_PROD_SERVER = 2
CAD_PROD_SERVER = 3

VBUS_CLI_COMMANDS = {
    ALPHA_SERVER: r"C:\vitrium\watched-apps\alpha\vbus-cli.bat",
    BETA_SERVER: r"C:\vitrium\watched-apps\beta\vbus-cli.bat",
    USA_PROD_SERVER: r"vbus-cli",
    CAD_PROD_SERVER: r"vbus-cli",
}

def get_channel_prefix(site_path):
    components = site_path.split(path.sep)
    app_name = components[-3]

    dir_components = []
    for c in components:
        if c == app_name:
            dir_components = dir_components [:-1]
            dir_components.append("infmon.channel.prefix")
            break
        
        dir_components.append(c)
    
    prefix_path = path.sep.join(dir_components)
    
    try:
        print prefix_path
        with open(prefix_path) as f:
            return f.read()
    except:
        print "not found: ", prefix_path
        return ""

def get_exec_environment(site_path):
    components = site_path.split(path.sep)
    app_name = components[-3]
    server = USA_PROD_SERVER

    if path.exists(r"C:\vitrium\watched-apps\alpha"):
        if "alpha" in components:
            server = ALPHA_SERVER
        elif "beta" in components:
            server = BETA_SERVER
        else:
            raise Exception("uknown server")

    return (app_name, server,)


def send_command(cmd, site_path):
    app_name, server = get_exec_environment(site_path)
    vbus = VBUS_CLI_COMMANDS[server]
    system_cmd = "{0} --cmd pub {3}/app-state/{1} {2}".format(vbus, app_name, cmd, get_channel_prefix(site_path))

    print(system_cmd)
    os.system(system_cmd)


if __name__ == "__main__":
    cmd = sys.argv[1]
    site_path = sys.argv[2]
    send_command(cmd, site_path)
