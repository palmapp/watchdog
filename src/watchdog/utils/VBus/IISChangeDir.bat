@echo off
rem This script only execute the DB update
SETLOCAL ENABLEDELAYEDEXPANSION

set oldSitePath=%1
set newSitePath=%2
set envProperties=%3

if exist %2/webupdater/WebUpdater.exe (
	@echo Running WebUpdater 
	@echo %newSitePath%/webupdater/WebUpdater.exe %envProperties% %newSitePath%\Web.config
	%newSitePath%/webupdater/WebUpdater.exe %envProperties% %newSitePath%\Web.config
	if errorlevel 1 (
		echo Web updater failed
		EXIT /B 1		
	)
)

EXIT /B 0

ENDLOCAL
:end
