@SETLOCAL ENABLEDELAYEDEXPANSION
@echo off

if exist C:\vitrium\watched-apps\alpha   GOTO ALPHA_BETA_SERVER

:GENERIC_CASE
set VBUS_PATH=%1
if exist %1\vbus-cli.exe (
    echo @%VBUS_PATH:"=%\vbus-cli.exe -c %2 %%* > "%windir%\vbus-cli.bat"
)
GOTO END
:ALPHA_BETA_SERVER
    set VBUS_PATH=%1

    echo %VBUS_PATH% | find "alpha" > nul
    if not errorlevel 1 (echo @%VBUS_PATH:"=%\vbus-cli.exe -c %2 %%* > "C:\vitrium\watched-apps\alpha\vbus-cli.bat")

    echo %VBUS_PATH% | find "beta" > nul
    if not errorlevel 1 (echo @%VBUS_PATH:"=%\vbus-cli.exe -c %2 %%* > "C:\vitrium\watched-apps\beta\vbus-cli.bat")

GOTO END
:END
    echo InitVbusCli has finished
