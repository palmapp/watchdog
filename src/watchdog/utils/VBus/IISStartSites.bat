@echo off
rem This script sends ready to the current VBUS instance

set UTILS=%~dp0
PowerShell -File %UTILS%RunNotify.ps1 ready %1

EXIT /B 0