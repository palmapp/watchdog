@echo off

SETLOCAL ENABLEDELAYEDEXPANSION

set newSitePath=%1

SET count=1
FOR /F "tokens=* USEBACKQ" %%F IN (`C:\Windows\System32\inetsrv\appcmd.exe list app /text:APP.NAME`) DO (
  SET iisApp!count!=%%F
  SET /a count=!count!+1
)
set count=1
:while
IF DEFINED iisApp!count! (
	for /f "tokens=*" %%i in ('echo !iisApp%count%!') do set appName=%%i
	for /f "tokens=*" %%i in ('C:\Windows\System32\inetsrv\appcmd.exe list app "!appName!" /text:[path^='/'].physicalPath') do set localSitePath=%%i
	IF /I "!localSitePath!"=="%newSitePath%" (
		C:\Windows\System32\inetsrv\appcmd.exe start site !appName!
		SET found=1
	)
	SET /a count=!count!+1
	GOTO while
) 

IF not defined found ( 
	ECHO "IIS site not found for path %newSitePath%"
)

EXIT /B 0

ENDLOCAL
