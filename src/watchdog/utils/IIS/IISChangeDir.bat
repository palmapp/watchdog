@echo off

SETLOCAL ENABLEDELAYEDEXPANSION

set oldSitePath=%1
set newSitePath=%2
set envProperties=%3
set scriptDir=%~dp0

if exist %2/webupdater/WebUpdater.exe (
	@echo Running WebUpdater
	@echo %newSitePath%/webupdater/WebUpdater.exe %envProperties% %newSitePath%\Web.config
	%newSitePath%/webupdater/WebUpdater.exe %envProperties% %newSitePath%\Web.config
	if errorlevel 1 (
		echo Web updater failed
		EXIT /B 1
	)
)

rem if oldSitePath ends with "versions" it is a new installation and we no longer need to change the dir
if "%oldSitePath:~-8%" == "versions" (
    EXIT /B 0
)

@echo Changing dir

%scriptDir%AppCmd.Cli\AppCmd.Cli.exe --change-vdir "%oldSitePath%" "%newSitePath%"
if errorlevel 1 (
    echo Change dir failed
    EXIT /B 1
)

EXIT /B 0

ENDLOCAL
:end
