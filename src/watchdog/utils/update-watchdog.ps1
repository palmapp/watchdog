function Check-IsElevated
 {
    $id = [System.Security.Principal.WindowsIdentity]::GetCurrent()
    $p = New-Object System.Security.Principal.WindowsPrincipal($id)
    if ($p.IsInRole([System.Security.Principal.WindowsBuiltInRole]::Administrator))
   { Write-Output $true }      
    else
   { Write-Output $false }   
 }
 
if(Check-IsElevated)
{
$install_dir =$args[0] | Resolve-Path
$new_watchodg_dir =$args[1] | Resolve-Path

$watchdog_path =  "$($args[0])\watchdog.exe"

If(Test-Path -Path $watchdog_path)
{
	echo "$($args[0]) is watchdog installation directory"
	$watchodog_services = Get-WmiObject win32_service | ?{$_.PathName -like "$watchdog_path*"} | select Name
	
	foreach($service in $watchodog_services)
	{
		echo "Stopping: $($service.Name)"
		
		#Start-Job {
		#		param($service_name)
			Stop-Service $service.Name
		#}  -ArgumentList $service.Name | Out-Null
	}
	
#	echo "Waiting for stop..."
#	Get-Job | Wait-Job  | Out-Null
	
	echo "Moving  $install_dir to watchdog.old"
	Rename-Item -Path $install_dir -NewName "watchdog.old"
	
	echo "Moving  $new_watchodg_dir to $install_dir"
	Move-Item -Path $new_watchodg_dir -Destination $install_dir
	
	foreach($service in $watchodog_services)
	{
		echo "Starting: $($service.Name)"
		
		#Start-Job {
		#	param($service_name)
			Start-Service $service.Name
			
		#	} -ArgumentList  $service.Name | Out-Null
	}
	
	#echo "Waiting for start..."
	#Get-Job | Wait-Job | Out-Null
}
else
{
	throw "$($args[0]) is not watchdog installation directory"
}

 }
 else{
 throw "You must run this as Administrator"
 }