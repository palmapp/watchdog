#pragma once

#include "script_output.h"
#include "system_update_model.h"
#include "types.h"
#include "urlfetch.h"
#include "user_config.h"
#include "watchdog.h"
#include <boost/asio.hpp>
#include <boost/filesystem/path.hpp>
#include <chrono>
#include <map>
#include <mutex>
#include <optional>
#include <string>

namespace watchdog
{
   struct app_base_config;
   struct system_updater_config
   {
      std::string app_group_name{};
      std::string url{};
      std::string status_url{};
      std::string current_version_string{};

      duration update_check_interval{0};
      filesystem_path apps_path{};
      filesystem_path env_path{};
      filesystem_path env_dir_path{};
      filesystem_path utils_path{};
      filesystem_path logging_dir_path{};
      filesystem_path app_data_path{};

      bool remote_update_synchronization_enabled = false;
      bool stop_before_update = false;
      duration kill_timeout{0};
      std::string script_after_app_fail{};
      bool update_force_on_start = false;
      std::string exit_mode{};
      std::string app_mode{};

      std::map<std::string, system_updater_config> instances;
      user_app_vec users;
   };

   void send_update_status(system_updater_config const &config, const std::string &version, bool success);

   std::optional<update_model> check_for_update(system_updater_config const &config,
                                                commons::cancellation_token token) noexcept;

   bool download_and_extract_update(update_model const &model, system_updater_config const &config,
                                    commons::cancellation_token token);

   bool install_update(system_updater_config const &config, const watchdog::update_model &model,
                       script_output_factory *output = nullptr);

   bool revert_update(system_updater_config const &config, script_output_factory *output = nullptr);

   void run_scripts(const system_updater_config &config, const update_model &model,
                    script_output_factory *output = nullptr);
   void run_scripts_ignore_errors(const system_updater_config &config, const update_model &model,
                                  script_output_factory *output = nullptr);

   std::optional<update_model> read_update_model(const system_updater_config &config);

   std::int64_t get_downloaded_size(const system_updater_config &config, const update_model &model);

} // namespace watchdog
