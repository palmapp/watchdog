#pragma once

#include <spider/scoped_app_base.h>
namespace watchdog
{
   class react_app : public spider::scoped_app_base
   {
    public:
      react_app(std::string scope_path, spider::filesystem_path react_index_html_path);

      bool is_current_scoped_path(spider::string_view scope) override;

    protected:
      void handle_request_internal(spider::request &request, spider::request_handler_callback callback) override;

    private:
      std::string load_react_app_template() const;
      std::string generate_custom_script(spider::request &request);
      void reload_template_if_changed();

      spider::filesystem_path react_index_html_path_;

      std::string template_;
      std::chrono::system_clock::time_point last_change_;
   };

} // namespace watchdog