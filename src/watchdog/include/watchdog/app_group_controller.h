//
// Created by jhrub on 09.01.2023.
//

#pragma once
#include "app_group.h"
#include "app_group_config.h"

#include <complex.h>
#include <map>
#include <memory>
#include <string>

namespace watchdog
{
   class app_group_controller
   {
    public:
      app_group_controller(status_observer_func fun);

      void pause(const std::string &app_group, bool sync = true);
      void restart(const std::string &app_group, optional<system_updater_config> new_config, bool sync = true);
      void status(const std::string &app_group, bool sync = true);

      bool group_exists(const std::string &app_group) const;
      void ensure_group(const std::string &app_group, system_updater_config config);
      void disable_group(const std::string &app_group);
      void update_user_configuration_if_needed(const std::string &app, system_updater_config config);

      void stop_all() const;

    private:
      status_observer_func fun_;
      std::map<std::string, std::unique_ptr<app_group>> groups_;
   };

} // namespace watchdog