//
// Created by jhrub on 21.04.2021.
//

#pragma once
#include <boost/filesystem.hpp>
#include <cstdint>
#include <functional>
#include <string>

namespace watchdog
{
   std::string do_memory_dump(std::uint32_t pid, boost::filesystem::path output_directory,
                              std::string dump_file_prefix);

} // namespace watchdog