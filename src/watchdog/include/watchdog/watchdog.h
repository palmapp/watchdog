#pragma once

#include "os_user_handle.h"
#include "status_observer.h"
#include "watched_app.h"
#include <commons/cancellation_token.h>
#include <memory>

namespace watchdog
{
   class app_session
   {
    public:
      virtual ~app_session() = default;

      virtual void stop() = 0;
      virtual void stop_and_wait() = 0;
   };

   struct app_session_settings
   {
      std::string app_group;
      status_observer_func observer;
      watched_apps apps;
      bool should_stop_before_update;
      std::string fail_script;
      int kill_timeout;
      std::string exit_mode;
      std::optional<os_user> user;
      commons::cancellation_token token;
   };

   std::unique_ptr<app_session> watch_apps(app_session_settings settings);
} // namespace watchdog
