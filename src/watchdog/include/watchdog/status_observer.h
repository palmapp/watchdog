//
// Created by jhrub on 12.10.2020.
//

#pragma once

#include <functional>
#include <string>
#include <variant>

namespace watchdog
{

   struct event_base
   {
      std::string app_group{};
      std::string app_name{};
   };

   struct started_event : public event_base
   {
      std::int64_t pid = 0;
   };

   struct exited_event : public event_base
   {
      std::int64_t code = 0;
   };

   struct term_event : public event_base
   {
   };

   struct not_started_event : public event_base
   {
      std::string message;
   };

   using status_event = std::variant<started_event, exited_event, not_started_event, term_event>;
   using status_observer_func = std::function<void(status_event)>;
} // namespace watchdog