//
// Created by jhrub on 19.10.2020.
//

#pragma once
#include "app_group_config.h"
#include "configuration_template.h"

namespace watchdog::installer
{

   struct app_configuration
   {
      app_group_config config;
      std::vector<std::string> apps;
      std::unordered_map<std::string, configuration_template> configuration_templates;
      bool new_only = false;

      inline bool has_auto_install_new_apps() const
      {
         return config.get_channel_config().update_url_init;
      }

      inline bool is_configuration_migration_enabled() const
      {
         return config.get_channel_config().auto_configuration;
      }
   };

   std::vector<std::string> get_configured_apps(const app_base_config &config);

   void configure_apps(app_configuration const& config);
   void install_app(app_group_config const &config_provider, const std::string &app, step_manager & mng);
   watchdog::installer::app_configuration run_app_configuration(watchdog::filesystem_path const &channel_path,
                                                                                     watchdog::filesystem_path const &cfg_file, bool new_only = false);
   void print_tokens_json(const std::string& channel_url);
} // namespace watchdog::installer