//
// Created by jhrub on 20.12.2022.
//

#pragma once
#include "types.h"

namespace watchdog
{
   struct configuration_template
   {
      std::string name;
      std::string content;

      std::set<std::string> get_required_tokens() const;
      std::set<std::string> get_child_configs() const;

      std::string format(string_map const& tokens, bool allow_env_lookup = true) const;

      static std::unordered_map<std::string, configuration_template> load_templates_from_directory(const filesystem_path &templates_dir);
      static std::unordered_map<std::string, configuration_template> load_templates_from_url(const std::string &url);
      static void write_templates_to_directory(std::unordered_map<std::string, configuration_template> const& templates, const filesystem_path &templates_dir);

      /// This function ensures that CRLF are converted to LF
      static std::string read_file(const filesystem_path& path);
   };



}