//
// Created by jhrub on 09.11.2020.
//

#pragma once

#include <commons/active_object.h>
#include <optional>
#include <string>

#include "app_group_config.h"
#include "script_buffer_output.h"
#include <boost/asio.hpp>
#include <boost/signals2.hpp>

namespace watchdog
{
   struct request
   {
      bool check_for_update = false;
      bool install_update = false;

      std::optional<std::string> app_group;
   };

   struct app_group_status
   {
      enum class state
      {
         none,
         checking,
         downloading,
         pending,
         installing
      };
      state current_state;
      optional<update_model> model;
      optional<std::string> error_message;
      system_updater_config config;

      optional<update_model> pending_model;
   };

   struct update_status
   {
      std::map<std::string, app_group_status> groups;
   };

   class default_update_manager
   {
    public:
      boost::signals2::signal<void(update_status)> on_state_changed;
      boost::signals2::signal<void(std::string)> on_stop_app_group_before_update;
      boost::signals2::signal<void(std::string, system_updater_config)> on_restart_app_group;

      default_update_manager(int threads = 12);

      bool download_update(app_base_config config, script_buffer_output *output = nullptr);
      void install_update(script_buffer_output *output = nullptr);
      std::vector<std::string> check_configured_apps(app_base_config const& config);
    private:
      template <class Fun>
      void update_group_state(Fun fun)
      {
         auto status = [&]
         {

               std::lock_guard lk{mutex_};
               fun(status_);
               return status_;

         }();
         on_state_changed(status);
      }

      std::vector<std::pair<std::string, app_group_status>> get_groups_for_installation() const;

      boost::asio::thread_pool thread_pool_;

      mutable std::mutex mutex_;
      update_status status_;
      optional<app_group_config> config_;
   };
} // namespace watchdog