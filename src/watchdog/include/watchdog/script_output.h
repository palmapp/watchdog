//
// Created by jhrub on 16.09.2022.
//

#pragma once
#include <boost/filesystem.hpp>
#include <memory>
#include <string_view>

namespace watchdog
{

   class execution_error_handler
   {
    public:
      virtual ~execution_error_handler() = default;
      virtual void on_execution_error(std::string_view what) = 0;
   };

   class script_output : public execution_error_handler
   {
    public:
      virtual ~script_output() = default;
      virtual void on_script_begin(std::string_view script_name, std::string_view command) = 0;
      virtual void on_process_started(int pid) = 0;
      virtual void on_std_out(std::string_view buffer) = 0;
      virtual void on_std_error(std::string_view buffer) = 0;
      virtual void on_script_finished(int status) = 0;
   };

   class script_output_factory : public execution_error_handler
   {
    public:
      virtual ~script_output_factory() = default;

      virtual std::shared_ptr<script_output> begin(std::string_view output_name) = 0;
   };

   class step_manager : public script_output_factory
   {
    public:
      virtual ~step_manager() = default;
      inline virtual void on_update_step(std::string text) {}
      inline virtual void on_step_done() {}
      inline virtual void begin_download(boost::filesystem::path file, std::size_t expected_size){};
      inline virtual void finish_download(){};
   };

   class null_script_output : public script_output
   {
    public:
      inline void on_script_begin(std::string_view script_name, std::string_view command) override {}
      inline void on_process_started(int pid) override {}
      inline void on_std_out(std::string_view buffer) override {}
      inline void on_std_error(std::string_view buffer) override {}
      inline void on_execution_error(std::string_view what) override {}
      inline void on_script_finished(int status) override {}
   };
} // namespace watchdog