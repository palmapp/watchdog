//
// Created by jhrub on 22.09.2022.
//

#pragma once

#include "command_parser.h"
#include "types.h"

namespace watchdog
{
   command to_script_engine(command const &cmd);
}