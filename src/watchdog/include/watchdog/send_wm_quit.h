//
// Created by jhrub on 02.11.2020.
//

#pragma once

#include <boost/process.hpp>

namespace watchdog
{
   void send_wm_quit(boost::process::pid_t pid);
}