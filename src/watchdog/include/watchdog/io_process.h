#pragma once
#include "command_parser.h"
#include "os_user_handle.h"
#include "script_output.h"
#include "watched_app.h"
#include <boost/asio/steady_timer.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/process.hpp>
#include <string_view>
#include <variant>

namespace watchdog
{
   struct std_capture_to_file
   {
      filesystem_path stdout_path;
      filesystem_path stderr_path;
   };

   struct std_handle_output
   {
      script_output *output = nullptr;
   };

   using io_process_capture_settings = std::variant<std_capture_to_file, std_handle_output>;

   class io_process : public std::enable_shared_from_this<io_process>
   {
    public:
      static void set_windows_termination_flags(boost::process::group &group);

      /// this method will make sure that our child process will inherit only handles for std io capture
      static boost::process::child start(boost::asio::io_context &io, boost::process::group &group, command const &app,
                                         boost::process::async_pipe &std_out, os_user_handle *user = nullptr);
      /// this method will make sure that our child process will inherit only handles for std io capture
      static boost::process::child start(boost::asio::io_context &io, boost::process::group &group, command const &app,
                                         boost::process::async_pipe &std_out, boost::process::async_pipe &std_err,
                                         os_user_handle *user = nullptr);
      /// this method will make sure that our child process will inherit only handles for std io capture
      static boost::process::child start(boost::asio::io_context &io, boost::process::group &group, command const &app,
                                         boost::process::async_pipe &std_out, boost::process::async_pipe &std_err,
                                         boost::process::async_pipe &std_in, os_user_handle *user = nullptr);

      static std::shared_ptr<io_process> start_io_process(std::shared_ptr<boost::asio::io_context> io,
                                                          boost::process::group &group, command const &app,
                                                          bool enable_stdin = false,
                                                          io_process_capture_settings settings = {std_handle_output{}},
                                                          std::optional<os_user> user = std::nullopt);

      static std::shared_ptr<io_process> start_io_process(boost::asio::io_context &io, boost::process::group &group,
                                                          command const &app, bool enable_stdin = false,
                                                          io_process_capture_settings settings = {std_handle_output{}},
                                                          std::optional<os_user> user = std::nullopt);

      void start_read();
      void write_stdin(std::string_view data);
      void close_file_handles() noexcept;

      boost::process::child &get() noexcept;
      std::string const &name() noexcept;

    private:
      io_process(std::shared_ptr<boost::asio::io_context> io, boost::process::group &group, command const &app,
                 bool enable_stdin = false, io_process_capture_settings settings = {std_handle_output{}},
                 std::optional<os_user> user = std::nullopt);

      io_process(boost::asio::io_context &io, boost::process::group &group, command const &app,
                 bool enable_stdin = false, io_process_capture_settings settings = {std_handle_output{}},
                 std::optional<os_user> user = std::nullopt);

      void on_std_output(const boost::system::error_code &ec, std::size_t size);
      void on_std_error(const boost::system::error_code &ec, std::size_t size);

      void begin_std_out();
      void begin_std_err();
      void begin_timer();

      io_process_capture_settings settings_;
      std::string name_;
      std::shared_ptr<boost::asio::io_context> io_;

      boost::asio::steady_timer timer_;
      boost::process::async_pipe std_out_;
      boost::process::async_pipe std_err_;
      boost::process::async_pipe std_in_;
      std::int64_t std_out_size_ = 0;
      std::int64_t std_err_size_ = 0;

      std::vector<char> std_out_buffer_;
      std::vector<char> std_err_buffer_;

      boost::filesystem::ofstream std_out_file_;
      boost::filesystem::ofstream std_err_file_;

      std::optional<os_user_handle> user_handle_;
      boost::process::child child_;
   };
} // namespace watchdog