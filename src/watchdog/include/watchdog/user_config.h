

#pragma once

#include "os_user_handle.h"
#include <unordered_set>

namespace watchdog
{
   struct user_config
   {
      os_user user{};
      std::unordered_set<std::string> app_groups{};
      bool run_all = false;
   };

   using user_app_vec = std::vector<user_config>;
   std::optional<os_user> try_to_find_user_for_app(const user_app_vec &users, const std::string &app_name);
} // namespace watchdog