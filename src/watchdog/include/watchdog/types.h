#pragma once

#include <boost/filesystem.hpp>
#include <optional>
#include <boost/unordered_map.hpp>
#include <boost/utility/string_view.hpp>
#include <set>
#include <chrono>
#include <string>

namespace watchdog
{
   template <class T>
   using optional = std::optional<T>;

   using filesystem_path = boost::filesystem::path;
   using string_view = boost::string_view;
   using duration = std::chrono::steady_clock::duration;
   using string_map = boost::unordered_map<std::string, std::string>;
} // namespace watchdog