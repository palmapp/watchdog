#pragma once

#include <serialization/serialization.h>
#include <string>
#include <vector>

namespace watchdog
{
   struct metadata_pair
   {
      std::string key;
      std::string value;

      MSGPACK_DEFINE_MAP(key, value);
   };

   struct update_model
   {
      std::string update_url;
      std::string hash;
      int size = 0;
      std::string update_script;
      std::string before_update_script;
      std::string after_update_script;
      std::vector<metadata_pair> metadata;

      int update_script_fail_attempts = 0;
      int before_update_script_fail_attempts = 0;
      int after_update_script_fail_attempts = 0;

      MSGPACK_DEFINE_MAP(update_url, hash, size, update_script, before_update_script, after_update_script, metadata,
                         update_script_fail_attempts, before_update_script_fail_attempts,
                         after_update_script_fail_attempts);
   };

} // namespace watchdog

VISITABLE_STRUCT(watchdog::metadata_pair, key, value);
VISITABLE_STRUCT(watchdog::update_model, update_url, hash, size, update_script, before_update_script,
                 after_update_script, metadata, update_script_fail_attempts, before_update_script_fail_attempts,
                 after_update_script_fail_attempts);
