//
// Created by jhrub on 16.09.2022.
//

#pragma once
#include "command_parser.h"
#include "script_output.h"
#include <chrono>
#include <optional>

namespace watchdog::script_runner
{
   /// It runs a script as a system command and reports its output to script_output
   /// This function cannot throw any exception. Error are reported through script_output interface
   ///
   /// \param name - tracing name of the script
   /// \param command - command including params to be executed
   /// \param output - output to status of the script to be reported
   /// \param time_out - (optional) maximum duration of the script
   /// \return returns true - if script execution ends with exit_code == 0 otherwise false
   bool run_script(std::string_view name, command const &cmd, script_output &output,
                   std::optional<std::chrono::steady_clock::duration> time_out = {}) noexcept;
} // namespace watchdog::script_runner