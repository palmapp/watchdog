//
// Created by jhrub on 10.11.2020.
//

#pragma once
#include "status_observer.h"
#include <chrono>
#include <map>
#include <mutex>
#include <serialization/json/buffer.h>
#include <set>
#include <sstream>
#include <string>
#include <thread>
#include <variant>
#include <vector>

namespace watchdog
{
   struct app_counter
   {
      std::int64_t pid = 0;
      std::int64_t exit_code = 0;

      std::chrono::steady_clock::time_point pid_changed{};
      std::chrono::steady_clock::time_point exit_code_changed{};

      std::int64_t error_state_counter = 0;
      bool terminating = false;
   };

   struct watchdog_status
   {
      void print_current_status();
      void format_json(spider::serialization::json::json_buffer &output);

      void on_event(status_event evt);
      void operator()(watchdog::started_event const &event);
      void operator()(watchdog::exited_event const &event);
      void operator()(watchdog::not_started_event const &event);
      void operator()(watchdog::term_event const &event);

      void clear_app_counter(const std::string &group);

      std::int64_t get_pid(const std::string& group, const std::string& app_name);

    private:
      void set_pid(watchdog::event_base const &event, std::int64_t pid);
      void set_exit_code(watchdog::event_base const &event, std::int64_t code);
      bool is_in_error_state(const app_counter &counter, std::chrono::steady_clock::time_point now) const;
      std::string key(watchdog::event_base const &event) const;

      std::map<std::string, std::set<std::string>> running_apps;
      std::map<std::string, app_counter> app_counters;
      std::mutex mutex_;
   };
} // namespace watchdog
