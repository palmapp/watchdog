#pragma once

#include <atomic>
#include <boost/filesystem.hpp>
#include <chrono>
#include <commons/cancellation_token.h>
#include <string>
#include <vector>
/// this replicates the interface of commons::url with the difference that HTTP requests are send in an another process
namespace watchdog::url
{

   using header_container = std::vector<std::pair<std::string, std::string>>;

   int fetch_to_string(std::string &result, const std::string &url, const header_container &headers = {},
                       commons::cancellation_token token = {},
                       std::chrono::milliseconds timeout = std::chrono::milliseconds{0});
   int send_get_request(const std::string &url, const header_container &headers = {},
                        commons::cancellation_token token = {},
                        std::chrono::milliseconds timeout = std::chrono::milliseconds{0});

   int fetch_to_file(const boost::filesystem::path &output_path, const std::string &url,
                     const header_container &headers = {}, commons::cancellation_token token = {},
                     std::chrono::milliseconds timeout = std::chrono::milliseconds{0});

} // namespace watchdog::url