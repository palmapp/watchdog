#pragma once
#include "types.h"
#include <commons/properties_parser.h>
#include <unordered_map>

#include "command_parser.h"

namespace watchdog
{
   struct watched_app;
   using watched_apps = std::vector<watched_app>;

   struct watched_app
   {
      std::string name;
      command cmd;

      std::string exit_method;
      filesystem_path logging_directory_path;
      bool enable_std_in = false;

      static watched_apps parse_from_map(const commons::properties_map &properties, const filesystem_path &exe_dir);
   };
} // namespace watchdog