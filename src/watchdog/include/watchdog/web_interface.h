//
// Created by jhrub on 10.11.2020.
//

#pragma once
#include "advanced_script_executor.h"
#include "app_group_config.h"
#include "default_update_manager.h"
#include "script_buffer_output.h"
#include "watchdog_status.h"
#include <commons/active_object.h>

#include <queue>

namespace spider
{
   class running_app;
}
namespace watchdog
{
   class websocket_status_bridge
   {
    public:
      virtual ~websocket_status_bridge() noexcept(false) = default;

      virtual void restart(std::string app_group) = 0;
      virtual std::string get_status_string() = 0;
      virtual std::string get_script_output() = 0;

      virtual void check_all_updates() = 0;
      virtual void install_all_updates() = 0;
      virtual void revert_all() = 0;
      virtual void run_script(std::string script_name) = 0;
      virtual void clear_log() = 0;
   };

   struct bind_settings
   {
      int http_port;
      bool bind_all;
      boost::optional<boost::asio::ip::address> bind_ip;
   };

   struct download_update_cmd
   {
      app_base_config cfg;
      script_buffer_output *output;

   };

   struct install_update_cmd
   {
      script_buffer_output *output;
   };

   struct set_update_manager_cmd
   {
      std::shared_ptr<default_update_manager> update_manager;
   };

   using update_manager_command = std::variant<set_update_manager_cmd, download_update_cmd, install_update_cmd>;

   class update_manager_active_object : public commons::active_object<update_manager_command>
   {
    public:
      update_manager_active_object();

    protected:
      void on_message(update_manager_command &message) final;
      std::shared_ptr<default_update_manager> update_manager_;
   };

   class app_group_controller;
   class default_update_manager;
   class web_interface : public websocket_status_bridge, public std::enable_shared_from_this<web_interface>
   {
    public:
      web_interface(commons::cancellation_token token, bind_settings settings, std::string dump_folder);
      ~web_interface();

      void start_thread();

      void restart(std::string app_group) override;
      std::string get_status_string() override;
      std::string get_script_output() override;

      void check_all_updates() override;
      void install_all_updates() override;

      void revert_all() override;
      void run_script(std::string script_name) override;
      void clear_log() override;

      void set_app_base_config(app_base_config const &cfg);
      void set_app_group_controller(std::shared_ptr<app_group_controller> ctrl);
      void set_update_manager(std::shared_ptr<default_update_manager> manager);
      std::string memory_dump(std::string app_group, std::string app);
      void on_event(status_event evt);
      inline script_buffer_output &get_script_buffer_output()
      {
         return buffer_;
      };

    private:
      void format_new_status_string();
      std::string_view get_update_status(const app_group_status &status) const;

      void thread_run() noexcept;
      void notify_clients() noexcept;

      commons::cancellation_token cancellation_token_;
      bind_settings bind_settings_;
      filesystem_path channel_dir_;
      std::string dump_folder_;
      watchdog_status status_;

      std::atomic<bool> notify_clients_;
      std::mutex mutex_;
      std::string current_json_status_;

      update_status current_update_status_;

      std::unique_ptr<spider::running_app> running_app_;
      app_base_config app_base_config_;

      advanced_script_executor scripts_executor_;
      script_buffer_output buffer_;

      std::optional<std::jthread> thread_;

      std::shared_ptr<app_group_controller> ctrl_;
      update_manager_active_object update_manager_;

   };
} // namespace watchdog