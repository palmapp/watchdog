//
// Created by jhrub on 22.09.2022.
//

#pragma once
#include "types.h"
#include <optional>
#include <string>
#include <vector>

namespace watchdog
{
   struct command
   {
      std::string command_string;
      std::vector<std::string> arguments;
      optional<filesystem_path> working_directory;

      /// converts the command to the format that can be executed in a shell
      std::string to_string(bool include_command_string = true) const;

      /// pushes other command to the end as arguments
      /// \cmd command_string must not be empty
      void push_command(const command &cmd);

      /// creates a copy of a current command and expands tokens into it
      command apply_tokens(const string_map *tokens = nullptr, bool allow_env_lookup = true) const;

      /// parses command from command string (interprets " and ')
      static std::optional<command> parse(std::string const &str);
      static std::vector<std::string> parse_command_string(std::string const &str);

      /// expands tokens in a str (syntax: "$token$token/$token\\$token $token")
      /// tokens are delimited by $/\\<space><tab>
      static std::string expand_tokens(std::string const &str, const string_map *tokens = nullptr,
                                       bool allow_env_lookup = true, bool allow_bash_style_variables = true);

      static std::optional<std::string> expand_token(std::string const &token, const string_map *tokens = nullptr,
                                       bool allow_env_lookup = true);
      /// gives a set of tokens that are in the string
      static std::set<std::string> parse_tokens(std::string const& str, bool allow_bash_style_variables = true);

   };

} // namespace watchdog