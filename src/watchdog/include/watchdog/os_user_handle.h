//
// Created by jhrub on 3/11/2024.
//

#pragma once

#include <memory>
#include <optional>
#include <string>

#include <boost/config.hpp>

#ifdef BOOST_WINDOWS
#include <boost/winapi/handles.hpp>
#endif

namespace watchdog
{

   struct os_user
   {
      std::string name;
      std::string domain;
      std::string password;
      bool interactive = false;

      /**
       * @brief It constructs an os_user_handle from a string. The string should be in the following format:
       * <username>@<domain>[?interactive][:password]
       * @param config_string - configuration string, interactive and password is optional
       * @return os_user -  that can be used to impersonate the user, on error it returns std::nullopt
       */
      static std::optional<os_user> parse(std::string_view config_string);

      bool operator==(const os_user &) const = default;
      bool operator!=(const os_user &) const = default;
   };

   struct os_user_handle_impl;
   class os_user_handle
   {
    public:
      /**
       * It constructs an os_user_handle from a username, password and domain. The user will be impersonated
       * @param user - credentials
       * @throws boost::system_error if the user cannot be impersonated
       */
      explicit os_user_handle(os_user const &user);

      ~os_user_handle() noexcept;

      os_user_handle(os_user_handle &&) noexcept;
      os_user_handle &operator=(os_user_handle &&) noexcept;

      os_user_handle(os_user_handle const &) = delete;
      os_user_handle &operator=(os_user_handle const &) = delete;

      [[nodiscard]] std::string get_name() const;

      [[nodiscard]] boost::winapi::HANDLE_ get_handle() const;

    private:
      std::unique_ptr<os_user_handle_impl> impl_;
   };
} // namespace watchdog