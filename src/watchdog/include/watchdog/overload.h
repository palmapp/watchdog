//
// Created by jhrub on 06.01.2023.
//

#pragma once
namespace watchdog
{
   /**
     * This is to easy create an overload set from lambdas
     *
     * overload{[](int x){}, [](float x){ })(2.f);
     *
     * @tparam FunT - lambda
    */
   template<class... FunT>
   struct overload : public FunT ... {
      overload(FunT &&... fnc) : FunT(std::forward<FunT>(fnc))... {}

      using FunT::operator() ...;
   };
}