//
// Created by jhrub on 27.09.2022.
//

#pragma once
#include "script_output.h"
#include <boost/signals2.hpp>
#include <memory>
#include <mutex>
#include <optional>
#include <visit_struct/visit_struct.hpp>

namespace watchdog
{
   struct script_execution
   {
      std::string output_name;
      std::int64_t run_id;

      std::string name;
      std::string command;
      std::optional<int> pid;

      std::string std_out;
      std::string std_err;
      std::optional<int> exit_code;
      std::optional<std::string> error_message;



   };

   struct script_execution_impl : public script_execution, public script_output
   {
      boost::signals2::signal<void(script_execution const &)> on_change;

      script_output * next_output = nullptr;

      void on_script_begin(std::string_view script_name, std::string_view command) override;
      void on_process_started(int pid) override;
      void on_std_out(std::string_view buffer) override;
      void on_std_error(std::string_view buffer) override;
      void on_execution_error(std::string_view what) override;
      void on_script_finished(int status) override;

      mutable std::mutex mutex_;
    protected:
      template <class T>
      script_execution make_diff(T fun);
   };

   using script_execution_ptr = std::shared_ptr<script_execution_impl>;


   struct script_execution_buffer
   {
      std::vector<script_execution_ptr> execution_history;
   };

   class script_buffer_output : public script_output_factory
   {
    public:
      void on_execution_error(std::string_view what) override;

    public:
      boost::signals2::signal<void(script_execution const &)> on_change;
      boost::signals2::signal<void()> on_reset;

      std::vector<script_execution> get_history();

      std::shared_ptr<script_output> begin(std::string_view script_name) override;

      void reset_buffer();

    private:
      std::mutex mutex_;
      script_execution_buffer buffer_;
   };

} // namespace watchdog

VISITABLE_STRUCT(watchdog::script_execution, output_name, run_id, name, command, pid, std_out, std_err, exit_code, error_message);
