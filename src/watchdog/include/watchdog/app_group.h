#pragma once

#include "status_observer.h"
#include "system_updater.h"

#include "watchdog.h"
#include <boost/filesystem.hpp>
#include <commons/active_object.h>
#include <future>
#include <optional>
#include <thread>
#include <variant>

namespace watchdog
{
   class app_group_config;

   struct app_group_message_base
   {
      optional<system_updater_config> new_config;
      std::promise<void> promise;
   };

   struct app_group_message_restart : public app_group_message_base
   {
   };

   struct app_group_message_pause : public app_group_message_base
   {
   };

   struct app_group_message_resume : public app_group_message_base
   {
   };

   struct app_group_message_status : public app_group_message_base
   {
   };

   struct app_group_refresh_config : public app_group_message_base
   {
      system_updater_config new_config;
   };

   using app_group_message = std::variant<app_group_message_status, app_group_message_resume, app_group_message_pause,
                                          app_group_message_restart, app_group_refresh_config>;

   class app_group : public commons::active_object<app_group_message>
   {
    public:
      app_group(std::string name, status_observer_func observer);

      std::future<void> process(app_group_message message);

    protected:
      void on_message(app_group_message &message) final;
      void on_stopped() noexcept final;
      void on_exception(const app_group_message &, const std::exception &) noexcept final;
      void on_unknown_exception(const app_group_message &) noexcept final;

    private:
      std::string name_;
      optional<system_updater_config> config_;
      std::unique_ptr<app_session> current_session_;
      commons::cancellation_token_source cts_;
      status_observer_func status_observer_;

      watched_app create_iisexpress_app(const system_updater_config &config, const boost::filesystem::path path) const;
      watched_apps get_watched_apps(const system_updater_config &updater_config) const;
      void print_app_info(const watched_apps &apps) const;

      std::string get_exit_mode(const watchdog::system_updater_config &updater_config) const;

      void fill_app_session_props(const system_updater_config &updater_config,
                                  std::string active_version_directory_string, watched_app &app) const;
      void create_new_session();
   };
} // namespace watchdog