#pragma once

#include "os_user_handle.h"
#include "system_updater.h"
#include "types.h"
#include "user_config.h"

#include <optional>
#include <set>
#include <unordered_set>

namespace watchdog
{
   namespace cfg
   {
      constexpr const char *property_file = "env.properties";
      constexpr const char *update_url = "update.url";
      constexpr const char *update_status_url = "update.status.url";
      constexpr const char *update_remote_synchronization_enabled = "update.remote.synchronization.enabled";

      constexpr const char *update_interval = "update.interval";
      constexpr const char *app_repository = "app.repository";
      constexpr const char *cfg_repository = "cfg.repository";
      constexpr const char *update_url_init = "update.url.init";
      constexpr const char *update_configuration = "update.configuration";
      constexpr const char *update_auto_enabled = "update.auto.enabled";

      constexpr const char *watchdog_name = "watchdog.name";

      constexpr const char *update_stop_apps = "update.stop.apps";
      constexpr const char *update_stop_apps_after_update = "after_update";
      constexpr const char *update_stop_apps_before_update = "before_update";

      constexpr const char *script_after_app_fail = "script.after.app.fail";
      constexpr const char *kill_timeout = "kill.timeout";
      constexpr const char *update_force_on_start = "update.force.on.start";
      constexpr const char *disabled_apps = "disabled.apps";

      constexpr const char *app_mode = "app.mode";
      constexpr const char *exit_mode = "exit.mode";
      constexpr const char *utils = "utils";
      constexpr const char *app_logging_path = "app.logging.path";
      constexpr const char *app_data_path = "app.data.path";

      constexpr const char *adv_updater_script_prefix = "script.";

   } // namespace cfg

   struct global_scripts
   {
      string_map scripts;
   };

   struct app_base_config
   {
      filesystem_path channel_path{};

      filesystem_path app_repo_path{};
      filesystem_path config_repo_path{};
      filesystem_path utils_path{};
      filesystem_path app_logging_path{};
      filesystem_path app_data_path{};

      std::string channel_base_url{};
      std::string status_base_url{};
      std::chrono::steady_clock::duration update_check_interval{0};
      std::chrono::steady_clock::duration kill_timeout{};
      bool update_force_on_start = false;

      bool update_url_init = false;
      bool auto_configuration = false;
      bool legacy_mode = false;
      bool auto_update_enabled = false;

      std::set<std::string> disabled_apps;

      string_map config_tokens;

      global_scripts scripts;
      std::string watchdog_name;

      string_map get_script_token_map(std::optional<std::string> app_name = {}) const;

      user_app_vec users;
   };

   class app_group_config
   {
    public:
      static std::string parse_channel(const std::string &url);

      app_group_config(app_base_config config);

      system_updater_config get_config(const std::string &app_name, bool silent = false) const;
      std::string get_current_version(system_updater_config const &config) const;

      inline const app_base_config &get_channel_config() const
      {
         return config_;
      }

    private:
      app_base_config config_;
      system_updater_config get_instance_config(system_updater_config const &base, const std::string &app_name,
                                                const std::string &instance_name) const;
      std::vector<std::string> get_instances(const std::string &app_name) const;
   };

   app_base_config parse(filesystem_path const &config_path, std::optional<app_base_config> parent = {});
   void write_config(app_base_config const &config, filesystem_path const &config_path);

} // namespace watchdog