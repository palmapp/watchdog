//
// Created by jhrub on 31.08.2022.
//

#pragma once
#include "app_group_config.h"
#include "command_parser.h"
#include "script_output.h"
#include <atomic>
#include <boost/asio/thread_pool.hpp>
#include <functional>
#include <mutex>

namespace watchdog
{

   class advanced_script_executor
   {
    public:
      void async_run_script(std::string name, command cmd, script_output_factory &output);

    private:
      boost::asio::thread_pool thread_{1};
   };

} // namespace watchdog