//
// Created by jhrub on 21.04.2021.
//

#pragma once

namespace watchdog
{
   template <class Funct>
   struct stupid_msvc_scope_exit
   {
      stupid_msvc_scope_exit(Funct &&funct) : funct_(std::forward<Funct>(funct)) {}

      ~stupid_msvc_scope_exit() noexcept(false)
      {
         funct_();
      }

    private:
      Funct funct_;
   };
} // namespace watchdog